CREATE TABLE IF NOT EXISTS `prices_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `prices` ADD `list_id` INT NOT NULL AFTER `price`;

INSERT INTO `prices_list` (`id`, `name`, `start_date`, `end_date`, `active`) VALUES (7, 'Preturi website', '2021-09-26 00:00:00', '2022-12-31 00:00:00', '1');

UPDATE `prices` SET `list_id`= 7

ALTER TABLE `prices` ADD INDEX(`list_id`);

ALTER TABLE `prices_list` CHANGE `start_date` `start_date` DATE NULL DEFAULT NULL;

ALTER TABLE `prices_list` CHANGE `end_date` `end_date` DATE NULL DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `prices_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;