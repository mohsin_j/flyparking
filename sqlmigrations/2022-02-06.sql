ALTER TABLE `reservations` ADD `initial_days` INT NOT NULL AFTER `initial_total`;

ALTER TABLE `agreements` ADD `initial_days` INT NOT NULL AFTER `initial_total`;