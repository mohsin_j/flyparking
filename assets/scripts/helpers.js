function request_confirm( text ){

	var answer = window.confirm("Sunteti sigur ca doriti actiunea "+text+"?")
		if (!answer) {
			return false;
		}else{
			return true;
		}

}

function disable_all_inputs( exclude_modals = false ){

	$( "input,select" ).each(function( index ) {

		$(this).prop('disabled',true);

	});

	if( exclude_modals === true ){



		$( ".modal input,.modal select" ).each(function( index ) {

			$(this).prop('disabled',false);

		});

	

	}

}

function enable_all_inputs(){

	$( "input,select" ).each(function( index ) {

		$(this).prop('disabled',false);

	});

}