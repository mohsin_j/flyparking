<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* Library for ANAF API
 */
class Registru_tva {

	function get_response($cui){

		$today = date('Y-m-d');

		$url = "https://webservicesp.anaf.ro/PlatitorTvaRest/api/v3/ws/tva";   

		$cui = str_replace(' ', '', $cui);
		$cui = str_replace('r', '', $cui);
		$cui = str_replace('R', '', $cui);
		$cui = str_replace('o', '', $cui);
		$cui = str_replace('O', '', $cui);

		$content[0]['cui'] = $cui;
		$content[0]['data'] = $today;

		//echo json_encode($content);
		$content = json_encode($content);

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		        array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if ( $status != 200 ) {
		    $firma['error_show'] = true;
		    $firma['error'] = "A avut loc o Eroare. Corecteaza codurile TVA. Asigura-te ca nu ai lasat linii goale / coduri gresite.";
		    //$firma['error'] .= "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);

		    return json_encode($firma);
		    
		}

		curl_close($curl);

		//echo $json_response;


		$response = json_decode($json_response, true);

		foreach($response['found'] as $key => $r){
			
			$firma['success'][$key]['denumire'] = (!empty($r['denumire'])) ? $r['denumire'] : 'CUI GRESIT : '.$r['cui'];

			$firma['success'][$key]['key'] = $key;
			
			if(empty($r['denumire'])){
				continue;
			}

			
			$firma['success'][$key]['cui'] = $r['cui'];
			$firma['success'][$key]['data'] = $r['data'];
			
			if( $r['statusSplitTVA'] == true ){
				$firma['success'][$key]['statusSplitTVA'] = 'Aplica Split';
			}else{
				$firma['success'][$key]['statusSplitTVA'] = 'Nu Aplica Split';
			}
			
			$firma['success'][$key]['dataInceputSplitTVA'] = $r['dataInceputSplitTVA'];
			$firma['success'][$key]['dataAnulareSplitTVA'] = $r['dataAnulareSplitTVA'];
			
			if( $r['scpTVA'] == true ){
				$firma['success'][$key]['scpTVA'] = 'Da';
			}else{
				$firma['success'][$key]['scpTVA'] = 'Nu';
			}
			
			if( $r['statusTvaIncasare'] == true ){
				$firma['success'][$key]['statusTvaIncasare'] = 'Da';
			}else{
				$firma['success'][$key]['statusTvaIncasare'] = 'Nu';
			}
			
			if( $r['statusInactivi'] == true ){
				$firma['success'][$key]['statusInactivi'] = 'Inactiv';
			}else{
				$firma['success'][$key]['statusInactivi'] = 'Activ';
			}

			$firma['success'][$key]['mesaj'] = $r['mesaj_ScpTVA'];
			
			
			$firma['success'][$key]['data_anul_imp_ScpTVA'] = $r['data_anul_imp_ScpTVA'];
			$firma['success'][$key]['data_sfarsit_ScpTVA'] = $r['data_sfarsit_ScpTVA'];
			
			$firma['success'][$key]['adresa'] = $r['adresa'];
			$firma['success'][$key]['dataRadiere'] = $r['dataRadiere'];
			
			
		}

		return json_encode($firma);

	}

}

?>
