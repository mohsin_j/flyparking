<?php
/**
 * CodeIgniter Alert Helpers For Alert Library
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Twitter: @d_nizh / Facebook: /dna1993
 * @link		https://github.com/dnizh
 */
 
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('has_alert'))
{
	function has_alert($type = '')
	{
		
		if(!empty($_SESSION['alerts'])){
			return $_SESSION['alerts'];
		}
		
		return false;
	}
}

if (!function_exists('set_alert'))
{
	function set_alert($type, $message)
	{
		$_SESSION['alerts'][]['type'] = $type;
		$_SESSION['alerts'][]['message'] = $type;
		
	}
}
