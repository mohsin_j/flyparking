<?php



function pre($data){



    echo '<pre>';

    print_r($data);

    exit();



}



function api_response($message = array(), $http_code = 200){



    http_response_code($http_code);



    echo json_encode($message);



    exit();



}



//Transforms metadata options table into quicly accessible arrays

//Takes db rows as data, key as meta_key, value as meta_value

function meta_key_value_transform($data, $key = 'name', $value = 'value'){



    $new_data = [];



    foreach ($data as $row) {

        $new_data[$row[$key]] = $row[$value];

    }



    return $new_data;



}



if (!function_exists('check_if_admin'))

{

    function check_if_admin()

    {

        	$email = $_SESSION['auth_email'];



            $CI =& get_instance();

            $access_level = $CI->db->where('email',$email)->get('users')->row_array();



    	   if($access_level['access_level'] == 1){



    	   		return true;



    	   }else{

    	   		return false;

    	   }

    }

}



//take Y-m-d H:i:s return as d-m-Y H:i:s

if (!function_exists('format_date'))

{

    function format_date( $ymd_date )

    {

        

        return date( 'd.m.Y H:i:s', strtotime( $ymd_date ) );



    }

}



//define reservation types

if (!function_exists('get_res_types'))

{

    function get_res_types()

    {

        

        return array( 'Standard', 'Abonament', 'Courtesy ( Gratuit )', 'ParkVia' );



    }

}



if (!function_exists('dmy_to_ymd'))

{



    function dmy_to_ymd($datetime) {

        $date = date_create_from_format('d/m/Y H:i', $datetime);

        if ($date != false) {

            $datetime = date_format($date, 'Y-m-d H:i:s');

        }

        return $datetime;

    }



}



if (!function_exists('ymd_to_dmy'))

{



    function ymd_to_dmy($datetime) {

        $date = date_create_from_format('Y-m-d H:i:s', $datetime);

        if ($date != false) {

            $datetime = date_format($date, 'd/m/Y H:i');

        }

        return $datetime;

    }



}



if(!function_exists('agregator_log')){



    function agregator_log($data, $agregator) {



        $good_data = array();



        $good_data['message'] = $data['msg'];

        $good_data['success'] = $data['success'];

        $good_data['agregator'] = $agregator;

        

        $CI =& get_instance();

        $CI->db->insert('sync_log', $good_data);



    }



}



function get_option($handle){



     $CI =& get_instance();

     $data = $CI->db->where('name', $handle)->get('options')->row_array();



     return $data['value'];



}



function get_user_by_id($user_id){



    //might be a string. bad db design

    if(!is_numeric($user_id)){

        return $user_id;

    }



     $CI =& get_instance();

     $data = $CI->db->where('id', $user_id)->get('users')->row_array();



     return $data['email'];



}



function get_agreement_by_res_id($reservation_id){



    



     $CI =& get_instance();

     $data = $CI->db->where('res_id', $reservation_id)->get('agreements')->row_array();



     return $data['ID'];



}



function format_payment_type($type){



    if($type == 0){

        return 'Cash';

    }



    if($type == 1){

        return 'Card';

    }



    return ucfirst($type);





}







function get_res_price_list_id($res_id){



     $CI =& get_instance();

     $data = $CI->db->where('reservation_id', $res_id)->get('prices_log')->row_array();



     if(!empty($data)){

         return $data['list_id'];

     }



     return 0;



    



}



function get_res_price_list_name($res_id){



     $CI =& get_instance();

     $data = $CI->db->where('res_id', $res_id)->get('prices_log')->row_array();



     $data = $CI->db->where('id',$data['list_id'])->get('prices_list')->row_array();



     return $data['name'];



}





//Checks overlapping date ranges

function datesOverlap($start_one,$end_one,$start_two,$end_two) {



    //In case they are not datetime object already

    $start_one = ( is_string($start_one) ) ? new DateTime($start_one) : $start_one;

    $end_one = ( is_string($end_one) ) ? new DateTime($end_one) : $end_one;

    $start_two = ( is_string($start_two) ) ? new DateTime($start_two) : $start_two;

    $end_two = ( is_string($end_two) ) ? new DateTime($end_two) : $end_two;

    $end_two = $end_two->modify("-1 second");



       if($start_one <= $end_two && $end_one >= $start_two) { //If the dates overlap

            return min($end_one,$end_two)->diff(max($start_two,$start_one))->days + 1; //return how many days overlap

       }



       return 0; //Return 0 if there is no overlap

}



//Checks overlapping date ranges

function daysInDateRange($start_one,$end_one) {



    if($start_one >= $end_one){

        return 0;

    }



    //In case they are not datetime object already

    $start_one = ( is_string($start_one) ) ? new DateTime($start_one) : $start_one;

    $end_one = ( is_string($end_one) ) ? new DateTime($end_one) : $end_one;

    $end_one = $end_one->modify("-1 second");



    return (int) $end_one->diff($start_one)->days + 1; //return how many days overlap

       

}



function dmy_to_ymd($datetime) {

        $date = date_create_from_format('d/m/Y H:i', $datetime);

        if ($date != false) {

            $datetime = date_format($date, 'Y-m-d H:i:s');

        }

        return $datetime;

}