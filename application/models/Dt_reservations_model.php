<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dt_reservations_Model extends CI_Model{
    
    function __construct() {
        // Set table name
        $this->table = 'reservations';
        // Set orderable column fields
        $this->column_order = array('r.ID','c.name','r.checkout_date', 'r.checkin_date','c.phone','r.status','r.total');
        // Set searchable column fields
       $this->column_search = array('r.ID','c.name','r.checkout_date', 'r.checkin_date','c.phone','r.status','r.total');
        // Set default order
        //$this->order = array('preety_no' => 'desc');
    }
    
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData, false);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData, true);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData, $count = false){
		
			
		$fields = $this->db->field_data('reservations');

		$arr = array();

		foreach ($fields as $field){

		   @$arr[ 'r.'.$field->name ] = 'r.'.$field->name;

		}

		$fields = $this->db->field_data('customers');

		foreach ($fields as $field){

		   @$arr[ 'c.'.$field->name ] = 'c.'.$field->name;

		}

        @$arr[ 'username' ] = 'u.username';


		 $this->db->select('*, r.ID as ID, r.status as status');
   		 $this->db->join('customers c', 'c.ID = r.customer_id', 'left');
         $this->db->join('users u', 'r.created_by = u.ID', 'left');
   		 $this->db->from('reservations r');
		
 
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
				
				$item = $arr[ $item ];
				
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         if(!empty($postData['status'])){
                $this->db->where('r.status', $postData['status']);
         }

        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

}