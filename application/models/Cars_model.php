<?php



class Cars_model extends CI_Model {

	public function save_client_car($customer_email, $marca, $model, $nr_inmatriculare){

		//check if car exists
		$q = $this->db->where('customer_email', $customer_email)->where('nr_inmatriculare', $nr_inmatriculare)->get('customers_cars');
		//if not exists
		if( $q-> num_rows() == 0 ){

			$this->db->insert('customers_cars', ['customer_email' => $customer_email, 'marca' => $marca, 'model' => $model, 'nr_inmatriculare' => $nr_inmatriculare, 'created_by' => $_SESSION['auth_user_id'], 'created_at' => date('Y-m-d H:i:s')]);

			return true;

		}

		return false;

	}

//EOC
}

