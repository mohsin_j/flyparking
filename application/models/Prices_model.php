<?php

class Prices_model extends CI_Model {

    public function get_option($option_handle)
    {

       	$data = $this->db->where('name', $option_handle)->get('prices_options');

       	if($data->num_rows() > 0){
       		$data = $data->row_array();

       		return $data['value'];
       	}

        return false;
    }


    public function get_price_lists($grouped = false)
    {

        if($grouped){
          $this->db->group_by('name');
        }

       	$data = $this->db->order_by('name')->get('prices_list');

       	if($data->num_rows() > 0){
       		$data = $data->result_array();

       		return $data;
       	}

        return false;
    }

    public function get_available_price_list()
	{
		$today = date('Y-m-d');

//		echo '<br>';
//		echo $today;
//		echo '<br>';

		$select = 'id, name, description, DATE_FORMAT(start_date, "%d-%m-%Y") start_date, DATE_FORMAT(end_date, "%d-%m-%Y") end_date';
		$this->db->select($select) ;
		$this->db->from('prices_list pl');
		$this->db->where('pl.active','1');
		$this->db->where("(pl.name='Preturi website' OR pl.name='Preturi website prepaid')");
		$this->db->where('pl.start_date <=', $today);
		$this->db->where('pl.end_date >=', $today);
		$this->db->order_by('pl.name','asc');
		$query = $this->db->get();

		if($query->num_rows() != 0)
		{
			$prices_list = $query->result_array();
			foreach ($prices_list as $index=>$list){
				$list_id = $list['id'];

				$this->db->select('*');
				$this->db->from('prices');
				$this->db->where('list_id', $list_id);
				$query = $this->db->get();
				$prices = array();
				if($query->num_rows() != 0){
					$prices = $query->result_array();
				}
				$prices_list[$index]['prices'] = $prices;
			}
			return $prices_list;
		}
		return false;
	}
}
