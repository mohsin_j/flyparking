<?php

class Reservation_model extends CI_Model {

    public function getNextDaysReservations($days = 7)
    {
        $query = $this->db->query('SELECT COUNT(*) as reservations_count FROM reservations WHERE status = 1 AND DATE(checkout_date) > CURRENT_DATE() AND DATE(checkout_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL '.$days.' DAY )');

        $results = $query->result()[0]->reservations_count;
        return $results;
    }

    public function getNextDaysExits($days = 7)
    {
        $query = $this->db->query('SELECT COUNT(*) as exits_count FROM agreements WHERE status = 4 AND DATE(checkin_date) >= CURRENT_DATE() AND DATE(checkin_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL '.$days.' DAY )');

        $results = $query->result()[0]->exits_count;
        return $results;
    }

    public function get_reservation($res_id)
    {
        return $this->db->where('ID', $res_id)->get('reservations')->row_array();
    }

    public function get_agreement($res_id)
    {
        return $this->db->where('ID', $res_id)->get('reservations')->row_array();
    }

    public function get_agreement_or_reservation($res_id)
    {
        $data = $this->db->where('ID', $res_id)->get('reservations')->row_array();

        if($data['status'] == 4){

            return $this->db->where('res_id', $res_id)->get('agreements')->row_array();

        }

        //else
        return $data;

        

    }
}