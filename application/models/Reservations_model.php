<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservations_model extends CI_Model{
    
    public function parkvia_save_reservation($booking_data){

    	if($booking_data->Status != 'CONFIRMED'){

    		agregator_log(array( 'success' => false, 'msg' => 'Not confirmed' ), 'ParkVia');

    		return json_encode( array( 'success' => false, 'msg' => 'Not confirmed' ) );
    	}

    	$res_exists = $this->db->where('agregator_internal_id', $booking_data->Id)->get('reservations');

    	if($res_exists->num_rows() > 0){

    		agregator_log(array( 'success' => false, 'msg' => 'Reservation already exists with ParkVia id of: '.$booking_data->Id ), 'ParkVia');

    		return json_encode( array( 'success' => false, 'msg' => 'Reservation already exists with ParkVia id of: '.$booking_data->Id ) );
    	}

        $lock_payment = 1;
        $washing = 'false';
        $data['data_plecare'] = date("Y-m-d H:i:s", strtotime($booking_data->ArrivalDate));
        $data['data_retur'] = date("Y-m-d H:i:s", strtotime($booking_data->DepartureDate));
        $subtotal_calculation = $this->compute_reservation_price($data['data_plecare'], $data['data_retur']);
        $total_calculation = $this->compute_reservation_price($data['data_plecare'], $data['data_retur'], $washing);
        $data['total'] = $booking_data->AmountPaid;
        $data['subtotal'] = $booking_data->AmountPaid;
        $data['paid'] = $booking_data->AmountPaid;
        $data['days'] = $total_calculation['days'];
        $data['initial_total'] = $booking_data->AmountPaid;
        if ($lock_payment == 'true') {
            $data['total'] = $booking_data->AmountPaid;
            $data['initial_total'] = $booking_data->AmountPaid;
            $data['subtotal'] = $booking_data->AmountPaid;
            $data['days'] = $total_calculation['days'];
            $res_data['lock_payment'] = 1;
        }
        $customer_data['name'] = $booking_data->Customer->FirstName.' '.$booking_data->Customer->Surname;
        $customer_data['email'] = $booking_data->Customer->Email;
        $customer_data['phone'] = $booking_data->Customer->Mobile;
        $customer_data['CUI'] = 0;
        $this->db->insert('customers', $customer_data);
        $success = ($this->db->affected_rows() != 1) ? false : true;
        if (!$success) {
            echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut insera clientul'));
            exit();
        }
        $res_data['customer_id'] = $this->db->insert_id();
        $parking_spot = $this->find_parking_spot($data['data_plecare'], $data['data_retur']);
        $res_data['parking_spot'] = $parking_spot;
        $res_data['checkout_date'] = $data['data_plecare'];
        $res_data['checkin_date'] = $data['data_retur'];
        $res_data['days'] = $data['days'];
        $res_data['subtotal'] = $data['subtotal'];
        $res_data['total'] = $data['total'];
        $res_data['paid'] = $data['paid'];
        $res_data['initial_total'] = $data['total'];
        $res_data['discount'] = 0;

        //parkvia extra details
        $res_data['agregator_created_at'] = $booking_data->BookingDate;
        $res_data['agregator_reference'] = $booking_data->Reference;
        //ParkVia ID is 1
        $res_data['agregator_id'] = 1;
        $res_data['agregator_internal_id'] = $booking_data->Id;
        $res_data['language'] = $booking_data->LanguageCode;

        $res_data['discount'] = '0';

        $res_data['mobilpay_status'] = '';

        $res_data['washing'] = 0;
        $res_data['status'] = 1;
        $res_data['created_by'] = 3;
        $res_data['tip'] = 'ParkVia';
        $res_data['request_invoice'] = 0;

        

        $this->db->insert('reservations', $res_data);
        $success = ($this->db->affected_rows() != 1) ? false : true;
        $res_id = $this->db->insert_id();

        if($res_data['tip'] == 'ParkVia'){
            $payment_data['res_id'] = $res_id;
            $payment_data['amount'] = $data['total'];
            $payment_data['type'] = 'ParkVia';
            $payment_data['transfered'] = 1;
            $payment_data['bon_fiscal_emis'] = 0;
            $payment_data['status'] = 1;
            $payment_data['created_by'] = 'parking.alanarent.ro';            

            $this->db->insert('payments', $payment_data);

        }

        $prices = $this->db->get('prices')->result_array();
        foreach ($prices as $price) {
            unset($price['ID']);
            $price['reservation_id'] = $res_id;
            $this->db->insert('prices_log', $price);
        }
        $washing_option = $this->db->where('name', 'washing')->get('options')->result_array() [0];
        $washing_option['reservation_id'] = $res_id;
        unset($washing_option['ID']);
        $this->db->insert('options_log', $washing_option);

        if ($success) {

        	agregator_log(array('success' => true, 'msg' => 'Rezervare salvata cu succes: '.$res_id, 'res_id' => $res_id), 'ParkVia');

        	 return json_encode(array('success' => true, 'msg' => 'Rezervare salvata cu succes: '.$res_id, 'res_id' => $res_id));
            exit();
        } else {

        	agregator_log(array('success' => false, 'msg' => 'Nu s-a putut salva rezervarea cu ID intern: '. $booking_data->Id), 'ParkVia');

            return json_encode(array('success' => false, 'msg' => 'Nu s-a putut salva rezervarea!'));
            exit();
        }

    }

    private function find_parking_spot($checkout, $checkin) {
        $total_parking_spots = $this->db->where('name', 'total_parking_spots')->get('options')->row()->value;
        $statuses = array(1, 4);
        $results = $this->db->group_start()->or_group_start()->where('checkin_date >=', $checkin)->where('checkout_date <=', $checkin)->group_end()->or_group_start()->where('checkin_date >=', $checkout)->where('checkout_date <=', $checkout)->group_end()->or_group_start()->where('checkin_date <=', $checkin)->where('checkout_date >=', $checkout)->group_end()->group_end()->where_in('status', $statuses)->order_by('parking_spot', 'ASC')->get('reservations')->result_array();

        if (empty($results)) {
            return 1;
        }
        $parking_spots = array();
        foreach ($results as $res) {
            array_push($parking_spots, $res['parking_spot']);
        }
        $i = 1;
        while ($i <= $total_parking_spots) {
            if (!in_array($i, $parking_spots)) {
                return $i;
                exit();
            }
            $i++;
        }
    }

    private function compute_reservation_price($checkout = null, $checkin = null, $washing = null, $res_id = null) {
        

        $date1 = dmy_to_ymd($checkout);
        $date1 = new DateTime(date('Y-m-d H:i:s', strtotime($date1)));
        $date2 = dmy_to_ymd($checkin);
        $date2 = new DateTime(date('Y-m-d H:i:s', strtotime($date2)));
        $date2 = $date2->modify("-1 second");
        $diff = $date1->diff($date2)->days + 1;
        $per_day = 0;

        $prices = $this->db->get('prices')->result_array();

        $first_chunk = $prices[0];
        unset($prices[0]);
        $i = 1;
        while ($i <= $diff) {
            if ($i <= $first_chunk['days']) {
                $per_day+= $first_chunk['price'];
            }
            if ($i > $first_chunk['days'] && $i <= $prices[1]['days']) {
                $per_day+= $prices[1]['price'];
            }
            foreach ($prices as $key => $val) {
                $next_key = $key + 1;
                if ($i > $val['days'] && $i <= $prices[$next_key]['days']) {
                    $per_day+= $prices[$next_key]['price'];
                }
            }
            $i++;
        }
        $res['total'] = $per_day;
        $res['subtotal'] = $per_day;
        $res['days'] = $diff;
        if ($washing == 'true') {
            $washing = $this->db->where('name', 'washing')->get('options')->row();
            $washing = $washing->value;
            if (!empty($res_id)) {
                $washing = $this->db->where('name', 'washing')->where('reservation_id', $res_id)->get('options_log')->row();
                $washing = $washing->value;
            }
            $res['total'] = $res['total'] + $washing;
        }
       
        return $res;
        

    }
    

}