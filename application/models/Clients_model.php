<?php

class Clients_model extends CI_Model {

    public function find_client($query)
    {
       
       	$this->db->like('name', $query);
       	$this->db->or_like('email', $query);
       	$this->db->or_like('phone', $query);
       	$this->db->group_by('email');
       	$data = $this->db->get('customers');

       	if($data->num_rows() > 0){
       		return $data->result_array();
       	}

        return false;
    }

}