<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_base extends CI_Migration {

	public function up() {

		## Create Table agreements
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`res_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`customer_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`parking_spot` int(11) NOT NULL ");
		$this->dbforge->add_field("`total` float NOT NULL ");
		$this->dbforge->add_field("`subtotal` float NOT NULL ");
		$this->dbforge->add_field("`days` int(11) NOT NULL ");
		$this->dbforge->add_field("`checkout_date` datetime NOT NULL ");
		$this->dbforge->add_field("`checkin_date` datetime NOT NULL ");
		$this->dbforge->add_field("`paid` float NOT NULL ");
		$this->dbforge->add_field("`washing` int(11) NOT NULL ");
		$this->dbforge->add_field("`internal_remarks` text NOT NULL ");
		$this->dbforge->add_field("`external_remarks` text NOT NULL ");
		$this->dbforge->add_field("`status` int(11) NOT NULL ");
		$this->dbforge->add_field("`created_by` int(11) NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT 'CURRENT_TIMESTAMP' ");
		$this->dbforge->create_table("agreements", TRUE);
		$this->db->query('ALTER TABLE  `agreements` ENGINE = MyISAM');
		## Create Table customers
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`name` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`email` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`phone` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT 'CURRENT_TIMESTAMP' ");
		$this->dbforge->create_table("customers", TRUE);
		$this->db->query('ALTER TABLE  `customers` ENGINE = MyISAM');
		## Create Table email_remplates
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`text` text NOT NULL ");
		$this->dbforge->add_field("`language` varchar(255) NOT NULL ");
		$this->dbforge->create_table("email_remplates", TRUE);
		$this->db->query('ALTER TABLE  `email_remplates` ENGINE = MyISAM');
		## Create Table options
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`name` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`value` float NOT NULL ");
		$this->dbforge->create_table("options", TRUE);
		$this->db->query('ALTER TABLE  `options` ENGINE = MyISAM');
		## Create Table options_log
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`name` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`value` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`reservation_id` int(11) NOT NULL ");
		$this->dbforge->create_table("options_log", TRUE);
		$this->db->query('ALTER TABLE  `options_log` ENGINE = MyISAM');
		## Create Table payments
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`res_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`amount` float NOT NULL ");
		$this->dbforge->add_field("`type` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT 'CURRENT_TIMESTAMP' ");
		$this->dbforge->add_field("`created_by` varchar(255) NOT NULL ");
		$this->dbforge->create_table("payments", TRUE);
		$this->db->query('ALTER TABLE  `payments` ENGINE = MyISAM');
		## Create Table prices
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`days` int(11) NOT NULL ");
		$this->dbforge->add_field("`price` int(11) NOT NULL ");
		$this->dbforge->create_table("prices", TRUE);
		$this->db->query('ALTER TABLE  `prices` ENGINE = MyISAM');
		## Create Table prices_log
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`days` int(11) NOT NULL ");
		$this->dbforge->add_field("`price` int(11) NOT NULL ");
		$this->dbforge->add_field("`reservation_id` int(11) NOT NULL ");
		$this->dbforge->create_table("prices_log", TRUE);
		$this->db->query('ALTER TABLE  `prices_log` ENGINE = MyISAM');
		## Create Table reservations
		$this->dbforge->add_field("`ID` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("ID",true);
		$this->dbforge->add_field("`customer_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`parking_spot` int(11) NOT NULL ");
		$this->dbforge->add_field("`total` float NOT NULL ");
		$this->dbforge->add_field("`subtotal` float NOT NULL ");
		$this->dbforge->add_field("`days` int(11) NOT NULL ");
		$this->dbforge->add_field("`checkout_date` datetime NOT NULL ");
		$this->dbforge->add_field("`checkin_date` datetime NOT NULL ");
		$this->dbforge->add_field("`paid` float NOT NULL ");
		$this->dbforge->add_field("`washing` int(11) NOT NULL ");
		$this->dbforge->add_field("`internal_remarks` text NOT NULL ");
		$this->dbforge->add_field("`external_remarks` text NOT NULL ");
		$this->dbforge->add_field("`status` int(11) NOT NULL ");
		$this->dbforge->add_field("`created_by` int(11) NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT 'CURRENT_TIMESTAMP' ");
		$this->dbforge->create_table("reservations", TRUE);
		$this->db->query('ALTER TABLE  `reservations` ENGINE = MyISAM');
		## Create Table users
		$this->dbforge->add_field("`id` int(10) unsigned NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`email` varchar(249) NOT NULL ");
		$this->dbforge->add_field("`password` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`username` varchar(100) NULL ");
		$this->dbforge->add_field("`status` tinyint(2) unsigned NOT NULL ");
		$this->dbforge->add_field("`verified` tinyint(1) unsigned NOT NULL ");
		$this->dbforge->add_field("`resettable` tinyint(1) unsigned NOT NULL DEFAULT '1' ");
		$this->dbforge->add_field("`roles_mask` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`registered` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`last_login` int(10) unsigned NULL ");
		$this->dbforge->add_field("`force_logout` mediumint(7) unsigned NOT NULL ");
		$this->dbforge->create_table("users", TRUE);
		$this->db->query('ALTER TABLE  `users` ENGINE = MyISAM');
		## Create Table users_confirmations
		$this->dbforge->add_field("`id` int(10) unsigned NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`user_id` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`email` varchar(249) NOT NULL ");
		$this->dbforge->add_field("`selector` varchar(16) NOT NULL ");
		$this->dbforge->add_field("`token` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`expires` int(10) unsigned NOT NULL ");
		$this->dbforge->create_table("users_confirmations", TRUE);
		$this->db->query('ALTER TABLE  `users_confirmations` ENGINE = MyISAM');
		## Create Table users_remembered
		$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`user` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`selector` varchar(24) NOT NULL ");
		$this->dbforge->add_field("`token` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`expires` int(10) unsigned NOT NULL ");
		$this->dbforge->create_table("users_remembered", TRUE);
		$this->db->query('ALTER TABLE  `users_remembered` ENGINE = MyISAM');
		## Create Table users_resets
		$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`user` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`selector` varchar(20) NOT NULL ");
		$this->dbforge->add_field("`token` varchar(255) NOT NULL ");
		$this->dbforge->add_field("`expires` int(10) unsigned NOT NULL ");
		$this->dbforge->create_table("users_resets", TRUE);
		$this->db->query('ALTER TABLE  `users_resets` ENGINE = MyISAM');
		## Create Table users_throttling
		$this->dbforge->add_field("`bucket` varchar(44) NOT NULL ");
		$this->dbforge->add_key("bucket",true);
		$this->dbforge->add_field("`tokens` float unsigned NOT NULL ");
		$this->dbforge->add_field("`replenished_at` int(10) unsigned NOT NULL ");
		$this->dbforge->add_field("`expires_at` int(10) unsigned NOT NULL ");
		$this->dbforge->create_table("users_throttling", TRUE);
		$this->db->query('ALTER TABLE  `users_throttling` ENGINE = MyISAM');
	 }

	public function down()	{
		### Drop table agreements ##
		$this->dbforge->drop_table("agreements", TRUE);
		### Drop table customers ##
		$this->dbforge->drop_table("customers", TRUE);
		### Drop table email_remplates ##
		$this->dbforge->drop_table("email_remplates", TRUE);
		### Drop table options ##
		$this->dbforge->drop_table("options", TRUE);
		### Drop table options_log ##
		$this->dbforge->drop_table("options_log", TRUE);
		### Drop table payments ##
		$this->dbforge->drop_table("payments", TRUE);
		### Drop table prices ##
		$this->dbforge->drop_table("prices", TRUE);
		### Drop table prices_log ##
		$this->dbforge->drop_table("prices_log", TRUE);
		### Drop table reservations ##
		$this->dbforge->drop_table("reservations", TRUE);
		### Drop table users ##
		$this->dbforge->drop_table("users", TRUE);
		### Drop table users_confirmations ##
		$this->dbforge->drop_table("users_confirmations", TRUE);
		### Drop table users_remembered ##
		$this->dbforge->drop_table("users_remembered", TRUE);
		### Drop table users_resets ##
		$this->dbforge->drop_table("users_resets", TRUE);
		### Drop table users_throttling ##
		$this->dbforge->drop_table("users_throttling", TRUE);

	}
}