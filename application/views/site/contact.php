

	<?php include('layout/header.php'); ?>



    <!-- BREADCRUMB -->

	<section class="bg-cherry pt-s3 pb-s3 text-white">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="d-lg-flex text-center">

						<div class="align-self-center mb-0--lg mb-s1--md mb-s1--sm mb-s1--xs">

							<h3 class="text-bold-400 text-size-s2 m-0 text-white">Contact Us</h3>

						</div>

						<div class="ml-auto align-self-center">

							<ol class="breadcrumb d-flex justify-content-center m-0 bg-none text-size-s1 p-0">

								<li class="breadcrumb-item"><a class="text-white" href="/">Home</a></li>

								<li class="breadcrumb-item" aria-current="page">Contactati-ne</li>

							</ol>

						</div>

					</div>

				</div>

			</div> <!-- ROW -->

		</div> <!-- /CONTAINER -->

	</section> <!-- /SECTION -->

	<!-- /BREADCRUMB -->

	<?php if ($this->session->flashdata('success')) { 

	echo '<div class="alert alert-success">

  <strong>Success!</strong> '.$this->session->flashdata('success').'.

</div>'; 

} ?>

	<section class="bg-v5-light pt-s2 pb-s2">

		<div class="container">

			<div class="row">

				<div class="col-lg-6 col-md-12 col-sm-12">

					<div class="mt-s2 mb-s2 box-shadow-parent--hover">

						<div class="d-flex">

							<div class="mr-s2">

								<div class="text-size-s2 rounded-circle bg-cherry text-white text-center h--70 w--70 d-flex justify-content-center align-items-center box-shadow-v2-s3 box-shadow-v3-s3-child--hover">

									<i class="icon-phone"></i>

								</div>

							</div>

							<div>

								<h2 class="text-size-18 text-bold-600">Sunați-ne</h2>

								<p class="m-0">+40 757 664 664</p>

							

							</div>

						</div>

					</div>

				</div>

				<div class="col-lg-6 col-md-12 col-sm-12">

					<div class="mt-s2 mb-s2 box-shadow-parent--hover">

						<div class="d-flex">

							<div class="mr-s2">

								<div class="text-size-s2 rounded-circle bg-cherry text-white text-center h--70 w--70 d-flex justify-content-center align-items-center box-shadow-v2-s3 box-shadow-v3-s3-child--hover">

									<i class="icon-envelope-open"></i>

								</div>

							</div>

							<div>

								<h2 class="text-size-18 text-bold-600">Trimiteți-ne un e-mail</h2>

								<p class="m-0">reservations@flyparking.ro</p>

							

							</div>

						</div>

					</div>

				</div>

				<!--<div class="col-lg-4 col-md-12 col-sm-12">-->

				<!--	<div class="mt-s2 mb-s2 box-shadow-parent--hover">-->

				<!--		<div class="d-flex">-->

				<!--			<div class="mr-s2">-->

				<!--				<div class="text-size-s2 rounded-circle bg-cherry text-white text-center h--70 w--70 d-flex justify-content-center align-items-center box-shadow-v2-s3 box-shadow-v3-s3-child--hover">-->

				<!--					<i class="icon-calendar"></i>-->

				<!--				</div>-->

				<!--			</div>-->

				<!--			<div>-->

				<!--				<h2 class="text-size-18 text-bold-600">Working Hours</h2>-->

				<!--				<p class="m-0">Mon-Sat 09:00 - 19:00</p>-->

				<!--				<p class="m-0">Sun 10:00 - 13:00</p>-->

				<!--			</div>-->

				<!--		</div>-->

				<!--	</div>-->

				<!--</div>-->

			</div> <!-- /ROW -->

		</div> <!-- /CONTAINER -->

	</section> <!-- /SECTION -->



	<section class="bg-white pt-s5 pb-s5">

		<div class="container">

			<div class="mb-s4">

				<h6 class="text-bold-700 text-cherry">Intră în Touch</h6>

				<h2 class="text-bold-700 text-dark text-capitalize">Cu ce vă putem ajuta?</h2>

				<hr class="w--60 bs-solid bc-cherry bw-s5 bt-0 bl-0 br-0 mt-s2 ml-0">

			</div>

			<div class="row">

				<div class="col-lg-6 col-md-6">

					<form action="<?= base_url('Welcome/sendmail') ?>" method = "post">

						<div class="form-row">

							<div class="form-group col-md-6">

								<input name="name" required type="text" class="form-control bg-light text-size-12 pt-16 pb-16 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark" placeholder="Nume">

							</div>

							<div class="form-group col-md-6">

								<input name="from" required type="email" class="form-control bg-light text-size-12 pt-16 pb-16 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark" placeholder="E-mail">

							</div>

						</div>

						<div class="form-row">

							<div class="form-group col-md-12">

								<input name="subject" required type="text" class="form-control bg-light text-size-12 pt-16 pb-16 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark" placeholder="Subiect">

							</div>

						</div>

						<div class="form-group">

							<textarea name="message" required class="form-control bg-light text-size-12 pt-16 pb-16 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark" rows="5" placeholder="Mesajul dvs."></textarea>

						</div>

						<button type="submit" class="btn bg-cherry text-white text-shadow-s1 text-uppercase text-bold-600 text-size-s1 rounded-s5 pt-12 pb-12 pl-s3 pr-s3 mt-s1 mb-s1 btn-block">

						    Trimite mesaj</button>

					</form>

				</div>

				<div class="col-lg-6 col-md-6">

					<div class="bs-solid bc-v1-dark bw-s1 p-s1">

						<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->

						<div id="googlemaps" class="min-h--300">

						    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2841.9097408874204!2d26.06833111537452!3d44.57841527910046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b21b9cce10bbf3%3A0x79bc90220bb482a0!2sCalea%20Bucure%C8%99tilor%20236%2C%20Otopeni%20075100%2C%20Romania!5e0!3m2!1sen!2s!4v1647349023818!5m2!1sen!2s" 

						    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

						</div>

					</div>

				</div>

			</div> <!-- /ROW -->

		</div> <!-- /CONTAINER -->

	</section> <!-- /SECTION -->

    <?php include('layout/footer.php'); ?>