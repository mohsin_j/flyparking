<?php include('layout/header.php'); ?>

<style type="text/css">

	button#getquote {
		background: #61149e !important;
		color: white !important;
		width: auto;
	}

	.bg-v9-dark, .btn-v9-dark {
		background: transparent;
		background: linear-gradient(to bottom, rgb(97 20 158) 1%, rgba(0, 0, 0, 1) 100%) !important;
		background-position: initial !important;
		background-size: cover !important;
	}

	@media (max-width: 768px) {
		.hero-section-container-tl .text-center.mb-s2 {
			background: #61149e94;
			padding: 10px;
		}

		.animated-headline.type b.is-visible {
			font-size: 14px;
		}

	}

	.bg-dark-g, .btn-dark-g {
		background: linear-gradient(to bottom, rgb(97 20 158) 1%, rgba(0, 0, 0, 1) 100%) !important;
	}

	.main-header .logo img {
		max-height: 70px;
	}
</style>

<section class="pt-s5 pb-s5 overlay-wrap" data-image-src="assets/images/bg/bg-1.jpg">

	<div class="container overlay-container pt-s2 pb-s5">

		<div class="pb-s5">

			<div class="hero-section-container-tl w-100 h-100 d-flex justify-content-center align-items-center">

				<div class="text-center mb-s2">

					<div class="mt-s3 text-white">

						<h6>Gaseste un loc de parcare la cel mai bun pret</h6>

					</div>

					<div class="mt-s1 mb-s5 text-white">

						<h1 class="animated-headline letters type mb-s2 text-white text-size-20--xs text-size-30--sm">

								<span class="headline-wrapper text-cherry text-capitalize">

									<b class="is-visible">Parcare langa Aeroportul Otopeni</b>

									<b>Transfer gratuit aeroport</b>

									<b>Parcare supravegheata video</b>

									<b>Preturi mici</b>

								</span>

						</h1>

					</div>

				</div>

			</div>

		</div>

	</div> <!-- /CONTAINER -->

	<div class="overlay bg-v9-dark"></div>

</section> <!-- /SECTION -->
<section class="bg-v5-light pt-s3 pb-s3">

	<form method="POST" id="form">

		<div class="quote-setting container">

			<div class="bg-white p-s3 bs-solid bc-light bw-s1 rounded-s2 box-shadow-v1-s5">

				<?php if ($this->session->flashdata('success')) { ?>

					<div class="alert alert-success">

						<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>.

					</div>
				<?php } ?>

				<div class="row">
					<div class="col-lg-3">
						<label><?= $this->lang->line('arrival_hour'); ?></label>
						<div class="form-row">
							<select id="arrival-hour" class="form-control">
								<option data-select2-id="2">00:00</option>
								<option data-select2-id="11">00:30</option>
								<option data-select2-id="12">01:00</option>
								<option data-select2-id="13">01:30</option>
								<option data-select2-id="14">02:00</option>
								<option data-select2-id="15">02:30</option>
								<option data-select2-id="16">03:00</option>
								<option data-select2-id="17">03:30</option>
								<option data-select2-id="18">04:00</option>
								<option data-select2-id="19">04:30</option>
								<option data-select2-id="20">05:00</option>
								<option data-select2-id="21">05:30</option>
								<option data-select2-id="22">06:00</option>
								<option data-select2-id="23">06:30</option>
								<option data-select2-id="24">07:00</option>
								<option data-select2-id="25">07:30</option>
								<option data-select2-id="26">08:00</option>
								<option data-select2-id="27">08:30</option>
								<option data-select2-id="28">09:00</option>
								<option data-select2-id="29">09:30</option>
								<option data-select2-id="30">10:00</option>
								<option data-select2-id="31">10:30</option>
								<option data-select2-id="32">11:00</option>
								<option data-select2-id="33">11:30</option>
								<option data-select2-id="34">12:00</option>
								<option data-select2-id="35">12:30</option>
								<option data-select2-id="36">13:00</option>
								<option data-select2-id="37">13:30</option>
								<option data-select2-id="38">14:00</option>
								<option data-select2-id="39">14:30</option>
								<option data-select2-id="40">15:00</option>
								<option data-select2-id="41">15:30</option>
								<option data-select2-id="42">16:00</option>
								<option data-select2-id="43">16:30</option>
								<option data-select2-id="44">17:00</option>
								<option data-select2-id="45">17:30</option>
								<option data-select2-id="46">18:00</option>
								<option data-select2-id="47">18:30</option>
								<option data-select2-id="48">19:00</option>
								<option data-select2-id="49">19:30</option>
								<option data-select2-id="50">20:00</option>
								<option data-select2-id="51">20:30</option>
								<option data-select2-id="52">21:00</option>
								<option data-select2-id="53">21:30</option>
								<option data-select2-id="54">22:00</option>
								<option data-select2-id="55">22:30</option>
								<option data-select2-id="56">23:00</option>
								<option data-select2-id="57">23:30</option>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<label><?= $this->lang->line('arrival_departure_date'); ?></label>
						<div class="form-row">
							<input type="text" id="arrival-departure-date" value="01/01/2022 - 01/15/2022"/>
						</div>
					</div>
					<div class="col-lg-3">
						<label><?= $this->lang->line('departure_hour'); ?></label>
						<div class="form-row">
							<select id="departure-hour" class="form-control">
								<option data-select2-id="2">00:00</option>
								<option data-select2-id="11">00:30</option>
								<option data-select2-id="12">01:00</option>
								<option data-select2-id="13">01:30</option>
								<option data-select2-id="14">02:00</option>
								<option data-select2-id="15">02:30</option>
								<option data-select2-id="16">03:00</option>
								<option data-select2-id="17">03:30</option>
								<option data-select2-id="18">04:00</option>
								<option data-select2-id="19">04:30</option>
								<option data-select2-id="20">05:00</option>
								<option data-select2-id="21">05:30</option>
								<option data-select2-id="22">06:00</option>
								<option data-select2-id="23">06:30</option>
								<option data-select2-id="24">07:00</option>
								<option data-select2-id="25">07:30</option>
								<option data-select2-id="26">08:00</option>
								<option data-select2-id="27">08:30</option>
								<option data-select2-id="28">09:00</option>
								<option data-select2-id="29">09:30</option>
								<option data-select2-id="30">10:00</option>
								<option data-select2-id="31">10:30</option>
								<option data-select2-id="32">11:00</option>
								<option data-select2-id="33">11:30</option>
								<option data-select2-id="34">12:00</option>
								<option data-select2-id="35">12:30</option>
								<option data-select2-id="36">13:00</option>
								<option data-select2-id="37">13:30</option>
								<option data-select2-id="38">14:00</option>
								<option data-select2-id="39">14:30</option>
								<option data-select2-id="40">15:00</option>
								<option data-select2-id="41">15:30</option>
								<option data-select2-id="42">16:00</option>
								<option data-select2-id="43">16:30</option>
								<option data-select2-id="44">17:00</option>
								<option data-select2-id="45">17:30</option>
								<option data-select2-id="46">18:00</option>
								<option data-select2-id="47">18:30</option>
								<option data-select2-id="48">19:00</option>
								<option data-select2-id="49">19:30</option>
								<option data-select2-id="50">20:00</option>
								<option data-select2-id="51">20:30</option>
								<option data-select2-id="52">21:00</option>
								<option data-select2-id="53">21:30</option>
								<option data-select2-id="54">22:00</option>
								<option data-select2-id="55">22:30</option>
								<option data-select2-id="56">23:00</option>
								<option data-select2-id="57">23:30</option>
							</select>
						</div>
					</div>
					<div class="col-lg-2">

						<div class="form-row">

							<div class="form-group col-md-12">

								<button id="getquote" type="button"
										class="btn bg-cherry text-white text-shadow-s1 text-uppercase text-bold-600 text-size-s1 mt-30 rounded-s5 mb-s1 btn-block">
									Calculeaza pret
								</button>

							</div>

						</div>

					</div>
				</div>

				<!--<div class="row">
					<div class="col-lg-5">

						<label><?/*= $this->lang->line('departure_date'); */?></label>

						<div class="form-row">

							<div class="form-group col-md-12">

								<input name="data_retur" id="checkin"
									   value="<?/*= date('Y-m-d\TH:i', strtotime(date('Y-m-d')." 12:00:00")); */?>"
									   type="datetime-local" class="form-control text-size-12 rounded-s5 bc-v1-dark">

							</div>

						</div>

					</div>

					<div class="col-lg-5">

						<label><?/*= $this->lang->line('arrival_date'); */?></label>

						<div class="form-row">

							<div class="form-group col-md-12">
								<input name="data_plecare" id="checkout" value="<?/*= date('Y-m-d\TH:i',
																						 strtotime(date('Y-m-d',
																										   (strtotime('+9 day',
																													  strtotime(date('Y-m-d')))))." 12:00:00")); */?>"
									   type="datetime-local" class="form-control text-size-12 rounded-s5 bc-v1-dark">

							</div>
						</div>

					</div>

					<div class="col-lg-2">

						<div class="form-row">

							<div class="form-group col-md-12">

								<button id="getquote2" type="button"
										class="btn bg-cherry text-white text-shadow-s1 text-uppercase text-bold-600 text-size-s1 mt-30 rounded-s5 mb-s1 btn-block">
									Calculeaza pret
								</button>

							</div>

						</div>

					</div>

				</div>-->
			</div> <!-- /CONTAINER -->

			<!--hidden form-->

			<div style="display:none" id="save_form" class="container pt-s4">
				<div class="bg-white p-20">

					<div class="row">

						<div class="col-lg-8 col-md-12">
							<div class="row" v-if="record.booking_edit == 0">
								<div class="col-md-12">

									<h4>Informatiile dvs</h4>

									<div class="row">

										<div class="form-group col-md-6">

											<label class="text-size-12 text-bold-500"><?= $this->lang->line('name_surname'); ?>
												<span class="text-danger">*</span></label>

											<input id="name" type="text" name="nume"
												   class="form-control bg-light text-size-12 pt-8 pb-8 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark">

										</div>

										<div class="form-group col-md-6">

											<label class="text-size-12 text-bold-500"><?= $this->lang->line('email'); ?>
												<span class="text-danger">*</span></label>

											<input id="email" type="email" name="email"
												   class="form-control bg-light text-size-12 pt-8 pb-8 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark">

										</div>

									</div>
									<div class="row">

										<div class="form-group col-md-6">

											<label class="text-size-12 text-bold-500">

												<?= $this->lang->line('phone'); ?><span
														class="text-danger">*</span></label>

											<input id="phone" name="telefon" type="text"
												   class="form-control bg-light text-size-12 pt-8 pb-8 pl-20 pr-20 text-bold-600 rounded-0 bc-v1-dark">

										</div>
									</div>

									<hr>

									<div class="row">

										<div class="form-group col-md-6">

											<div>

												<?php

												foreach ($services->result() as $service) {
													echo '<div class="position-relative form-check"><label class="form-check-label"><input

type="checkbox" value="'.$service->ID.'" name="services[]" class="form-check-input services"> '.$service->name.' - '.$service->value.' Ron</label></div>';
												}

												?>

											</div>

											<div class="position-relative form-check"><label
														class="form-check-label"><input type="checkbox"
																						id="request_invoice"
																						name="request_invoice"
																						class="form-check-input"> <?= $this->lang->line('request_invoice_msg'); ?>
												</label></div>

										</div>
									</div>
									<div style="display:none" id="cui_row" class="row">

										<div class="form-group col-md-6">

											<label><?= $this->lang->line('cui_company'); ?></label>

											<input type="text" id="cui" name="cui" class="form-control"/>

										</div>

									</div>
								</div>

							</div>
						</div>
						<div class="bg-white">

							<table class="w-100 text-size-12">
								<tr class="bs-solid bc-light bw-s1 mt-0 bl-0 br-0">

									<td class="p-8"><?= $this->lang->line('departure_date'); ?></td>

									<td id="checkin_td" class="p-8 text-right"></td>

								</tr>
								<tr class="bs-solid bc-light bw-s1 mt-0 bl-0 br-0">

									<td class="p-8"><?= $this->lang->line('arrival_date'); ?></td>

									<td id="checkout_td" class="p-8 text-right"></td>

								</tr>
								<tr class="bs-solid bc-light bw-s1 mt-0 bl-0 br-0">

									<td class="p-8"><?= $this->lang->line('days'); ?></td>

									<td id="days_td" class="p-8 text-right"></td>

								</tr>

								<tr class="bs-solid bc-light bw-s1 mt-0 bl-0 br-0">

									<td class="p-8"><?= $this->lang->line('at_location'); ?></td>

									<td id="location_td" class="p-8 text-right text-size-18 text-bold-600"></td>

								</tr>

								<tr class="bs-solid bc-light bw-s1 mt-0 bl-0 br-0">

									<td class="p-8"><?= $this->lang->line('online_payment'); ?></td>

									<td id="online_td" class="p-8 text-right text-size-18 text-bold-600"></td>

								</tr>

							</table>

							<hr>

							<div class="row mt-10">

								<div class="col-md-12">

									<input id="agree" type="checkbox"> <?= $this->lang->line('agreement_msg'); ?>
									<span></span>

								</div>

							</div>

							<input type="hidden" name="pay_online" id="pay_online"/>

							<button id="save_reservation"
									class="btn bg-cherry text-white text-shadow-s1 text-uppercase text-bold-600 text-size-s1 rounded-0 pt-8 pb-8 pl-20 pr-20 box-shadow-v1-s3--hover mt-15"><?= $this->lang->line('booking_btn'); ?></button>

						</div>

					</div>

					<div class="row">

						<div id="err_msg" style="display:none;border:1px solid red" class="col-md-12 text-center">

							<h4 class="text-danger mt-1"><?= $this->lang->line('onlinereservation_form_err_msg'); ?></h4>

						</div>

					</div>

				</div>
			</div>
		</div> <!-- /CONTAINER -->

		<!-- form end-->

		<div class="container pt-s4">

			<div class="row">

				<div class="col-lg-4 col-md-12 col-sm-12">

					<div class="mt-s2 mb-s2 box-shadow-parent--hover">

						<div class="d-flex align-items-center mb-s2">

							<div class="mr-s2">

								<div class="text-size-s1 rounded-s3 bg-cherry text-white text-center h--40 w--40 d-flex justify-content-center align-items-center box-shadow-v2-s3-child--hover">

									<i class="icon-paper-plane"></i>

								</div>

							</div>

							<div>

								<h2 class="text-size-18 text-bold-600 m-0">Preturi mici, Non-Stop</h2>

							</div>

						</div>

						<p class="m-0">Facem tot posibilul sa iti asiguram cele mai mici preturi pe tot parcursul
							anului. Avem parcarea deschisa pentru tine 27/7, non-stop.</p>

					</div>

				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">

					<div class="mt-s2 mb-s2 box-shadow-parent--hover">

						<div class="d-flex align-items-center mb-s2">

							<div class="mr-s2">

								<div class="text-size-s1 rounded-s3 bg-cherry text-white text-center h--40 w--40 d-flex justify-content-center align-items-center box-shadow-v2-s3-child--hover">

									<i class="icon-equalizer"></i>

								</div>

							</div>

							<div>

								<h2 class="text-size-18 text-bold-600 m-0">Servicii incluse</h2>

							</div>

						</div>

						<p class="m-0">Transporturile Parcare - Aeroport si Aeroport - Parcare sunt gratuite pentru
							toate persoanele cu care ai venit in masina.</p>

					</div>

				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">

					<div class="mt-s2 mb-s2 box-shadow-parent--hover">

						<div class="d-flex align-items-center mb-s2">

							<div class="mr-s2">

								<div class="text-size-s1 rounded-s3 bg-cherry text-white text-center h--40 w--40 d-flex justify-content-center align-items-center box-shadow-v2-s3-child--hover">

									<i class="icon-screen-desktop"></i>

								</div>

							</div>

							<div>

								<h2 class="text-size-18 text-bold-600 m-0">Parcare supravegheata video</h2>

							</div>

						</div>

						<p class="m-0">Iti poti lasa masina in siguranta la noi. Avem camere de supraveghere si personal
							prezent tot timpul. Astfel, poti calatori linistit.</p>

					</div>

				</div>

			</div> <!-- /ROW -->

		</div> <!-- /CONTAINER -->

	</form>

</section> <!-- /SECTION -->
<section class="bg-dark-g pt-s5 pb-s5">

	<div class="container">

		<div class="mb-80 text-center">

			<h6 class="text-bold-700 text-cherry">Care sunt pasii?</h6>

			<h2 class="text-bold-700 text-white">Parcheaza masina langa aeroport in 3 pasi simpli</h2>

			<div class="mb-s3">

				<hr class="mb-0 bc-v1-dark">

				<hr class="w--60 bs-solid bc-cherry bw-s5 bt-0 bl-0 br-0 mt-0">

			</div>

		</div>

		<div class="row">

			<div class="col-lg-4 col-md-12 col-sm-12">

				<div class="mt-s2 mb-s2 mb-60--md mb-60--sm mb-60--xs box-shadow-parent--hover mt--parent--hover">

					<div class="bg-white text-center pt-s4 bs-solid bc-light bw-s1 position-relative">

						<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">

							<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover"
								 src="<?= base_url('assets/site_assets/images/steps/step-3.png'); ?>"
								 alt="Departure & Arrival Details">

						</div>

						<div class="p-s2 mt-s2 mb-s2">

							<span class="text-cherry text-bold-600">Pasul 1</span>

							<h5 class="mt-s1 mb-0 text-bold-600">Creezi o rezervare pe website si platesti online sau
								cash</h5>

						</div>

					</div>

				</div>

			</div>

			<div class="col-lg-4 col-md-12 col-sm-12">

				<div class="mt-s2 mb-s2 mb-60--md mb-60--sm mb-60--xs box-shadow-parent--hover mt--parent--hover">

					<div class="bg-white text-center pt-s4 bs-solid bc-light bw-s1 position-relative">

						<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">

							<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover"
								 src="<?= base_url('assets/site_assets/images/steps/step-2.png'); ?>"
								 alt="Your & Vehicle Detail">

						</div>

						<div class="p-s2 mt-s2 mb-s2">

							<span class="text-cherry text-bold-600">Pasul 2</span>

							<h5 class="mt-s1 mb-0 text-bold-600">Vii la parcare cu 30 minute inainte</h5>

						</div>

					</div>

				</div>

			</div>

			<div class="col-lg-4 col-md-12 col-sm-12">

				<div class="mt-s2 mb-s2 mb-60--md mb-60--sm mb-60--xs box-shadow-parent--hover mt--parent--hover">

					<div class="bg-white text-center pt-s4 bs-solid bc-light bw-s1 position-relative">

						<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">

							<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover"
								 src="<?= base_url('assets/site_assets/images/steps/step-1.png'); ?>"
								 alt="Payment Detail">

						</div>

						<div class="p-s2 mt-s2 mb-s2">

							<span class="text-cherry text-bold-600">Pasul 3</span>

							<h5 class="mt-s1 mb-0 text-bold-600">Transportul la aeroport dureaza 4 minute</h5>

						</div>

					</div>

				</div>

			</div>

		</div> <!-- /ROW -->

	</div> <!-- /CONTAINER -->

</section> <!-- /SECTION -->
<section class="bg-white pt-s5 pb-s5">

	<div class="container">

		<div class="row">

			<div class="col-lg-6 col-md-12 col-sm-12">

				<div class="row">

					<div class="col-md-12">

						<div>

							<h4 class="text-bold-700">Recenzii</h4>

							<hr class="mt-0 mb-s1 bc-v1-dark">

						</div>

						<div class="row">

							<div class="col-md-12">

								<div class="owl-carousel owl-theme owl-nav-bg show-nav-title"
									 data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 1}, '979': {'items': 1}, '1199': {'items': 1}}, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">

									<div>

										<div class="mt-s2 mb-s2 box-shadow-parent--hover">

											<div>

												<p class="mb-s1 text-size-s2 font-italic">Am gasit usor parcarea si am
													ajuns la aeroport in 10 minute de cand am lasat masina. Masinile de
													transfer sunt confortabile, iar personalul m-a ajutat cu
													bagajele.</p>

											</div>

											<div class="d-flex mt-s2 ml-20">
												<div>

													<span class="text-bold-600 text-dark d-block">Amalia Bot</span>
												</div>

											</div>

										</div>

									</div>

									<div>

										<div class="mt-s2 mb-s2 box-shadow-parent--hover">

											<div>

												<p class="mb-s1 text-size-s2 font-italic">Parcarea este noua, bine
													amenanjata si supravegheata video. Initial o ratasem. Atentie,
													parcarea este fix dupa podul de la Aeroport. Folositi Waze.</p>

											</div>

											<div class="d-flex mt-s2 ml-20">
												<div>

													<span class="text-bold-600 text-dark d-block">Andrei Enescu</span>
												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div> <!-- /ROW -->

					</div>

				</div>

			</div>

			<div class="col-lg-6 col-md-12 col-sm-12">

				<div class="mb-s2 mt-s3--md mt-s3--sm mt-s3--xs">

					<h4 class="text-bold-700">Intrebari frecvent intalnite</h4>

					<hr class="mt-0 mb-s1 bc-v1-dark">

				</div>

				<div class="accordion" id="accordion-acc7">

					<div class="card border-none bg-none">

						<div id="headingOne-acc7-1">

							<h5 class="mb-s1">

								<button class="btn btn-link btn-block text-size-s1 text-bold-600 text-dark rounded-0 text-left bg-v5-light bg-cherry--active text-white--active text-cherry--hover active collapsed pt-s1 pl-s2 pr-s2 pb-s1"
										type="button" data-toggle="collapse" data-target="#collapseOne-acc7-1"
										aria-expanded="true" aria-controls="collapseOne-acc7-1">

										<span class="mr-s1">

											<span>

												<i class="fa fa-plus hide"></i>

												<i class="fa fa-minus show"></i>

											</span>

										</span>

									Ce se intampla dupa ce las masina?

								</button>

							</h5>

						</div>
						<div id="collapseOne-acc7-1" class="collapse show" aria-labelledby="headingOne-acc7-1"
							 data-parent="#accordion-acc7">

							<div class="card-body pt-s2 pb-s2">

								<p>Va sfatuim sa ajungeti cu 20-30 minute inainte de a trebui sa ajungeti la aeroport.
									Dupa ce parcati masina, trebuie sa va completam datele in sistem, sa va generam un
									contract si sa va transferam catre aeroport.</p>

							</div>

						</div>

					</div>

					<div class="card border-none bg-none">

						<div id="headingTwo-acc7-2">

							<h5 class="mb-s1">

								<button class="btn btn-link btn-block text-size-s1 text-bold-600 text-dark rounded-0 text-left bg-v5-light bg-cherry--active text-white--active text-cherry--hover collapsed pt-s1 pl-s2 pr-s2 pb-s1"
										type="button" data-toggle="collapse" data-target="#collapseTwo-acc7-2"
										aria-expanded="false" aria-controls="collapseTwo-acc7-2">

										<span class="mr-s1">

											<span>

												<i class="fa fa-plus hide"></i>

												<i class="fa fa-minus show"></i>

											</span>

										</span>

									Cat costa parcarea?

								</button>

							</h5>

						</div>

						<div id="collapseTwo-acc7-2" class="collapse" aria-labelledby="headingTwo-acc7-2"
							 data-parent="#accordion-acc7">

							<div class="card-body pt-s2 pb-s2">

								<p>Cu cat parchezi mai mult timp, cu atat devine mai ieftin per zi. Preturile sunt
									dinamice, iar modalitatea cea mai buna de a calcula pretul este sa folositi
									formularul din website, selectand data de sosire si data de plecare din parcare.</p>

							</div>

						</div>

					</div>

					<div class="card border-none bg-none">

						<div id="headingThree-acc7-3">

							<h5 class="mb-s1">

								<button class="btn btn-link btn-block text-size-s1 text-bold-600 text-dark rounded-0 text-left bg-v5-light bg-cherry--active text-white--active text-cherry--hover collapsed pt-s1 pl-s2 pr-s2 pb-s1"
										type="button" data-toggle="collapse" data-target="#collapseThree-acc7-3"
										aria-expanded="false" aria-controls="collapseThree-acc7-3">

										<span class="mr-s1">

											<span>

												<i class="fa fa-plus hide"></i>

												<i class="fa fa-minus show"></i>

											</span>

										</span>

									Cat de departe este parcarea de Aeroportul Otopeni?

								</button>

							</h5>

						</div>

						<div id="collapseThree-acc7-3" class="collapse" aria-labelledby="headingThree-acc7-3"
							 data-parent="#accordion-acc7">

							<div class="card-body pt-s2 pb-s2">

								<p>Odata ajunsi, noi va transferam gratuit la Aeroport. Drumul dureaza 1-2 minute. Ne
									aflam la 500 metri de Aeroport.</p>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div> <!-- /ROW -->

	</div> <!-- /CONTAINER -->

</section> <!-- /SECTION -->

<?php include('layout/footer.php'); ?>

<script>

	$(document).ready(function () {

		let base_url = $('#base_url').val();
		let totallocation = 0;
		let totalonline = 0;
		let services = [];
		let service_price = [];
		let service_arr = [];
		let selected_price = [];
		let arrival_date = '2022-01-01';
		let departure_date = '2022-01-15';

		$('#arrival-departure-date').daterangepicker({
			"autoApply": true,
			opens: 'center',
			startDate: moment().startOf('hour'),
			endDate: moment().startOf('hour').add(32, 'hour'),
		}, function(start, end, label) {
			arrival_date = start.format('YYYY-MM-DD');
			departure_date = end.format('YYYY-MM-DD');

			getquote();
		});

		$('#getquote').click(function () {

			getquote();

			$('#getquote').hide();

		});

		$('#arrival-hour, #departure-hour').change(function () {
			getquote();
		});

		$('[name="request_invoice"]').click(function () {

			if ($(this).prop("checked") == true) {

				$('#cui_row').show();

			} else if ($(this).prop("checked") == false) {

				$('#cui_row').hide();

			}

		});

		function getquote() {

			totallocation = 0;
			totalonline = 0;

			let arrival_hour = $("#arrival-hour").val();
			let departure_hour = $("#departure-hour").val();

			let checkin = arrival_date+"T"+arrival_hour;
			let checkout = departure_date+"T"+departure_hour;

			$.ajax({

				method: "POST",

				url: base_url + "Main_controller/calc_online_price",

				data: {"key": "parking", checkin: checkin, checkout: checkout},

				success: function (data) {

					const obj = JSON.parse(data);

					if (obj.success != false) {

						$('#save_form').show();

						let options = obj.options;

						let html = '';

						for (i = 0; i < options.length; i++) {

							services.push(options[i].ID);

							service_price.push(options[i].price);

						}
						$('#option_row').html(html);

						//assigning value to table

						$('#checkin_td').html(checkin);

						$('#checkout_td').html(checkout);

						$('#days_td').html(obj.prices[0].days);

						totallocation = obj.prices[0].total;

						totalonline = obj.prices[1].total;

						gettotal(false);
					}

					// console.log(obj.prices[0]);

				}
			})

		}

		$('.services').click(function () {

			let index = services.indexOf($(this).val());

			let p = parseInt(service_price[index]);
			if ($(this).prop("checked") == true) {

				service_arr.push($(this).val());

				selected_price.push(p);

				// totallocation+=p;

				// totalonline+=p;

				gettotal(false);

			} else if ($(this).prop("checked") == false) {

				var myIndex = service_arr.indexOf($(this).val());

				if (myIndex !== -1) {

					service_arr.splice(myIndex, 1);

					selected_price.splice(myIndex, 1);

				}

				// totallocation-=p;

				// totalonline-=p;

				gettotal(false);

			}

		});

		function gettotal(value) {
			let sum = selected_price.reduce(function (a, b) {

				return a + b;

			}, 0);

			let tl = 0;

			let to = 0;

			if (value == false) {

				tl = totallocation + sum;

				to = totalonline + sum;

			}

			// else{

			// 	tl=totallocation - value;

			// 	to = totalonline- value;
// }
			$('#location_td').html(tl + " Ron");

			$('#online_td').html(to + " Ron");

		}

		function savedata(formdata) {
			$.ajax({

				method: "POST",

				url: base_url + "Main_controller/save_reservation_post",

				data: {
					key: "parking",
					data: formdata,
					checkin: $('#checkin').val(),
					checkout: $('#checkout').val(),
					"services": service_arr
				},

				success: function (data) {

					let obj = JSON.parse(data);

					console.log(obj.success);

					if (obj.success == true) {
						window.location.href = base_url + "Welcome/redirect";

					}

				}

			})

		}

		$('#save_reservation').click(function (e) {
			e.preventDefault();

//check empty check

			if ($('#name').val() == "") {

				$('#err_msg').show();

				return;

			} else if ($('#email').val() == "") {

				$('#err_msg').show();

				return;

			} else if ($('#phone').val() == "") {

				$('#err_msg').show();

				return;

			} else if ($('#agree').prop("checked") == false) {

				$('#err_msg').show();

				return;

			} else {

				$('#err_msg').hide();

			}
			var data = $("form").serialize().split("&");
			var obj = {};

			for (var key in data) {

				console.log(data[key]);

				obj[data[key].split("=")[0]] = data[key].split("=")[1];

			}
			savedata(obj);
		});

	});
</script>
