<?php include("layout/header.php");?>
<!-- BREADCRUMB -->
<section class="bg-cherry pt-s3 pb-s3 text-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="d-lg-flex text-center">
						<div class="align-self-center mb-0--lg mb-s1--md mb-s1--sm mb-s1--xs">
							<h3 class="text-bold-400 text-size-s2 m-0 text-white">Frequently Asked Questions</h3>
						</div>
						<div class="ml-auto align-self-center">
							<ol class="breadcrumb d-flex justify-content-center m-0 bg-none text-size-s1 p-0">
								<li class="breadcrumb-item"><a class="text-white" href="index.html">Home</a></li>
								<li class="breadcrumb-item" aria-current="page">FAQs</li>
							</ol>
						</div>
					</div>
				</div>
			</div> <!-- ROW -->
		</div> <!-- /CONTAINER -->
	</section> <!-- /SECTION -->
	<!-- /BREADCRUMB -->

	<section class="bg-white pt-s5 pb-s5">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-12">
					<div class="mt-s2 mb-s2">
						<div class="mb-s3">
							<h6 class="text-bold-700 text-cherry">FAQs</h6>
							<h3 class="text-bold-700 text-dark text-capitalize">Get perfect answers from us !</h3>
						</div>
						<div class="accordion" id="accordion-acc16">
							<div class="card border-none bg-none mb-s2">
								<div id="headingOne-acc16-1">
									<h5 class="mb-0">
										<button class="btn btn-link btn-block text-uppercase text-size-s1 text-bold-600 text-dark rounded-s5 text-left bs-solid bc-cherry--active bw-s4 text-cherry--active text-cherry--hover active collapsed pt-s1 pb-s1" type="button" data-toggle="collapse" data-target="#collapseOne-acc16-1" aria-expanded="true" aria-controls="collapseOne-acc16-1">
											<span class="mr-s1">
												<span>
													<i class="fa fa-plus hide"></i>
													<i class="fa fa-minus show"></i>
												</span>
											</span>
											consectetur adipiscing elit vivamus dapibus
										</button>
									</h5>
								</div>

								<div id="collapseOne-acc16-1" class="collapse show" aria-labelledby="headingOne-acc16-1" data-parent="#accordion-acc16">
									<div class="card-body pt-s3 pb-s3">
										Nulla ultrices enim at erat scelerisque, id euismod sem consequat. Pellentesque ac pulvinar diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In consectetur mauris ac elit finibus, tempor gravida nibh posuere. Nullam lobortis maximus sapien sed euismod. Integer tempor justo dolor.
									</div>
								</div>
							</div>
							<div class="card border-none bg-none mb-s2">
								<div id="headingTwo-acc16-2">
									<h5 class="mb-0">
										<button class="btn btn-link btn-block text-uppercase text-size-s1 text-bold-600 text-dark rounded-s5 text-left bs-solid bc-cherry--active bw-s4 text-cherry--active text-cherry--hover collapsed pt-s1 pb-s1" type="button" data-toggle="collapse" data-target="#collapseTwo-acc16-2" aria-expanded="false" aria-controls="collapseTwo-acc16-2">
											<span class="mr-s1">
												<span>
													<i class="fa fa-plus hide"></i>
													<i class="fa fa-minus show"></i>
												</span>
											</span>
											mollis ornare integer pharetra malesuada
										</button>
									</h5>
								</div>
								<div id="collapseTwo-acc16-2" class="collapse" aria-labelledby="headingTwo-acc16-2" data-parent="#accordion-acc16">
									<div class="card-body pt-s3 pb-s3">
										Nulla ultrices enim at erat scelerisque, id euismod sem consequat. Pellentesque ac pulvinar diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In consectetur mauris ac elit finibus, tempor gravida nibh posuere. Nullam lobortis maximus sapien sed euismod. Integer tempor justo dolor.
									</div>
								</div>
							</div>
							<div class="card border-none bg-none mb-s2">
								<div id="headingThree-acc16-3">
									<h5 class="mb-0">
										<button class="btn btn-link btn-block text-uppercase text-size-s1 text-bold-600 text-dark rounded-s5 text-left bs-solid bc-cherry--active bw-s4 text-cherry--active text-cherry--hover collapsed pt-s1 pb-s1" type="button" data-toggle="collapse" data-target="#collapseThree-acc16-3" aria-expanded="false" aria-controls="collapseThree-acc16-3">
											<span class="mr-s1">
												<span>
													<i class="fa fa-plus hide"></i>
													<i class="fa fa-minus show"></i>
												</span>
											</span>
											Aliquam aliquam orci non placerat vestibulum
										</button>
									</h5>
								</div>
								<div id="collapseThree-acc16-3" class="collapse" aria-labelledby="headingThree-acc16-3" data-parent="#accordion-acc16">
									<div class="card-body pt-s3 pb-s3">
										Nulla ultrices enim at erat scelerisque, id euismod sem consequat. Pellentesque ac pulvinar diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In consectetur mauris ac elit finibus, tempor gravida nibh posuere. Nullam lobortis maximus sapien sed euismod. Integer tempor justo dolor.
									</div>
								</div>
							</div>
							<div class="card border-none bg-none mb-s2">
								<div id="headingTwo-acc16-4">
									<h5 class="mb-0">
										<button class="btn btn-link btn-block text-uppercase text-size-s1 text-bold-600 text-dark rounded-s5 text-left bs-solid bc-cherry--active bw-s4 text-cherry--active text-cherry--hover collapsed pt-s1 pb-s1" type="button" data-toggle="collapse" data-target="#collapseTwo-acc16-4" aria-expanded="false" aria-controls="collapseTwo-acc16-4">
											<span class="mr-s1">
												<span>
													<i class="fa fa-plus hide"></i>
													<i class="fa fa-minus show"></i>
												</span>
											</span>
											commodo consectetur tellus eu ornare
										</button>
									</h5>
								</div>
								<div id="collapseTwo-acc16-4" class="collapse" aria-labelledby="headingTwo-acc16-4" data-parent="#accordion-acc16">
									<div class="card-body pt-s3 pb-s3">
										Nulla ultrices enim at erat scelerisque, id euismod sem consequat. Pellentesque ac pulvinar diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In consectetur mauris ac elit finibus, tempor gravida nibh posuere. Nullam lobortis maximus sapien sed euismod. Integer tempor justo dolor.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-12">
					<div class="mt-s2 mb-s2">
						<div class="mb-s3">
							<h6 class="text-bold-700 text-cherry">Question Us</h6>
							<h3 class="text-bold-700 text-dark">Have You Any Question?</h3>
						</div>
						<div class="p-s3 bg-white box-shadow-v1-s3 bs-solid bc-v1-dark bw-s5">
							<form>
								<div class="form-row">
									<div class="form-group col-md-12">
										<input type="text" class="form-control bg-light text-size-12 pt-10 pb-10 pl-15 pr-15 text-bold-600 rounded-0 bc-v1-dark" placeholder="Name">
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-12">
										<input type="email" class="form-control bg-light text-size-12 pt-10 pb-10 pl-15 pr-15 text-bold-600 rounded-0 bc-v1-dark" placeholder="Email">
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-12">
										<input type="text" class="form-control bg-light text-size-12 pt-10 pb-10 pl-15 pr-15 text-bold-600 rounded-0 bc-v1-dark" placeholder="Subject">
									</div>
								</div>
								<div class="form-group">
									<textarea class="form-control bg-light text-size-12 pt-10 pb-10 pl-15 pr-15 text-bold-600 rounded-0 bc-v1-dark" rows="5" placeholder="Your Message"></textarea>
								</div>
								<button type="submit" class="btn bg-cherry text-white text-shadow-s1 text-uppercase text-bold-600 text-size-s1 rounded-0 pt-12 pb-12 pl-s3 pr-s3 mt-s1 mb-s1">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div> <!-- /ROW -->
		</div> <!-- /CONTAINER -->
	</section> <!-- /SECTION -->

<?php include("layout/footer.php");?>