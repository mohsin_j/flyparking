<!DOCTYPE html>

<html lang="ro">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Fly Parking - Parcare termen lung Aeroport Otopeni</title>



	<link rel="shortcut icon" href="<?= base_url('assets/site_assets/images/logo/favicon.png');?>">

	<link rel="apple-touch-icon" href="<?= base_url('assets/site_assets/images/logo/apple-touch-icon.png');?>">



	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">

<!--	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/bootstrap.min.css');?>">



	<link rel="stylesheet" href="<?= base_url('assets/site_assets/vendors/menu/src/main.menu.css');?>">



	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/style.css');?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/utilities.css');?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/colors.css');?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/colors-gh.css');?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/css/colors-g.css');?>">



	<!-- CAROUSEL -->

	<link rel="stylesheet" href="<?= base_url('assets/site_assets/vendors/owl.carousel/owl.carousel.min.css');?>">

	<link rel="stylesheet" href="<?= base_url('assets/site_assets/vendors/owl.carousel/owl.theme.default.min.css');?>">



	<!-- ANIMATE -->

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/vendors/animate/animate.css');?>">



	<!-- ANIMATED HEADLINES -->

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets/vendors/animated-headline/css/style.css');?>">



	<!-- FANCY BOX -->

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/site_assets//vendors/fancybox/jquery.fancybox.min.css');?>">

	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style>
	span.text-cherry.text-bold-600 {
		color: #61149e !important;
	}
	.bc-white {
		filter: hue-rotate(561deg);
	}
	footer p.m-0, footer a {
		color: white !important;
	}
	a.text-cherry {
		color: white !important;
	}
	footer.footer {
		color: white !important;
	}
	.bg-dark, .btn-dark {
		background: #60149d !important;
	}
	img.full-width.max-width-140.m-right-10 {
		max-width: 200px;
	}

	.btn-color{

		color:#000000 !important;

	}

	.small-text{

		font-size:14px;

	}

	.bg-cherry{color:black !important;}

	select#lang {
	    background: #f8c201;
	    margin-top: -11px;
	}

</style>

</head>

<body>

<input type="hidden" id="base_url" value="<?= base_url(); ?>"/>

	<div class="main-header header-shadow cherry-header">

	    <!-- <div class="bg-cherry">

	        <div class="container">

	            <div class="row">

	                <div class="col-lg-6 col-md-12">

	                    <div class="btn-color mt-10 text-center--md text-center--sm text-center--xs">

	                        <b class="small-text">Helpline 0123456789</b>

	                    </div>

	                </div>

	                <div class="col-lg-6 col-md-12">

	                    <div class="text-right text-center--md text-center--sm text-center--xs">

	                        <span class="mt-8 pl-20 pr-20 d-inline-block">

	                            Welcome <span class="text-bold-600">Parking</span>

	                        </span>

	                        <a class="btn btn-white text-cherry btn-color text-size-11 text-bold-600 mt-5 mb-5 mt-10--md mb-10--md mt-10--sm mb-10--sm mt-10--xs mb-10--xs text-uppercase" href="my-bookings.html"><i class="zmdi zmdi-account-o text-size-15 mr-6 float-left mt-1"></i> My Bookings</a>

	                        <a class="btn btn-white text-cherry btn-color text-size-11 text-bold-600 mt-5 mb-5 mt-10--md mb-10--md mt-10--sm mb-10--sm mt-10--xs mb-10--xs text-uppercase" href="#"><i class="zmdi zmdi-power text-size-15 mr-6 float-left mt-1"></i> Logout</a>

							

							<select id="lang" class="btn">

								<option value="english">English</option>

								<option <?= $this->session->userdata('lang') == "romania" ? "selected" : "" ;?> value="romania">Romania</option>

							</select>

	                    </div>

	                </div>

	            </div>

	        </div>

	    </div> -->

		<div class="container">



			<div class="main-header-container">
 


				<div class="logo" data-mobile-logo="<?= base_url('assets/images/logoflyparking.png');?>" data-sticky-logo="<?= base_url('assets/images/logoflyparking.png');?>">

					<a href="/"><img class="w--100 mt-1" src="<?= base_url('assets/images/logoflyparking.png');?>" alt="logo"/></a>

				</div> <!-- /LOGO -->



				<div class="burger-menu">

					<div class="line-menu line-half first-line"></div>

					<div class="line-menu"></div>

					<div class="line-menu line-half last-line"></div>

				</div> <!-- /BURGER MENU -->



				<nav class="main-menu menu-caret menu-hover-2 submenu-top-border submenu-scale">

					<ul>

						<li class=""><a href="<?= base_url();?>">Acasa</a></li>

						<!-- <li><a href="parkings.html">Parkings</a>

							<ul>

								<li><a href="parkings.html">Parkings</a></li>

								<li><a href="booking.html">Booking</a></li>

								<li><a href="booking-confirm.html">Booking Confirm</a></li>

							</ul>

						</li> -->

						
						<li><a href="<?= base_url('Welcome/prices');?>">Preturi</a></li>

						<li><a href="<?= base_url('Welcome/faq');?>">FAQ</a></li>

						<li><a href="<?= base_url('Welcome/contact');?>">Contact</a></li>

						<!--<select id="lang" class="btn">-->

						<!--		<option <?= $this->session->userdata('lang') == "romania" ? "selected" : "" ;?> value="romania">Romana</option>-->

						<!--		<option value="english">English</option>-->


						<!--	</select>-->

					</ul>


				</nav> <!-- NAVIGATION MENU -->



			</div> <!-- /HEADER CONTAINER -->



		</div> <!-- /CONTAINER -->

	</div> <!-- /HEADER -->
