

<footer class="footer">

        <div class="bg-dark pt-60 pb-30">

            <div class="container">

                

                <div class="row">

                    

                    <div class="col-md-12">

                        <div class="bs-solid bc-v2-light bw-s1 bt-0 bl-0 br-0 pb-40 mb-50">

                            

                            <div class="row">

                                

                                <div class="col-md-6">

                                    <!-- Begin: LOGO -->

                                    <a class="navbar-brand logo text-cherry" href="<?= base_url()?>">

                                        <img class="full-width max-width-140 m-right-10" alt="Parking" src="<?= base_url('assets/images/logoflyparkingnomotto.png');?>">

                                    </a>

                                    <span>Parcare langa Aeroportul Otopeni supravegheata video</span>

                                    <!-- End: LOGO -->

                                </div>

                                

                                <div class="col-md-6">

                                    <!-- Begin: SOCIAL -->

                                    <!-- <div class="text-right">

										<ul class="social list-inline text-size-s1 mb-s2">

											<li class="list-inline-item mr-s2">

												<a class="bg-cherry   bg-white--hover text-cherry--hover rounded-circle" href="#">

													<i class="fab fa-facebook-f"></i>

												</a>

											</li>

											<li class="list-inline-item mr-s2">

												<a class="bg-cherry   bg-white--hover text-cherry--hover rounded-circle" href="#">

													<i class="fab fa-twitter"></i>

												</a>

											</li>

											<li class="list-inline-item mr-s2">

												<a class="bg-cherry   bg-white--hover text-cherry--hover rounded-circle" href="#">

													<i class="fab fa-linkedin-in"></i>

												</a>

											</li>

											<li class="list-inline-item mr-s2">

												<a class="bg-cherry   bg-white--hover text-cherry--hover rounded-circle" href="#">

													<i class="fab fa-instagram"></i>

												</a>

											</li>

											<li class="list-inline-item mr-s2">

												<a class="bg-cherry   bg-white--hover text-cherry--hover rounded-circle" href="#">

													<i class="fab fa-pinterest-p"></i>

												</a>

											</li>

										</ul>

									</div> -->

                                    <!-- End: SOCIAL -->

                                </div>



                            </div>



                        </div>

                    </div>



                </div>

                

                <div class="row">

                    

                    <div class="col-lg-4 col-md-6">

                        <div class="mb-30">

                            <h5 class="text-bold-700 mb-30   text-uppercase">Contact</h5>

                            <div class="row">

                                <div class="col-lg-12 col-md-6">

                                    <div class="row mb-20 map-bg">

                                        <div class="col-md-12  ">

				                            <address> <abbr title="Address"><strong>Address:</strong></abbr><br> Calea Bucureștilor nr. 236, Otopeni, Ilfov </address>

				                            <address> <abbr title="Phone"><strong>Phone:</strong></abbr><br> +40 757 664 664 </address>

				                            <address> <abbr title="Email"><strong>Email:</strong></abbr><br> <a class="text-cherry" href="mailto:reservations@flyparking.ro">reservations@flyparking.ro</a> </address>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    

                    <div class="col-lg-4 col-md-6">

                        <div class="mb-30">

<!--                             

                            <h5 class="text-bold-700 mb-26   text-uppercase">Blog</h5>



                            <div class="row">

                                <div class="col-lg-12 col-md-12">

                                	<ul>

										<li>

											<div class="mb-s2">

												<div class="d-flex">

													<div class="mr-s2">

														<a href="#"><img class="w--70" src="<?= base_url('assets/site_assets/images/news/thumb/1-100x70.jpg');?>" alt="News"></a>

													</div>

													<div>

														<h6 class="text-bold-600 text-size-15"><a class="  text-cherry--hover" href="#">With one of Australia largest and comprehensive</a></h6>

														<ul class="list-inline text-size-10 text-bold-400   mt-s1">

															<li class="list-inline-item">

																<i class="icon-clock"></i> &nbsp;Jun 01, 2018

															</li>

															<li class="list-inline-item text-size-10">/</li>

															<li class="list-inline-item">

																<i class="icon-bubbles"></i>

																<a class="  text-cherry--hover" href="#">12</a>

															</li>

														</ul>

													</div>

												</div>

											</div>

										</li>

										<li><hr class="bs-dashed bc-v1-dark"></li>

										<li>

											<div class="mb-s2">

												<div class="d-flex">

													<div class="mr-s2">

														<a href="#"><img class="w--70" src="<?= base_url('assets/site_assets/images/news/thumb/2-100x70.jpg')?>" alt="News"></a>

													</div>

													<div>

														<h6 class="text-bold-600 text-size-15"><a class="  text-cherry--hover" href="#">Aliquam lorem ante, dapibus in, viverra quis</a></h6>

														<ul class="list-inline text-size-10 text-bold-400   mt-s1">

															<li class="list-inline-item">

																<i class="icon-clock"></i> &nbsp;Jun 01, 2018

															</li>

															<li class="list-inline-item text-size-10">/</li>

															<li class="list-inline-item">

																<i class="icon-bubbles"></i>

																<a class="  text-cherry--hover" href="#">12</a>

															</li>

														</ul>

													</div>

												</div>

											</div>

										</li>

										<li><hr class="bs-dashed bc-v1-dark"></li>

										<li>

											<div>

												<div class="d-flex">

													<div class="mr-s2">

														<a href="#"><img class="w--70" src="<?= base_url('assets/site_assets/images/news/thumb/3-100x70.jpg');?>" alt="News"></a>

													</div>

													<div>

														<h6 class="text-bold-600 text-size-15"><a class="  text-cherry--hover" href="#">Ut enim ad minima veniam, quis nostrum</a></h6>

														<ul class="list-inline text-size-10 text-bold-400   mt-s1">

															<li class="list-inline-item">

																<i class="icon-clock"></i> &nbsp;Jun 01, 2018

															</li>

															<li class="list-inline-item text-size-10">/</li>

															<li class="list-inline-item">

																<i class="icon-bubbles"></i>

																<a class="  text-cherry--hover" href="#">12</a>

															</li>

														</ul>

													</div>

												</div>

											</div>

										</li>

									</ul>

                                </div>

                            </div>
 -->


                        </div>

                    </div>

                    

                    <div class="col-lg-4 col-md-12">

                        <div class="mb-30">

                            

                            <div class="row">



                                <div class="col-lg-12 col-md-6">

                                    

                                   



                                    <div class="row mb-20">

                                        <div class="col-md-12">

                                            

                                            



                                            <img class="full-width max-width-140 m-right-10" alt="Parking" src="<?= base_url('assets/site_assets/images/visapic.jpg');?>">



                                        </div>

                                    </div>

                                </div>



                            </div>



                        </div>

                    </div>



                </div>



            </div>

        </div>

    </footer>



	<footer class="bg-dark pt-s1 pb-s1">

		<div class="container">

			<div class="row">

				<div class="col-lg-6 col-md-6 col-sm-12">

					<div class="mt-s2 mb-s2  ">

						<p class="m-0  ">&copy; Copyright <?= date('Y'); ?> FlyParking - Created By <abbr><a href="https://exclusive.ro" class=" ">Exclusive</a></abbr></p>

					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-12">

					<div class="mt-s2 mb-s2   text-right">

						<a class=" " href="#">Privacy Policy</a> | <a class=" " href="#">Terms & Conditions</a>

					</div>

				</div>

			</div> <!-- /ROW -->

		</div> <!-- /CONTAINER -->

	</footer>

    <!-- End: FOOTER -

    ################################################################## -->



	<script src="<?= base_url('assets/site_assets/js/jquery-3.3.1.min.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/vendors/appear/jquery.appear.min.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/vendors/jquery.easing/jquery.easing.min.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/js/popper.min.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/js/bootstrap.min.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/vendors/common/common.min.js');?>"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


	<!-- FANCY-BOX -->

	<script src="<?= base_url('assets/site_assets/vendors/fancybox/jquery.fancybox.min.js');?>"></script>



	<!-- MENU -->

	<script src="<?= base_url('assets/site_assets/vendors/menu/src/main.menu.js');?>"></script>



	<!-- CAROUSEL -->

	<script src="<?= base_url('assets/site_assets/vendors/owl.carousel/owl.carousel.min.js');?>"></script>



	<!-- ANIMATED-HEADLINES -->

	<script src="<?= base_url('assets/site_assets/vendors/animated-headline/js/animated-headline.js');?>"></script>



	<!-- THEME-CUSTOM -->

	<script src="<?= base_url('assets/site_assets/js/main.js');?>"></script>



	<!-- THEME-INITIALIZATION-FILES -->

	<script src="<?= base_url('assets/site_assets/js/theme.init.js');?>"></script>

	<script src="<?= base_url('assets/site_assets/js/custom.js');?>"></script>



</body>

</html>

<script>

$(document).ready(function(){

	$('#lang').change(function(){

		$.ajax({

					method : "POST",

					url : $('#base_url').val() + "Welcome/changelang",

					data : {key:"parking",lang:$(this).val()},

					success:function(data)

					{ 

						let obj = JSON.parse(data);

						if(obj.status == true){

							window.location.href= $('#base_url').val();

						}		

						

					}

				});

	})

});

</script>

