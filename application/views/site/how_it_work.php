<?php include("layout/header.php");?>
<!-- BREADCRUMB -->
<section class="bg-cherry pt-s3 pb-s3 text-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="d-lg-flex text-center">
						<div class="align-self-center mb-0--lg mb-s1--md mb-s1--sm mb-s1--xs">
							<h3 class="text-bold-400 text-size-s2 m-0 text-white">How it Works</h3>
						</div>
						<div class="ml-auto align-self-center">
							<ol class="breadcrumb d-flex justify-content-center m-0 bg-none text-size-s1 p-0">
								<li class="breadcrumb-item"><a class="text-white" href="index.html">Home</a></li>
								<li class="breadcrumb-item" aria-current="page">How it Works</li>
							</ol>
						</div>
					</div>
				</div>
			</div> <!-- ROW -->
		</div> <!-- /CONTAINER -->
	</section> <!-- /SECTION -->
	<!-- /BREADCRUMB -->

	<section class="bg-white pt-100 pb-s1">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="mt-s2 mb-s2 box-shadow-parent--hover mt--parent--hover">
						<div class="bg-v5-light text-center pt-s4 bs-solid bc-light bw-s1 position-relative">
							<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">
								<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover" src="<?= base_url('assets/site_assets/images/steps/step-1.png');?>" alt="Departure & Arrival Details">
							</div>
							<div class="p-s2 mt-s2 mb-s2">
								<span class="text-cherry text-bold-600">Step 1</span>
								<h5 class="mt-s1 mb-0 text-bold-600">Departure & Arrival Details</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="mt-s2 mb-s2 box-shadow-parent--hover mt--parent--hover">
						<div class="bg-v5-light text-center pt-s4 bs-solid bc-light bw-s1 position-relative">
							<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">
								<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover" src="<?= base_url('assets/site_assets/images/steps/step-2.png');?>" alt="Your & Vehicle Detail">
							</div>
							<div class="p-s2 mt-s2 mb-s2">
								<span class="text-cherry text-bold-600">Step 2</span>
								<h5 class="mt-s1 mb-0 text-bold-600">Your & Vehicle Detail</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="mt-s2 mb-s2 box-shadow-parent--hover mt--parent--hover">
						<div class="bg-v5-light text-center pt-s4 bs-solid bc-light bw-s1 position-relative">
							<div class="hero-section-container-tl w-100 mt--s4 mt--s5-child">
								<img class="w--100 rounded-circle bs-solid bc-white bw-s5 box-shadow-v2-s3-child--hover" src="<?= base_url('assets/site_assets/images/steps/step-3.png');?>" alt="Payment Detail">
							</div>
							<div class="p-s2 mt-s2 mb-s2">
								<span class="text-cherry text-bold-600">Step 3</span>
								<h5 class="mt-s1 mb-0 text-bold-600">Payment Detail</h5>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /ROW -->
		</div> <!-- /CONTAINER -->
	</section> <!-- /SECTION -->

	<section class="bg-white pt-s5 pb-s5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="mb-s2">
						<h3 class="text-bold-700 text-dark text-capitalize">Booking</h3>
					</div>
					<p>Etiam elit eros, fermentum eu magna iaculis, maximus pretium nunc. Curabitur ullamcorper laoreet sapien at convallis. Cras imperdiet maximus quam ut elementum. Aliquam euismod elit mattis viverra condimentum. Suspendisse bibendum in nisi quis finibus.</p>
					<ul class="mt-s3 mb-s3">
						<li class="mb-6">
							<div class="d-flex align-items-center">
								<span class="bg-cherry text-white h--20 w--20 d-flex justify-content-center align-items-center text-size-8 rounded-circle mr-s2">
									<i class="icon-arrow-right"></i>
								</span>
								<p class="m-0">Departure and Arrival Dates and Time.</p>
							</div>
						</li>
						<li class="mb-6">
							<div class="d-flex align-items-center">
								<span class="bg-cherry text-white h--20 w--20 d-flex justify-content-center align-items-center text-size-8 rounded-circle mr-s2">
									<i class="icon-arrow-right"></i>
								</span>
								<p class="m-0">Departure and Arrival terminal.</p>
							</div>
						</li>
						<li class="mb-6">
							<div class="d-flex align-items-center">
								<span class="bg-cherry text-white h--20 w--20 d-flex justify-content-center align-items-center text-size-8 rounded-circle mr-s2">
									<i class="icon-arrow-right"></i>
								</span>
								<p class="m-0">Contact Details.</p>
							</div>
						</li>
						<li class="mb-6">
							<div class="d-flex align-items-center">
								<span class="bg-cherry text-white h--20 w--20 d-flex justify-content-center align-items-center text-size-8 rounded-circle mr-s2">
									<i class="icon-arrow-right"></i>
								</span>
								<p class="m-0">Vehicle's Details.</p>
							</div>
						</li>
						<li class="mb-6">
							<div class="d-flex align-items-center">
								<span class="bg-cherry text-white h--20 w--20 d-flex justify-content-center align-items-center text-size-8 rounded-circle mr-s2">
									<i class="icon-arrow-right"></i>
								</span>
								<p class="m-0">Payment Method (PayPal, Debit or Credit Cards).</p>
							</div>
						</li>
					</ul>
					<p>Etiam elit eros, fermentum eu magna iaculis, maximus pretium nunc. Curabitur ullamcorper laoreet sapien at convallis. Cras imperdiet maximus quam ut elementum. Aliquam euismod elit mattis viverra condimentum. Suspendisse bibendum in nisi quis finibus.</p>
					<div class="mb-s2">
						<h3 class="text-bold-700 text-dark text-capitalize">Departure</h3>
					</div>
					<p>Suspendisse vehicula ut lectus scelerisque finibus. Donec ac scelerisque ante. Fusce ornare nibh et diam ultricies, et venenatis leo venenatis. Proin convallis vel nunc et tincidunt. Cras vel consectetur leo. Morbi volutpat dolor urna, eget semper dolor finibus condimentum. Duis porta ante felis, id malesuada erat gravida ac. Proin ullamcorper ipsum eu faucibus venenatis. Ut in sem et mi placerat consectetur non nec sapien. Suspendisse vehicula ut lectus scelerisque finibus. Donec ac scelerisque ante. Fusce ornare nibh et diam ultricies.</p>
					<div class="mb-s2">
						<h3 class="text-bold-700 text-dark text-capitalize">Arrival</h3>
					</div>
					<p>Suspendisse vehicula ut lectus scelerisque finibus. Donec ac scelerisque ante. Fusce ornare nibh et diam ultricies, et venenatis leo venenatis. Proin convallis vel nunc et tincidunt. Cras vel consectetur leo. Morbi volutpat dolor urna, eget semper dolor finibus condimentum. Duis porta ante felis, id malesuada erat gravida ac. Proin ullamcorper ipsum eu faucibus venenatis. Ut in sem et mi placerat consectetur non nec sapien.</p>
				</div>
			</div> <!-- /ROW -->
		</div> <!-- /CONTAINER -->
	</section> <!-- /SECTION -->

<?php include("layout/footer.php");?>