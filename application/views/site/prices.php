<?php include("layout/header.php"); ?>
<!-- BREADCRUMB -->
<section class="bg-cherry pt-s3 pb-s3 text-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="d-lg-flex text-center">
					<div class="align-self-center mb-0--lg mb-s1--md mb-s1--sm mb-s1--xs">
						<h3 class="text-bold-400 text-size-s2 m-0 text-white">Preturi Lista</h3>
					</div>
					<div class="ml-auto align-self-center">
						<ol class="breadcrumb d-flex justify-content-center m-0 bg-none text-size-s1 p-0">
							<li class="breadcrumb-item"><a class="text-white" href="index.html">Home</a></li>
							<li class="breadcrumb-item" aria-current="page">PRETURI</li>
						</ol>
					</div>
				</div>
			</div>
		</div> <!-- ROW -->
	</div> <!-- /CONTAINER -->
</section> <!-- /SECTION -->
<!-- /BREADCRUMB -->

<section class="bg-white pt-s5 pb-s5">
	<div class="container">
		<div class="row">
			<?php
			foreach ($prices_list as $index => $list) {
				?>
				<div class="col-lg-4 col-md-4">
					<div class="card card-success">
						<div class="card-header bg-success text-white">
							<h4 class="card-title">
								<?= $list['name'] ?>
							</h4>
							<h6>
								(<?= $list['start_date'] ?> - <?= $list['end_date'] ?>)
							</h6>
						</div>
						<div class="card-body" style="display: block;">

							<table class="table">
								<thead>
								<tr>
									<th scope="col">Zile</th>
									<th scope="col">Pret in RON</th>
								</tr>
								</thead>
								<tbody><?php
								foreach ($list['prices'] as $p_index => $price) {
								?>
								<tr>
									<th><?= $price['days'] ?></th>
									<th><?= $price['price'] ?></th>
								</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
				<?php
			}
			?>
		</div> <!-- /ROW -->
	</div> <!-- /CONTAINER -->
</section> <!-- /SECTION -->

<?php include("layout/footer.php"); ?>
