<?php include('header.php'); ?>


<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Contracte neplatite

         </div>

      </div>

   </div>

</div>


<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

        <div class="col-md-12">
          
         

        </div>

            <div class="col-md-12">

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>ID</th>

                      	<th>Name</th>

                        <th>Sosire</th>

                        <th>Telefon</th>

                        <th>Auto</th>

                        <th>Total</th>

                        <th>Platit</th>

                        <th>Neplatit</th>

                        <th>Nr. loc parcare</th>

                        <th>Status</th>
                     </tr>

                  </thead>

                  <tbody>

                    <?php if(!empty($unpaid_contracts)) {?>
                  	<?php foreach($unpaid_contracts as $key => $p){	?>

				

						<tr>

							<td><a href="/edit_agreement?id=<?php echo $p->ID; ?>"><?php echo $p->ID; ?></a></td>

    					<td> <?php echo $p->name; ?> </td>

    					<td><?php echo format_date($p->checkin_date); ?></td>

              <td><?php echo $p->phone; ?></td>

              <td><?php echo $p->marca ?> <?php echo $p->model; ?> <?php echo $p->nr_inmatriculare; ?></td>

              <td><?php echo $p->total; ?></td>

              <td><?php echo $p->paid; ?></td>

              <td><?php echo $p->total - $p->paid; ?></td>

              <td><?php echo $p->parking_spot; ?></td>

              <td><?php if($p->status == 4) { echo 'Deschis'; } else if($p->status == 5){ echo 'Inchis'; }    ?></td>

						</tr>

                  	<?php }} else { echo '<p class="text-center">Nu exista contracte neplatite</p>'; }?>

					



                  </tbody>

                  

               </table>

            </div>

            <div class="col-md-12">
            	

            </div>

         </div>

   </div>

</div>


<?php include('footer.php'); ?>

<script type="text/javascript">

  var table = $('#datatabletable').DataTable({

         columnDefs: [

           { type: 'de_datetime', targets: 2 }

         ],
     
     

      });


$('#set_washing_price').click(function(){
  var new_price = $('#washing_price').val();

  $.post("/Main_controller/change_washing_price", {new_price: new_price}, function(data, status){

      
      window.location.reload();
      

    });

});

</script>