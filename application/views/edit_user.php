<?php include('header.php'); ?>

<div class="container">
	
	<div class="row">
		<div class='col-md-8 offset-md-2'>
			
			<form id="update_user_form">

				
				<div class="position-relative form-group">
					<label for="">New Password</label>
					<input id="password" name="password" type="text" class="form-control">
				</div>

				<button type="button" class="btn btn-primary" id="update_user">Update</button>

			</form>

		</div>
	</div>

</div>

<?php include('footer.php'); ?>

<script type="text/javascript">
	
	$('#update_user').click(function(){

		var password = $('#password').val();
		var user_id = '<?php echo $_GET['id']; ?>';
		var email = '<?php echo $user->email; ?>';

		$.ajax({

            url: '/Main_controller/update_user',

            dataType: 'text',

			type: 'POST',

            data: {

                password: password,
                email: email,

                user_id: user_id

            },

			error: function (request, error) {

				console.log(arguments);

			},

            success: function(response) {

				alert(response);

            }

        });


	});

</script>