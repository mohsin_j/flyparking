<!doctype html>

<html lang="en">
<script>
	let base_url = "<?= base_url() ?>";
</script>



<meta http-equiv="content-type" content="text/html;charset=UTF-8" />



<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta http-equiv="Content-Language" content="en">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Login - Fly Parking</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"

	/>

	<meta name="description" content="Parcare pe termen lung">

	<!-- Disable tap highlight on IE -->

	<meta name="msapplication-tap-highlight" content="no">

	<link href="main.cba69814a806ecc7945a.css?v=1" rel="stylesheet">

	<link rel="stylesheet" href="alana-parking.css?v=1" rel="stylesheet">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

</head>

<body class="login-body">

<div class="app-container  body-tabs-shadow alana-login-wrapper">

	<div class="app-container">

		<div class="h-100">

			<div class="h-100 no-gutters row">

				<div class="h-100 align-items-center d-flex justify-content-center col-md-12 col-lg-12">

					<div class="mx-auto app-login-box col-sm-12 col-md-6 col-lg-3">

						<div class="logo-wrapper">

							<img src="assets/images/logoflyparking.png" class="img-fluid alana-login-logo" alt="Flyparking Parking">

						</div>

						<h4 class="mb-0">

							<span class="d-block">Bine ai revenit,</span>

							<span>Te rugam sa te autentifici.</span>

						</h4>

						<div class="divider row"></div>

						<div>

							<form class="login_form">

								<div class="form-row">

									<div class="col-md-12">

										<div class="position-relative form-group"><label for="exampleEmail" class="">Email</label><input name="email" id="exampleEmail" placeholder="" type="email" class="form-control"></div>

									</div>

									<div class="col-md-12">

										<div class="position-relative form-group"><label for="examplePassword" class="">Parola</label><input name="password" id="examplePassword" placeholder="" type="password"

																																			 class="form-control"></div>

									</div>

								</div>

								<div class="position-relative form-check"><input name="check" id="exampleCheck" type="checkbox" class="form-check-input"><label for="exampleCheck" class="form-check-label">Tine-ma logat 1 an</label></div>

								<div class="divider row"></div>

								<div class="d-flex align-items-center">

									<div class="ml-auto"><a href="javascript:void(0);" class="btn-lg btn btn-link">Recupereaza parola</a>

										<button class="btn btn-primary btn-lg" id="submit_login">Login</button>

									</div>

								</div>

							</form>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script type="text/javascript" src="assets/scripts/main.cba69814a806ecc7945a.js"></script>

<script  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

</body>



<script type="text/javascript">



	// A $( document ).ready() block.

	$( document ).ready(function() {



		$('#submit_login').click(function(e){



			e.preventDefault();



			var data = $('.login_form').serialize();

			$.post(base_url+"/Welcome/login", data, function(data, status){

				// Display an error toast, with a title



				if(data == 'User is logged in'){

					toastr.success(data, 'Success!');

					setTimeout(function(){ window.location.href = base_url+"/dashboard";; }, 500);



					return false;

				}

				toastr.error(data, 'Error!');

			});

		})



	});



</script>



</html>

