<?php include('header.php'); ?>



<link rel='stylesheet' href="<?= base_url('/assets/scripts/fullcalendar.css')?>" />

<link rel='stylesheet' href="<?= base_url('/assets/scripts/scheduler.css')?>" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />



<style type="text/css">		

	

	#btn_calc_price{

		display: block;

		margin: 0 auto;

	}

	div#cal {

	    padding: 20px;

	}



</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Planning

         </div>

      </div>

   </div>

</div>

<div class="row">

	<div class="col-md-12">

	    <div class="mb-3 card">

	    	

			 <div id="cal" class="has-toolbar"> </div>



	    </div>

	</div>

</div>

<?php include('footer.php'); ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>

	  	<script src="<?= base_url('/assets/scripts/fullcalendar.min.js') ?>" type="text/javascript"></script>

		<script src="<?= base_url('/assets/scripts/scheduler.min.js') ?>"></script>



<script type="text/javascript">

	



$( document ).ready(function() {



	var calendarHeight = $('.app-main__inner').height();

	var todayDate = moment().startOf('day');

	var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');

	var TODAY = todayDate.format('YYYY-MM-DD');

	var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');



	//Defining resources for fullcalendar

	 var resourceObjects = [];

	 var i = 0;

	 var parking_spots = <?php echo $total_parking_spots; ?>;



	 while( i < parking_spots ){



	 	i++;



	 	resourceObjects.push({

			id: i,

			lp: 'Loc Parcare '+i

		});



	 }







	



	$('#cal').fullCalendar({

		resourceAreaWidth: 200,

		editable: false,

		eventResourceEditable: true,

		aspectRatio: 1.5,

		height: calendarHeight,

		scrollTime: '00:00',

		gotoDate: TODAY,

		selectable: true,

		select: function(startDate, endDate, jsEvent, view, resource) {

		  ReservationDialog(startDate.format('DD-MM-YYYY HH:mm:ss'), endDate.format('DD-MM-YYYY HH:mm:ss'), resource.id );

		},

		header: {

			left: 'promptResource prev,next',

			center: 'title',

			right: 'timelineFifteenDays,timelineMonth'

		},

		allDayDefault: false,

		slotDuration: '12:00:00',

		customButtons: {

			promptResource: {

				text: 'Adauga Rezervare',

				click: function() {

					window.location.replace("/add_reservation");

				}

			}

		},

		defaultView: 'timelineFifteenDays',

		views: {

			timelineFifteenDays: {

				type: 'timeline',

				duration: { days: 15 }

			},

			

			timelineMonth: {

				type: 'timeline',

				duration: { days: 31 }

			}

		},

		resourceLabelText: 'Vehicles',

		resourceColumns: [

        {

            labelText: 'Loc Parcare',

            field: 'lp',

			width: '100%'

        },

        

    ],

    resources: resourceObjects,

	events: function(start, end, timezone, callback) {

	

	

			$.ajax({

            url: base_url+'/Main_controller/get_reservations',

            dataType: 'json',

			type: 'POST',

            data: {

                start: start.format('YYYY-MM-DD'),

                end: end.format('YYYY-MM-DD')

            },

			error: function (request, error) {

				console.log(arguments);

			},

            success: function(res) {

				

				console.log(res);

				

				

                var resourceObjects = [];

				

				

                $.each(res, function( index, value ) {

					/**

					{ id: '".$data['reservation_no']."',

									resourceId: '".$data['vehicle_no']."', 

									start: '".$data['check_out_date']."',

									end: '".$data['check_in_date']."',

									title: 'CheckIn: ".$checkInHour[1]." ".$initial_veh_display." ".$display."',

									qtip_title: '".$qtip_title." - ".$no."',

									description: '".str_replace("'","\'",$name)." - ".$phone_no."<br />".$checkOutHour[1]." - ".$checkInHour[1]."<br />".$checkout_location." - ".$checkin_location."<br />Total payment:".$total_payment."',

									color: '".$color."',

									eventTextColor: '".$event_text_color."',

									dbReservationId: '".$data['reservation_no']."',

									agreement_status: '".$data['agreement_status']."',

								}

					*/

						var status = value.status;
						var text = 'white';

						if(status == 0){
							var color = 'yellow'; 
							text = 'black';   //pending
						} else if(status == 1){
							var color = 'green';
						} else if(status == 4){
							var color = 'purple';
						} else {
							return;
						}

						resourceObjects.push({

							id: value.ID,

							resourceId: value.parking_spot,

							start: value.checkout_date,

							end: value.checkin_date,

							title: value.name+' - '+value.total+' RON',

							color: color,

							eventTextColor: text,

							textColor : text,

						});

					

					

					

					

                });

				console.log(resourceObjects);

                callback(resourceObjects);

            }

        });

			

	},

});



});



</script>
