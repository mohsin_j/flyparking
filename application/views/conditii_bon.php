<?php include('header.php'); ?>



<style type="text/css">

   #btn_calc_price{

   display: block;

   margin: 0 auto;

   }

   .modal-backdrop.fade.show {
      display: none;
  }

</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Conditii Bon

         </div>

      </div>

   </div>

</div>

<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

            <div class="col-md-12">

              <div class="row">
                
                <div class="col-md-6">
                  
                  <div class="position-relative form-group"><label for="">Conditie Noua:</label><input name="conditie" id="conditie" placeholder="" type="text" class="form-control"></div>

                </div>

              </div>

              <button type="button" class='btn btn-secondary' id="add_new_condition">Adauga Conditie</button>

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                      	<th>#</th>

                      	<th>Conditie</th>

                        <th>Actiuni</th>

                     </tr>

                  </thead>

                  <tbody>



                  	<?php foreach($conditii as $key => $d){	?>

				

      						<tr>

      							

      							<td><?php echo $key + 1; ?></td>

      							<td condition-id="<?php echo $d->ID; ?> "><?php echo $d->text; ?></td>

                    <td> <button type="button" class="btn btn-warning edit_condition" condition-id="<?php echo $d->ID; ?> " data-toggle="modal" data-target="#exampleModal">Edit</button> <button type="button" class="btn btn-danger delete_condition" condition-id="<?php echo $d->ID; ?>">Sterge</button> </td>

      						</tr>



                  	<?php } ?>

					



                  </tbody>

                  

               </table>

            </div>

         </div>

   </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:50px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            
        <div class="position-relative form-group">
          <label for="">Editeaza Conditia:</label>

          <textarea class="form-control" id="edit_conditie_input"></textarea>
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Inchide</button>
        <button type="button" class="btn btn-primary" id="update_condition">Salveaza</button>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">

  $("#update_condition").click(function(){

    var condition_id = $(this).attr('condition-id');
    var conditie = $('#edit_conditie_input').val();

    if(conditie == ''){
      alert('Nu poti salva o conditie goala!');
      return;
    }

    var data = { condition_id : condition_id, conditie: conditie };

    $.post("/Main_controller/update_condition", data, function(data, status){

      var data = JSON.parse(data);

      if(data){
        window.location.reload();
      }

    });

  });

  $(".edit_condition").click(function(){

    var condition_id = $(this).attr('condition-id');

    $('#edit_conditie_input').val();

    var conditie = $('td[condition-id="'+condition_id+'"]').text();
    $('#edit_conditie_input').val(conditie);
    $('#update_condition').attr('condition-id', condition_id);

  });

  $(".delete_condition").click(function(){

    var condition_id = $(this).attr('condition-id');

    var data = { condition_id : condition_id };

    $.post("/Main_controller/delete_condition", data, function(data, status){

      var data = JSON.parse(data);

      if(data){
        window.location.reload();
      }

    });

  });
      
  $("#add_new_condition").click(function(){
    var conditie = $('#conditie').val();

    var data = { conditie : conditie };

    if(conditie == ''){
      alert('Te rugam sa completezi conditia!'); return;
    }

    $.post("/Main_controller/add_new_condition", data, function(data, status){

      var data = JSON.parse(data);

      if(data){
        window.location.reload();
      }

    });


  });


  $('#datatabletable').DataTable({

     columnDefs: [

       { type: 'de_datetime', targets: 2 },

       { type: 'de_datetime', targets: 3 }

     ]

  });

      

 

   

</script>