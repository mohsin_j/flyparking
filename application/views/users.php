<?php include('header.php'); ?>

<style type="text/css">
    
    a.btn.btn-danger.delete_user {
        color: white;
        margin-left: 5px;
    }

</style>

<button type="button" class="btn btn-primary" onclick="window.location.href='/add_user'" style="margin-bottom:50px;">Add
    User
</button>


<table style="width: 100%;" id="datatabletable"
       class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

    <thead>

    <tr role="row">

        <th>#</th>

        <th>Username</th>

        <th>Email</th>

        <th>Status</th>

        <th>Actions</th>

    </tr>

    </thead>

    <tbody>


    <?php foreach ($users as $key => $d) { ?>


        <tr>


            <td><?php echo $key + 1; ?></td>

            <td><?php echo $d->username; ?></td>

            <td><?php echo $d->email; ?></td>

            <td><?= ($d->status == 5) ? 'Blocat' : 'Activ'; ?></td>

            <td><a type="button" class="btn btn-warning" href="/edit_user?id=<?php echo $d->id; ?>">Change
                    Password</a><a type="button" class="btn btn-danger delete_user"
                                   data-user-id="<?php echo $d->id; ?>">Inactiveaza</a></td>


        </tr>


    <?php } ?>


    </tbody>


</table>


<?php include('footer.php'); ?>

<script type="text/javascript">
    $('#datatabletable').DataTable();

    $(document).on('click','.delete_user',function () {

        //  alert('You are NOT allowed to delete a user because of database restrictions. Please change the user password if you want to block it.');
        // return false;

        //some code
        var user_id = $(this).data('user-id');

        var result = confirm("Esti sigur ca vrei sa stergi acest utilizator?");
        if (result) {

            var data = {user_id: user_id};

            $.post("/Main_controller/delete_user", data, function (result, status) {

                if(result) {

                    var jsonResult = JSON.parse(result);

                    if(jsonResult.status === true) {
                        window.location.reload();
                    }

                }


            });

        } else {
            return;
        }

    });

</script>