<?php include('header.php'); ?>


<table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

  <thead>

     <tr role="row">

      	<th>#</th>

      	<th>Status</th>

      	<th>Language</th>

      	<th>Actions</th>

     </tr>

  </thead>

  <tbody>



  	<?php foreach($templates as $key => $d){	?>



		<tr>

			

			<td><?php echo $key + 1; ?></td>

			<td>Confirmed</td>

			<td><?php echo $d->language; ?></td>

			<td><a type="button" class="btn btn-warning" href="/edit_template?id=<?php echo $d->ID; ?>">Edit</a></td>


		</tr>



  	<?php } ?>

	



  </tbody>

  

</table>


<?php include('footer.php'); ?>

<script type="text/javascript">
	$('#datatabletable').DataTable();
</script>