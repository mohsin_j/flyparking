<?php include('header.php'); ?>



<style type="text/css">

   #btn_calc_price{

   display: block;

   margin: 0 auto;

   }

</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Rezervari

         </div>

      </div>

   </div>

</div>

<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

            <div class="col-md-12">

              <div class="row">
                  <div class="col-md-3">
                    <div class="form-group"> 
                      <label> Status </label>
                      <select class="form-control input-medium" onchange="filter(this.id)" id="status" table_col="5">
                        <option value="">All</option>
                        <option value="1">Confirmat</option>
                        <option value="2">Anulat</option>
                        <option value="3">No Show</option>
                        <option value="4">Contract</option>
                      </select>
                    </div>
<?php //var_dump($access_level); ?>
                  </div>


                  <div class="col-md-3">
                    <button type="button" class="btn btn-lg btn-info" onclick="window.location.href='/add_reservation';" style="margin-top:29px;">Adauga Rezervare</button>
                  </div>
              </div>

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                      	<th>ID</th>

                      	<th>Nume</th>

                      	<th class="activate_sorting">Data sosire</th>

                      	<th>Data plecare</th>

                        <th>Tip</th>

                      	<th>Telefon</th>

                        <th>Status</th>

                      	<th>Total</th>

                        <th>Creat De</th>

                        <th>Creat La</th>
						
						<th>Status Plată Online</th>

                        

                     </tr>

                  </thead>

                  <tbody>



                
					



                  </tbody>

                  

               </table>

            </div>

         </div>

   </div>

</div>

<?php include('footer.php'); ?>

<script type="text/javascript">

    function change_status(status, res_id, color){

      var answer = window.confirm("Sunteti sigur?")
      if (answer) {
          //some code
          var data = { status: status, res_id: res_id };
           $.post("/Main_controller/change_reservation_status", data, function(data, status1){

              
              toastr.success('Rezervarea a fost modificata!', 'Success!');

              $('.rowID_'+res_id).css({'background-color':color});

            
              });
      }
      else {
          return;
      }

    }

    function datatablesButtons(filename, message) {
    var name = "System";
 
    return [
        {
            text: "Excel",
            extend: "excelHtml5",
            title: filename,
        },
        {
            text: "PDF",
            extend: "pdfHtml5",
            filename: filename,
            title: name,
            message: message,
            pageSize: "letter",
        },
        {
            text: "Print",
            extend: "print",
            message: '<span class="pull-left">'+name+'</span><span class="pull-right">'+message+'</span>',
        }
    ];
}

      var table = $('#datatabletable').DataTable({
          

         columnDefs: [

           { type: 'de_datetime', targets: 2 },

           { type: 'de_datetime', targets: 3 }

         ],

         <?php if($access_level == 1) {?>

         dom: 'Blfrtip',
          buttons: [
            'excelHtml5'
          ],

        aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],

        <?php }?>
          

          // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,

            

            // Initial no order.
            "order": [],
            // Load data from an Ajax source
            "ajax": {
              "url": "/Main_controller/dt_reservations",
              "type": "POST",
              "data": function ( d ) {
                d.status = $('#status').val()
                console.log(d.status);
              }
            },
            
            "oSearch": { "bSmart": false, "bRegex": true }

      });

      function filter(id){
        table
          .columns($('#'+id).attr('table_col'))
          .search($('#'+id).val())
          .draw();
          
          
        //table.ajax.reload();
      }

 
$(window).on("load", function () {
   $("#status option").each(function(){
    console.log($(this).text());
      if($(this).text() == 'Confirmat'){
        $(this).prop('selected', true);
      }
   });

   filter('status');
   
      $('.activate_sorting').click();
});
   

</script>