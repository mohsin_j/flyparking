<?php include ('header.php'); ?>
<link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css"/>
<?php echo Xcrud::load_css(); ?>
<div class="app-page-title">
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <i class="pe-7s-car icon-gradient bg-mean-fruit">
        </i>
      </div>
      <div>
        <?php echo $page_title; ?>
      </div>
    </div>
  </div>
</div>
<div class="tab-pane tabs-animation fade active show" id="tab-content-2" role="tabpanel">
  <div class="main-card mb-3 card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-info">
          	
          		Pentru a creea o lista de preturi cu mai multe intervale de aplicabilitate, se va folosi acelasi nume pentru toate.<br />
          		Se va alege lista de preturi pentru website / parkvia dupa nume, iar programul va trimite preturile aferente perioadei necesare.

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="tab-pane tabs-animation fade active show" id="tab-content-2" role="tabpanel">
  <div class="main-card mb-3 card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <?php echo $table; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="tab-pane tabs-animation fade active show" id="tab-content-2" role="tabpanel">
  <div class="main-card mb-3 card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <?php echo form_open('main_controller/save_price_options'); ?>
          <div class="row">

          		<div class="col-md-6">
		          	<label>
		          		Preturi fara discount pentru website:
		          		<select class="form-control input-medium" name="website_price_list">

		          			<?php foreach ($price_lists as $key => $l): ?>
		          			
		          				<option <?= ($l['id'] == $price_options['website_price_list']) ? 'selected' : ''; ?> value="<?= $l['id']; ?>">
		          				
		          					<?= $l['name']; ?>

		          				</option>

		          			<?php endforeach ?>
		          			
		          			

		          		</select>

		          	</label>

		          </div>

		      </div>
			<div class="row">

          		<div class="col-md-6">
		          	<label>
		          		Preturi prepaid website:
		          		<select class="form-control input-medium" name="website_price_list_prepaid">

		          			<?php foreach ($price_lists as $key => $l): ?>
		          			
		          				<option <?= ($l['id'] == $price_options['website_price_list_prepaid']) ? 'selected' : ''; ?> value="<?= $l['id']; ?>">
		          				
		          					<?= $l['name']; ?>

		          				</option>

		          			<?php endforeach ?>
		          			
		          			

		          		</select>

		          	</label>
				</div>

          	</div>
          	<div class="row">

          		<div class="col-md-6">

          				<label>
		          		Preturi ParkVia pentru prelungiri:
		          		<select class="form-control input-medium" name="parkvia_reschedule_pricing">

		          			<?php foreach ($price_lists as $key => $l): ?>
		          			
		          				<option <?= ($l['id'] == $price_options['parkvia_reschedule_pricing']) ? 'selected' : ''; ?> value="<?= $l['id']; ?>">
		          				
		          					<?= $l['name']; ?>

		          				</option>

		          			<?php endforeach ?>
		          			
		          			

		          		</select>

		          	</label>

          		</div>

          	</div>

          	<hr />
          	<input type="submit" value="Salveaza" class="btn btn-lg btn-info" />

          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include ('footer.php'); ?>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js">
</script>
<?php echo Xcrud::load_js(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"  />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js">
</script>
<script type="text/javascript">
  jQuery(document).on("xcrudbeforerequest", function(event, container) {
    if (container) {
      jQuery(container).find("select").select2("destroy");
    }
    else {
      jQuery(".xcrud").find("select").select2("destroy");
    }
  }
                     );
  jQuery(document).on("ready xcrudafterrequest", function(event, container) {
    if (container) {
      jQuery(container).find("select").select2();
    }
    else {
      jQuery(".xcrud").find("select").select2();
    }
  }
                     );
  jQuery(document).on("xcrudbeforedepend", function(event, container, data) {
    jQuery(container).find('select[name="' + data.name + '"]').select2("destroy");
  }
                     );
  jQuery(document).on("xcrudafterdepend", function(event, container, data) {
    jQuery(container).find('select[name="' + data.name + '"]').select2();
  }
                     );
</script>
<script type="text/javascript">
  $( document ).ready(function() {
  }
                     );
</script>
