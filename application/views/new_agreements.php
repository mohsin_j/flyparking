<?php include('header.php'); ?>


<style type="text/css">

    #btn_calc_price {

        display: block;

        margin: 0 auto;

    }

</style>

<div class="app-page-title">

    <div class="page-title-wrapper">

        <div class="page-title-heading">

            <div class="page-title-icon">

                <i class="pe-7s-car icon-gradient bg-mean-fruit">

                </i>

            </div>

            <div>

                Agreement-uri new

            </div>

        </div>

    </div>

</div>

<div class="main-card mb-3 card">

    <div class="card-body">

        <div class="row">

            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label> Status </label>
                            <select class="form-control input-medium" onchange="changedStatus(this.value)" id="status"
                                    table_col="7">
                                <option value="-1">All</option>
                                <option selected value="4">Deschis</option>
                                <option value="5">Inchis</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <button type="button" class="btn btn-lg btn-info"
                                onclick="window.location.href='/add_reservation';" style="margin-top:29px;">Adauga
                            Rezervare
                        </button>
                    </div>
                </div>

                <table style="width: 100%;" id="datatabletable"
                       class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                    <thead>
                    <tr role="row">
                        <th>ID</th>
                        <th>Nume</th>
                        <th>Data sosire</th>
                        <th class="activate_sorting">Data plecare</th>
                        <th>Telefon</th>
                        <th>Auto</th>
                        <th>Spalatorie</th>
                        <th>Mobilpay Status</th>
                        <th>Tip</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th>Creat De</th>
                        <th>Creat La</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>


                </table>

            </div>

        </div>

    </div>

</div>

<?php include('footer.php'); ?>


<script src="/assets/scripts/moment-with-locales.js" type="text/javascript"></script>

<script type="text/javascript">
    moment.locale('ro');
    var mainTable = null;

    function renderDatatable( pageLength = 25, status = 4) {
        mainTable = $('#datatabletable').DataTable({

            dom: 'Blfrtip',
            processing: true,
            serverSide: true,
            buttons: [
                'excelHtml5'
            ],
            "order": [[ 3, "asc" ]],

            aLengthMenu: [
                [25, 50, 100, 200, 100000],
                [25, 50, 100, 200, "All"]
            ],

            columnDefs: [


            ],
            ajax: {
                "url": "<?php echo base_url('/new_agreements_ajax')?>",
                "type": "GET",
                data: {
                    pageLength: pageLength,
                    status: status
                }
            },
            "columns": [
                {
                    data: "agreement_id",
                    "render": function(data){
                        return `<a href="/edit_agreement?id=${data}">${data}</a>`;
                    }
                },
                {
                    data: "customer_name"
                },
                {
                    data: "checkout_date",
                    "render": function(data){
                        return moment(data).format('DD.MM.YYYY HH:mm')
                    }
                },
                {
                    data: "checkin_date",
                    "render": function(data){
                        return moment(data).format('DD.MM.YYYY HH:mm')
                    }
                },
                {
                    data: "customer_phone"
                },
                {
                    data: "customer_auto"
                },
                {
                    data: "washing",
                    "render": function (data, type, row, meta) {
                        return data == 0 ? 'Nu' : 'Da';
                    }
                },
                {
                    data: "mobilpay_status",
                    "render": function (data, type, row, meta) {
                        if(data == 'Confirmed') {
                            return '<p style="background: green; color: #fff; padding: 5px 10px; border-radius: 5px; display: inline-block; margin: 0;">Confirmed</p>';
                        }

                        if(data == 'Unpaid') {
                            return '<p style="background: orange; color: #fff; padding: 5px 10px; border-radius: 5px; display: inline-block; margin: 0;">Unpaid</p>';
                        }

                        return '-';
                    }
                },{
                    data: "tip",
                    "render": function (data, type, row, meta) {
                        if(data == 'ParkVia') {
                            return '<p class="parkvia-prepaid"><img class="pv-prepaid-logo" src="assets/images/PV_Logo.png"></p>';
                        }


                        return data;
                    }
                },{
                    data: "status",
                    "render": function (data, type, row, meta) {
                        if(data == 4) {
                            return 'Deschis';
                        }

                        if(data == 5) {
                            return 'Inchis';
                        }

                        return '-';
                    }
                },
                {
                    data: "total"
                },
                {
                    data: "username"
                },
                {
                    data: "created_at",
                    "render": function(data){
                        return moment(data).format('DD.MM.YYYY HH:mm')
                    }
                },
                

            ]

        });
    }
    
    renderDatatable(25, 4);


    function changedStatus(statusVal) {
        var pageLength = $('select[name="datatabletable_length"] option:selected').val();

        mainTable.destroy();
        renderDatatable(pageLength, statusVal);
    }

    // function filter(id) {
    //     table
    //         .columns($('#' + id).attr('table_col'))
    //         .search($('#' + id).val())
    //         .draw();
    //
    //
    //     //table.ajax.reload();
    // }

    // $(window).on("load", function () {
    //    $("#status option").each(function(){
    //     console.log($(this).text());
    //       if($(this).text() == 'Deschis'){
    //         $(this).prop('selected', true);
    //       }
    //    });
    //
    //    filter('status');
    //
    //    $('.activate_sorting').click();
    // });

</script>