<?php include('header.php'); ?>
<style>
   .modal-backdrop.show, .show.blockOverlay {
    opacity: 0;
}
.modal-backdrop,.blockOverlay{
   z-index: 0;
}
modal-title{
   float:left;
}
</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Spalatorie

         </div>

      </div>

   </div>

</div>


<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

        <div class="col-md-12">
          <?php if(check_if_admin()){ ?>
            <button data-toggle="modal" data-target="#addmyModal" class="btn btn-primary btn-sm">Add Service</button>
 <hr>
            <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">
                  <thead>

                     <tr role="row">

                      	<th>Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Action</th>
                     </tr>

                  </thead>

                  <tbody>
                     <?php 
                        foreach($washing_price->result() as $washing)
                        {
                           $status = $washing->status == 1 ? "Active": "Inactive";
                           echo '<tr>';
                           echo '<td>'.$washing->name.'</td>';
                           echo '<td>'.$washing->value.'</td>';
                           echo '<td>'.$status.'</td>';
                           ?>
                           <td><button onclick='editmodel("<?= $washing->ID ?>,<?= $washing->name ?>,<?= $washing->value ?>,<?= $washing->status ?>")' data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm">edit</button></td>
                           <?php
                           echo '</tr>';
                        }
                     ?>
               </tbody>
               </table>
        </div>
        <?php } ?>
        <div class="modal fade" id="addmyModal" role="dialog">
    <div style="top:20%" class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Service</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="<?= base_url('Main_controller/create_service');?>">
             <label>Name</label>
             <input type="text" name="name" class="form-control"/>
             <label>Price</label>
             <input type="number" name="price" class="form-control"/>
             <br>
             <button class="btn btn-primary">Update</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div style="top:20%" class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Service</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="<?= base_url('Main_controller/update_services');?>">
             <input type="hidden" name="id" id="id"/>
             <label>Name</label>
             <input readonly id="name" type="text" name="name" class="form-control"/>
             <label>Price</label>
             <input id="price" type="number" name="price" class="form-control"/>
             <br>
             
             <input type="checkbox" name="status" id="status"/><label> &nbsp;Visible</label>
             <br>
             <button class="btn btn-primary">Update</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <!-- <div class="col-md-12">

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>ID</th>

                      	<th>Name</th>

                        <th>Sosire</th>

                        <th>Telefon</th>

                        <th>Auto</th>

                        <th>Nr. loc parcare</th>
                     </tr>

                  </thead>

                  <tbody>

                    <?php if(!empty($upcoming_washes)) {?>
                  	<?php foreach($upcoming_washes as $key => $p){	?>

				

						<tr>

							<td><a href="/edit_agreement?id=<?php echo $p->ID; ?>"><?php echo $p->ID; ?></a></td>

    					<td> <?php echo $p->name; ?> </td>

    					<td><?php echo format_date($p->checkin_date); ?></td>

              <td><?php echo $p->phone; ?></td>

              <td><?php echo $p->marca ?> <?php echo $p->model; ?> <?php echo $p->nr_inmatriculare; ?></td>

              <td><?php echo $p->parking_spot; ?></td>

						</tr>

                  	<?php }} else { echo '<p class="text-center">No upcoming washes </p>'; }?>

					



                  </tbody>

                  

               </table>

            </div> -->

            <div class="col-md-12">
            	

            </div>

         </div>

   </div>

</div>


<?php include('footer.php'); ?>

<script type="text/javascript">
$(document).ready(function(){
  
//    $('#myModal').on('show.bs.modal', function(e){

// let id = $(e.relatedTarget).data('id');
// let name = $(e.relatedTarget).data('name');
// let price = $(e.relatedTarget).data('price');
// //alert(id);
// $('#id').val(id);
// $('#name').val(name);
// $('#price').val(price);
// alert(id);
  
//    $('#myModal').on('show.bs.modal', function(e){

// let id = $(e.relatedTarget).data('id');
// let name = $(e.relatedTarget).data('name');
// let price = $(e.relatedTarget).data('price');
// //alert(id);
// $('#id').val(id);
// $('#name').val(name);
// $('#price').val(price);
// alert(id);

// });
})
function editmodel(arr){
   let dataArray = arr.split(",");
            let id = dataArray[0];
            let name = dataArray[1];
            let price = dataArray[2];
            let status = dataArray[3];
//alert(id);
$('#id').val(id);
$('#name').val(name);
$('#price').val(price);
   if(status == 1){
      $('#status').prop('checked', true);
   }else{
      $('#status').prop('checked', false);
   }
}
  var table = $('#datatabletable').DataTable({

         columnDefs: [

           { type: 'de_datetime', targets: 2 }

         ],
		 
		 "aaSorting": [[2,'asc']]
     
     

      });
	  
	  

$('#set_washing_price').click(function(){
  var new_price = $('#washing_price').val();

  $.post("/Main_controller/change_washing_price", {new_price: new_price}, function(data, status){

      
      window.location.reload();
      

    });

});

</script>