<?php include('header.php'); ?>

<style type="text/css">


    #btn_calc_price {

        display: block;

        margin: 0 auto;

    }

ol.fill_in_customer {
    padding: 0;
    margin: 0;
    text-align: left;
    color: black !important;
    max-height: 200px;
    overflow: auto;
}
 .fill_in_customer li {
    text-align: left;
    color: black !important;
    border-bottom: 1px solid #d5d5d5;
    padding: 5px !important;
}
  li.client-results.nav-item {
    color: black !important;
}

</style>

<div class="app-page-title">

    <div class="page-title-wrapper">

        <div class="page-title-heading">

            <div class="page-title-icon">

                <i class="pe-7s-car icon-gradient bg-mean-fruit">

                </i>

            </div>

            <div>

                Adauga rezervare

            </div>

        </div>

    </div>

</div>

<div class="tab-pane tabs-animation fade active show" id="tab-content-2" role="tabpanel">

    <div class="main-card mb-3 card">

        <div class="card-body">

            <div id="smartwizard3" class="forms-wizard-vertical sw-main sw-theme-default">

                <ul class="forms-wizard nav nav-tabs step-anchor">

                    <li class="nav-item active">

                        <a href="#step-122" class="nav-link">

                            <em>1</em><span>Simuleaza pretul</span>

                        </a>

                    </li>

                    <li class="nav-item">

                        <a href="#step-222" class="nav-link">

                            <em>2</em><span>Informatiile clientului</span>

                        </a>

                    </li>

                    <li class="nav-item">

                        <a href="#step-322" class="nav-link">

                            <em>3</em><span>Finish Wizard</span>

                        </a>

                    </li>

                    <li class=" client-results" style="display: block">


                            <span>Alege un client existent:</span>

                            <ol class="fill_in_customer">

                            </ol>


                    </li>

                </ul>

                <form class="add_reservation">

                    <div class="form-wizard-content sw-container tab-content" style="min-height: 0px;">

                        <div id="step-122" class="tab-pane step-content">

                            <div class="card-body">

                                <div class="form-row">

                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><label for="">Tip rezervare*</label>

                                            <select id="res_type" name="tip" class="form-control">

                                                <option value="Standard">Standard</option>
                                                <option value="Abonament">Abonament</option>
                                                <option value="Courtesy">Courtesy ( Gratuit )</option>
                                                <option value="ParkVia">Park Via</option>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><label for="">Lista pret*</label>

                                            <select id="price_list" name="price_list" class="form-control">

                                                <?php foreach ($prices_list as $key => $p) { ?>
                                                  

                                                    <option value="<?= $p['id'];?>" <?= ( $p['name'] == 'Preturi website' ) ? 'selected' : ''; ?> ><?= $p['name'];?></option>


                                                <?php } ?>

                                            </select>

                                        </div>

                                    </div>


                                    <div class="col-md-6">

                                        <div class="position-relative form-group"><label>Clientul ajunge in data
                                                de:</label>

                                            <input name="checkout_date" placeholder="date placeholder" type="text" readonly
                                                   class="form-control calc"></div>

                                    </div>


                                    <div class="col-md-6">

                                        <div class="position-relative form-group"><label>Clientul pleaca in data
                                                de:</label>

                                            <input name="checkin_date" placeholder="date placeholder" type="text" readonly 
                                                   class="form-control calc"></div>

                                    </div>


                                </div>


                                <!-- <div class="position-relative form-check"><label class="form-check-label"><input
                                                type="checkbox" name="washing" class="form-check-input">
                                        Spalatorie</label></div>

                                 -->
                                 <div class="position-relative form-check"><label class="form-check-label"><input
                                                type="checkbox" name="send_mail" class="form-check-input"
                                                checked="checked"> Trimite Mail</label></div>
                                                    <?php 
                                                    foreach($services->result() as $service)
                                                    {
echo '<div class="position-relative form-check"><label class="form-check-label"><input
type="checkbox" value="'.$service->ID.'" name="services[]" class="form-check-input services"> '.$service->name.' - '.$service->value.' Ron</label></div>';
                                                    }
                                                    ?>
                                             

                                <br>
                                <br>

                                <div class="col-md-6">

                                    <div class="position-relative form-group"><label>Discount:</label>

                                        <input name="discount" placeholder="Discount" type="text" class="form-control">
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-md-3 col-xl-4">

                                    </div>

                                    <div class="col-sm-12 col-md-6 col-xl-4">

                                        <div class="card mb-3 widget-chart cost_result_wrapper" style="display: none">

                                            <div class="widget-chart-content">

                                                <div class="icon-wrapper rounded">

                                                    <div class="icon-wrapper-bg bg-warning"></div>

                                                    <i class="lnr-laptop-phone text-warning"></i></div>

                                                <div class="widget-numbers">

                                                    <span id="cost_result">3M</span>
                                                    <input type="hidden" name="discounted_price" id="discounted_price">
                                                    <input type="hidden" name="full_price" id="full_price">

                                                </div>


                                            </div>

                                            <div class="widget-chart-wrapper">

                                                <div id="dashboard-sparklines-simple-1" style="min-height: 120px;">
                                                    <div id="apexchartsqlv3ltbv"
                                                         class="apexcharts-canvas apexchartsqlv3ltbv"
                                                         style="width: 501px; height: 120px;">
                                                        <svg id="SvgjsSvg1163" width="501" height="120"
                                                             xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                                             xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                                             style="background: transparent;">
                                                            <g id="SvgjsG1165"
                                                               class="apexcharts-inner apexcharts-graphical"
                                                               transform="translate(0, 0)">
                                                                <defs id="SvgjsDefs1164">
                                                                    <clipPath id="gridRectMaskqlv3ltbv">
                                                                        <rect id="SvgjsRect1168" width="504"
                                                                              height="123" x="-1.5" y="-1.5" rx="0"
                                                                              ry="0" fill="#ffffff" opacity="1"
                                                                              stroke-width="0" stroke="none"
                                                                              stroke-dasharray="0"></rect>
                                                                    </clipPath>
                                                                    <clipPath id="gridRectMarkerMaskqlv3ltbv">
                                                                        <rect id="SvgjsRect1169" width="509"
                                                                              height="128" x="-4" y="-4" rx="0" ry="0"
                                                                              fill="#ffffff" opacity="1"
                                                                              stroke-width="0" stroke="none"
                                                                              stroke-dasharray="0"></rect>
                                                                    </clipPath>
                                                                </defs>
                                                                <rect id="SvgjsRect1167" width="0" height="120" x="0"
                                                                      y="0" rx="0" ry="0" fill="#b1b9c4" opacity="1"
                                                                      stroke-width="0" stroke-dasharray="0"
                                                                      class="apexcharts-xcrosshairs" filter="none"
                                                                      fill-opacity="0.9"></rect>
                                                                <g id="SvgjsG1176" class="apexcharts-xaxis"
                                                                   transform="translate(0, 0)">
                                                                    <g id="SvgjsG1177" class="apexcharts-xaxis-texts-g"
                                                                       transform="translate(0, -4)"></g>
                                                                </g>
                                                                <g id="SvgjsG1180" class="apexcharts-grid">
                                                                    <line id="SvgjsLine1182" x1="0" y1="120" x2="501"
                                                                          y2="120" stroke="transparent"
                                                                          stroke-dasharray="0"></line>
                                                                    <line id="SvgjsLine1181" x1="0" y1="1" x2="0"
                                                                          y2="120" stroke="transparent"
                                                                          stroke-dasharray="0"></line>
                                                                </g>
                                                                <g id="SvgjsG1171"
                                                                   class="apexcharts-line-series apexcharts-plot-series">
                                                                    <g id="SvgjsG1172"
                                                                       class="apexcharts-series series-1"
                                                                       data:longestSeries="true" rel="1"
                                                                       data:realIndex="0">
                                                                        <path id="apexcharts-line-0"
                                                                              d="M 10.4375 63.26824254881809C 17.74375 63.26824254881809 24.00625 86.70092497430628 31.3125 86.70092497430628C 38.61875 86.70092497430628 44.88125 64.50154162384378 52.1875 64.50154162384378C 59.49375 64.50154162384378 65.75625 86.70092497430628 73.0625 86.70092497430628C 80.36875 86.70092497430628 86.63125 53.401849948612536 93.9375 53.401849948612536C 101.24375 53.401849948612536 107.50625 74.36793422404932 114.8125 74.36793422404932C 122.11875 74.36793422404932 128.38125 44.76875642343268 135.6875 44.76875642343268C 142.99375 44.76875642343268 149.25625 53.401849948612536 156.5625 53.401849948612536C 163.86875 53.401849948612536 170.13125 76.83453237410072 177.4375 76.83453237410072C 184.74375 76.83453237410072 191.00625 73.13463514902364 198.3125 73.13463514902364C 205.61875 73.13463514902364 211.88125 66.96813977389516 219.1875 66.96813977389516C 226.49375 66.96813977389516 232.75625 81.76772867420348 240.0625 81.76772867420348C 247.36875 81.76772867420348 253.63125 71.90133607399794 260.9375 71.90133607399794C 268.24375 71.90133607399794 274.50625 62.03494347379239 281.8125 62.03494347379239C 289.11875 62.03494347379239 295.38125 76.83453237410072 302.6875 76.83453237410072C 309.99375 76.83453237410072 316.25625 57.10174717368962 323.5625 57.10174717368962C 330.86875 57.10174717368962 337.13125 96.56731757451182 344.4375 96.56731757451182C 351.74375 96.56731757451182 358.00625 5.3031860226104754 365.3125 5.3031860226104754C 372.61875 5.3031860226104754 378.88125 39.8355601233299 386.1875 39.8355601233299C 393.49375 39.8355601233299 399.75625 43.53545734840698 407.0625 43.53545734840698C 414.36875 43.53545734840698 420.63125 69.43473792394656 427.9375 69.43473792394656C 435.24375 69.43473792394656 441.50625 54.635149023638235 448.8125 54.635149023638235C 456.11875 54.635149023638235 462.38125 50.935251798561154 469.6875 50.935251798561154C 476.99375 50.935251798561154 483.25625 90.40082219938336 490.5625 90.40082219938336"
                                                                              fill="none" fill-opacity="1"
                                                                              stroke="rgba(58,196,125,0.85)"
                                                                              stroke-opacity="1" stroke-linecap="butt"
                                                                              stroke-width="3" stroke-dasharray="0"
                                                                              class="apexcharts-line" index="0"
                                                                              clip-path="url(#gridRectMaskqlv3ltbv)"
                                                                              pathTo="M 10.4375 63.26824254881809C 17.74375 63.26824254881809 24.00625 86.70092497430628 31.3125 86.70092497430628C 38.61875 86.70092497430628 44.88125 64.50154162384378 52.1875 64.50154162384378C 59.49375 64.50154162384378 65.75625 86.70092497430628 73.0625 86.70092497430628C 80.36875 86.70092497430628 86.63125 53.401849948612536 93.9375 53.401849948612536C 101.24375 53.401849948612536 107.50625 74.36793422404932 114.8125 74.36793422404932C 122.11875 74.36793422404932 128.38125 44.76875642343268 135.6875 44.76875642343268C 142.99375 44.76875642343268 149.25625 53.401849948612536 156.5625 53.401849948612536C 163.86875 53.401849948612536 170.13125 76.83453237410072 177.4375 76.83453237410072C 184.74375 76.83453237410072 191.00625 73.13463514902364 198.3125 73.13463514902364C 205.61875 73.13463514902364 211.88125 66.96813977389516 219.1875 66.96813977389516C 226.49375 66.96813977389516 232.75625 81.76772867420348 240.0625 81.76772867420348C 247.36875 81.76772867420348 253.63125 71.90133607399794 260.9375 71.90133607399794C 268.24375 71.90133607399794 274.50625 62.03494347379239 281.8125 62.03494347379239C 289.11875 62.03494347379239 295.38125 76.83453237410072 302.6875 76.83453237410072C 309.99375 76.83453237410072 316.25625 57.10174717368962 323.5625 57.10174717368962C 330.86875 57.10174717368962 337.13125 96.56731757451182 344.4375 96.56731757451182C 351.74375 96.56731757451182 358.00625 5.3031860226104754 365.3125 5.3031860226104754C 372.61875 5.3031860226104754 378.88125 39.8355601233299 386.1875 39.8355601233299C 393.49375 39.8355601233299 399.75625 43.53545734840698 407.0625 43.53545734840698C 414.36875 43.53545734840698 420.63125 69.43473792394656 427.9375 69.43473792394656C 435.24375 69.43473792394656 441.50625 54.635149023638235 448.8125 54.635149023638235C 456.11875 54.635149023638235 462.38125 50.935251798561154 469.6875 50.935251798561154C 476.99375 50.935251798561154 483.25625 90.40082219938336 490.5625 90.40082219938336"
                                                                              pathFrom="M -1 120L -1 120L 31.3125 120L 52.1875 120L 73.0625 120L 93.9375 120L 114.8125 120L 135.6875 120L 156.5625 120L 177.4375 120L 198.3125 120L 219.1875 120L 240.0625 120L 260.9375 120L 281.8125 120L 302.6875 120L 323.5625 120L 344.4375 120L 365.3125 120L 386.1875 120L 407.0625 120L 427.9375 120L 448.8125 120L 469.6875 120L 490.5625 120"></path>
                                                                        <g id="SvgjsG1173"
                                                                           class="apexcharts-series-markers-wrap"></g>
                                                                        <g id="SvgjsG1174"
                                                                           class="apexcharts-datalabels"></g>
                                                                    </g>
                                                                </g>
                                                                <line id="SvgjsLine1183" x1="0" y1="0" x2="501" y2="0"
                                                                      stroke="#b6b6b6" stroke-dasharray="0"
                                                                      stroke-width="1"
                                                                      class="apexcharts-ycrosshairs"></line>
                                                                <line id="SvgjsLine1184" x1="0" y1="0" x2="501" y2="0"
                                                                      stroke-dasharray="0" stroke-width="0"
                                                                      class="apexcharts-ycrosshairs-hidden"></line>
                                                                <g id="SvgjsG1185"
                                                                   class="apexcharts-yaxis-annotations"></g>
                                                                <g id="SvgjsG1186"
                                                                   class="apexcharts-xaxis-annotations"></g>
                                                                <g id="SvgjsG1187"
                                                                   class="apexcharts-point-annotations"></g>
                                                            </g>
                                                            <g id="SvgjsG1178" class="apexcharts-yaxis" rel="0"
                                                               transform="translate(-35, 0)">
                                                                <g id="SvgjsG1179" class="apexcharts-yaxis-texts-g"></g>
                                                            </g>
                                                        </svg>
                                                        <div class="apexcharts-legend"></div>
                                                    </div>
                                                </div>

                                                <div class="resize-triggers">
                                                    <div class="expand-trigger">
                                                        <div style="width: 502px; height: 121px;"></div>
                                                    </div>
                                                    <div class="contract-trigger"></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>


                                <button class="btn-shadow btn-wide btn btn-success btn-lg" id="btn_calc_price">
                                    Calculeaza pret
                                </button>


                            </div>

                        </div>

                        <div id="step-222" class="tab-pane step-content">

                          <input name="clientid" placeholder="with a placeholder" type="text" hidden  class="form-control">


                            <div class="position-relative form-group"><label for="">Nume si prenume *</label><input
                                        name="name" id="" placeholder="with a placeholder" type="text"
                                        class="form-control"></div>

                            <div class="position-relative form-group"><label for="">Email *</label><input name="email"
                                                                                                          id=""
                                                                                                          placeholder="with a placeholder"
                                                                                                          type="email"
                                                                                                          class="form-control">
                            </div>

                            <div class="position-relative form-group"><label for="">Telefon *</label><input name="phone"
                                                                                                            id=""
                                                                                                            placeholder="with a placeholder"
                                                                                                            type="text"
                                                                                                            class="form-control">
                            </div>

                            <div class="position-relative form-group">

                              <style type="text/css">
                                
                                  label.form-check-label {
                                      padding-left: 20px;
                                  }

                              </style>
                              
                              <label class="form-check-label" for="pers_jur"><input type="checkbox" class="form-check-input" id="pers_jur">Persoana juridica</label>
                            </div>
                            <br>


                            <div id="pers_jur_details" style="display: none;">

                                <!-- <div class="position-relative form-group"><label for="">Firma</label><input name="firma" 
                                                                                                            id="firma"
                                                                                                            placeholder=""
                                                                                                            type="text"
                                                                                                            class="form-control">
                                </div>-->

                                <div class="position-relative form-group"><label for="">CUI</label><input name="CUI"
                                                                                                          id="CUI"
                                                                                                          placeholder=""
                                                                                                          type="text"
                                                                                                          class="form-control">
                                </div>

                            </div>


                        </div>

                        <div id="step-322" class="tab-pane step-content">

                            <div class="no-results">

                                <div class="swal2-icon swal2-success swal2-animate-success-icon">

                                    <div class="swal2-success-circular-line-left"
                                         style="background-color: rgb(255, 255, 255);"></div>

                                    <span class="swal2-success-line-tip"></span>

                                    <span class="swal2-success-line-long"></span>

                                    <div class="swal2-success-ring"></div>

                                    <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>

                                    <div class="swal2-success-circular-line-right"
                                         style="background-color: rgb(255, 255, 255);"></div>

                                </div>

                                <div class="results-subtitle mt-4">Ultimul pas este sa salvezi!</div>

                                <div class="results-title">Doresti sa verifici din nou informatiile?</div>

                                <div class="mt-3 mb-3"></div>

                                <div class="text-center">

                                    <button class="btn-shadow btn-wide btn btn-success btn-lg" id="save_reservation">
                                        Salveaza rezervarea
                                    </button>

                                </div>

                            </div>

                        </div>

                    </div>

                </form>

            </div>

            <div class="divider"></div>

            <div class="clearfix">


                <button type="button" id="next-btn22"
                        class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-primary">Inainte
                </button>

                <button type="button" id="prev-btn22"
                        class="btn-shadow float-right btn-wide btn-pill mr-3 btn btn-outline-secondary">Inapoi
                </button>

            </div>

        </div>

    </div>

</div>

<?php include('footer.php'); ?>

<script type="text/javascript">


    $(document).ready(function () {

      function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this, args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function () {
            callback.apply(context, args);
          }, ms || 0);
        };
      }


      $('.client-results').hide()

      $('[name="name"], [name="email"], [name="phone"]').keyup(delay(function (e) {

        $('.fill_in_customer').html('')

        if( this.value.length < 4 ){
          return false
        }

        $('.client-results').show()

       $.post('/Main_controller/find_client', {'q':this.value}, function(r){

          $('[name="clientid"]').val('')

          $.each(JSON.parse(r), function(i, item) {

              $('.fill_in_customer').append('<li data-id="'+item.ID+'"><span class="name">'+item.name+'</span>, <span class="email">'+item.email+'</span>, <span class="phone">'+item.phone+'</span></li>')

          })

       })

      }, 500));

      $(document).on('click','.fill_in_customer li',function(){

            $('[name="clientid"]').val( $(this).attr('data-id') )
            $('[name="name"]').val( $(this).find('.name').text() )
            $('[name="email"]').val( $(this).find('.email').text() )
            $('[name="phone"]').val( $(this).find('.phone').text() )

      })


        $('#res_type').on('change', function () {
            if (this.value == 'Abonament') {
                window.location.href = '/add_membership_reservation';
            }
        });


        $("#next-btn22").click(function (e) {
            if ($('.nav-item.active').find('span').text() == 'Informatiile clientului') {


                var name = $('input[name="name"]').val();
                var email = $('input[name="email"]').val();
                var phone = $('input[name="phone"]').val();

                if (name == '' || email == '' || phone == '') {
                    alert('Te rugam sa completezi toate informatiile necesare!');
                    $("#prev-btn22").click();
                }

            }
        });

        $('#pers_jur').click(function () {
            $("#pers_jur_details").toggle();
        });


        $.datetimepicker.setLocale('ro');

        $('[name="checkout_date"]').datetimepicker({

            i18n: {

                ro: {

                    months: [

                        'Ianuare', 'Februarie', 'Martie', 'Aprilie',

                        'Mai', 'Iunie', 'Iulie', 'August',

                        'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie',

                    ],

                    dayOfWeek: [

                        "Du", "Lu", "Ma", "Mi",

                        "Jo", "Vi", "Sa",

                    ]

                }

            },

            timepicker: true,
            step: 15,
            format: 'd/m/Y H:i',
            mask: '39/19/9999 29:59',
            minDate: new Date(),
            onChangeDateTime:function(dp,$input){
              console.log($input.val())
              $('[name="checkout_date"]').trigger('change')
             $('[name="checkin_date"]').datetimepicker({minDate: dp})
           }

        })




        $('[name="checkin_date"]').datetimepicker({

            i18n: {

                ro: {

                    months: [

                        'Ianuare', 'Februarie', 'Martie', 'Aprilie',

                        'Mai', 'Iunie', 'Iulie', 'August',

                        'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie',

                    ],

                    dayOfWeek: [

                        "Du", "Lu", "Ma", "Mi",

                        "Jo", "Vi", "Sa",

                    ]

                }

            },
            timepicker: true,
            step: 15,
            format: 'd/m/Y H:i',

            mask: '39/19/9999 29:59'

        });


        $('#btn_calc_price').click(function (e) {

            e.preventDefault();


            calc_price();

        });


        $('[name="washing"]').click(function () {


            calc_price();

        });


        function calc_price() {


            var checkout_date = $('[name="checkout_date"]').val();

            var checkin_date = $('[name="checkin_date"]').val();

            //var washing = $('[name="washing"]').prop('checked');
         
           
            var washing = []
        $("input[name='services[]']:checked").each(function ()
        {
            washing.push(parseInt($(this).val()));
        });
       // console.log(washing);
            
            var price_list = $('[name="price_list"]').val();


            var data = {checkout: checkout_date, checkin: checkin_date, washing: washing, price_list: price_list};

            $.post("/Main_controller/new_compute_reservation_price", data, function (data, status) {


                var data = JSON.parse(data);


                $('.cost_result_wrapper').show();

                var discount = $('[name="discount"]').val();

                if (discount.indexOf('%') > -1) {
                    var totall = parseInt(data.total) - ((parseInt(discount) * parseInt(data.total)) / 100);

                    $('#cost_result').text(totall + ' RON pentru ' + data.days + ' zile');
                    $('#discounted_price').val(totall);
                    $('#full_price').val(data.total);
                    return false;
                }

                if (discount == '') {
                    $('#cost_result').text(data.total + ' RON pentru ' + data.days + ' zile');
                    $('#discounted_price').val(data.total);
                    $('#full_price').val(data.total);
                    return false;
                } else {
                    var totall = parseInt(data.total) - parseInt(discount);
                    $('#cost_result').text(totall + ' RON pentru ' + data.days + ' zile');
                    $('#discounted_price').val(totall);
                    $('#full_price').val(data.total);
                    return false;
                }


            });

        }


        $('#save_reservation').click(function (e) {

            e.preventDefault();

            $('#save_reservation').hide();


            var data = $('form').serialize();

            // var price = $('#discounted_price').val();
            // var discount = $('[name="discount"]').val();

            $.post("/Main_controller/save_reservation", data, function (data, status) {


                var data = JSON.parse(data);                

                if (data.success == false) {

                    toastr.error(data.msg, 'Eroare!');

                } else if (data.success == true) {

                    toastr.success(data.msg, 'Success!');

                    setTimeout(function(){ window.location.href = "/edit_reservation?id="+data.res_id; }, 500);

                } else {

                    toastr.error('Cauza neidentificata', 'Eroare!');

                }


            });

        });


    });


</script>