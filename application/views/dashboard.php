<?php include('header.php'); ?>
<div class="app-page-title" style="display: none">
	<div class="page-title-wrapper">
		<div class="page-title-heading">
			<div class="page-title-icon">
				<i class="pe-7s-car icon-gradient bg-mean-fruit"></i>
			</div>
			<div>
				Sistem management parcare
			</div>
		</div>
	</div>
</div>
<div class="tabs-animation">
	<div class="mb-3 card" style="display: none">
		<div class="card-header-tab card-header">
			<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
				<i class="header-icon lnr-charts icon-gradient bg-happy-green"></i>
				Performanta lunii

				<?php echo date('m'); ?>
			</div>
		</div>
		<div class="no-gutters row">
			<div class="col-sm-6 col-md-3 col-xl-3">
				<div class="card no-shadow rm-border bg-transparent widget-chart text-left">
					<div class="icon-wrapper rounded-circle">
						<div class="icon-wrapper-bg opacity-10 bg-warning"></div>
						<i class="lnr-laptop-phone text-dark opacity-8"></i>
					</div>
					<div class="widget-chart-content">
						<div class="widget-subheading">Rata de utilizare
						</div>
						<div class="widget-numbers">50%
						</div>
						<div class="widget-description opacity-8 text-focus">
							<div class="d-inline text-danger pr-1">
								<i class="fa fa-angle-down"></i>
								<span class="pl-1">54.1%
                </span>
							</div>
							fata de saptamana trecuta

						</div>
					</div>
				</div>
				<div class="divider m-0 d-md-none d-sm-block"></div>
			</div>
			<div class="col-sm-6 col-md-3 col-xl-3">
				<div class="card no-shadow rm-border bg-transparent widget-chart text-left">
					<div class="icon-wrapper rounded-circle">
						<div class="icon-wrapper-bg opacity-9 bg-danger"></div>
						<i class="lnr-graduation-hat text-white"></i>
					</div>
					<div class="widget-chart-content">
						<div class="widget-subheading">De incasat
						</div>
						<div class="widget-numbers">
              <span>200 RON
              </span>
						</div>
						<!--
			<div class="widget-description opacity-8 text-focus">
			Fata de luna trecuta:
			<span class="text-info pl-1"><i class="fa fa-angle-down"></i><span class="pl-1">14.1%</span></span></div>
			-->
					</div>
				</div>
				<div class="divider m-0 d-md-none d-sm-block"></div>
			</div>
			<div class="col-sm-12 col-md-3 col-xl-3">
				<div class="card no-shadow rm-border bg-transparent widget-chart text-left">
					<div class="icon-wrapper rounded-circle">
						<div class="icon-wrapper-bg opacity-9 bg-success"></div>
						<i class="lnr-apartment text-white"></i>
					</div>
					<div class="widget-chart-content">
						<div class="widget-subheading">Deja incasat
						</div>
						<div class="widget-numbers text-success">
              <span>3000 RON
              </span>
						</div>
						<div class="widget-description text-focus"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3 col-xl-3">
				<div class="card no-shadow rm-border bg-transparent widget-chart text-left">
					<div class="icon-wrapper rounded-circle">
						<div class="icon-wrapper-bg opacity-9 bg-success"></div>
						<i class="lnr-apartment text-white"></i>
					</div>
					<div class="widget-chart-content">
						<div class="widget-subheading">Total zile stationare
						</div>
						<div class="widget-numbers text-success">
              <span>581 zile
              </span>
						</div>
						<div class="widget-description text-focus"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center d-block p-3 card-footer">
			<button class="btn-pill btn-shadow btn-wide fsize-1 btn btn-primary btn-lg">
        <span class="mr-2 opacity-7">
          <i class="icon icon-anim-pulse ion-ios-analytics-outline"></i>
        </span>
				<span class="mr-1">Doresti mai multe rapoarte?
        </span>
			</button>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-lg-6">
			<div class="card-hover-shadow-2x mb-3 card">
				<div class="card-header-tab card-header">
					<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
						<i class="header-icon lnr-users icon-gradient bg-amy-crisp"></i>
						<strong><?php echo $next_days_reservations; ?></strong>&nbsp; rezervari in urmatoarele 7 zile | Intrari
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-lg-6">
			<div class="card-hover-shadow-2x mb-3 card">
				<div class="card-header-tab card-header">
					<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
						<i class="header-icon lnr-users icon-gradient bg-amy-crisp"></i>
						<strong><?php echo $next_days_exits; ?></strong>&nbsp; contracte in urmatoarele 7 zile | Iesiri
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-lg-6">
			<div class="card-hover-shadow-2x mb-3 card">
				<div class="card-header-tab card-header">
					<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
						<i class="header-icon lnr-users icon-gradient bg-amy-crisp"></i>
						Ultimele rezervari

					</div>
				</div>
				<div class="scroll-area-lg">
					<div class="scrollbar-container">
						<div class="p-4">
							<?php foreach ($latest_reservations as $res) { ?>
								<div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
									<div class="vertical-timeline-item dot-success vertical-timeline-element">
										<div>
											<span class="vertical-timeline-element-icon bounce-in"></span>
											<div class="vertical-timeline-element-content bounce-in">
												<h4 class="timeline-title">
													<a href="/edit_reservation?id=<?php echo $res['ID']; ?>"><?php echo $res['ID']; ?></a>
													- <?php echo $res['name']; ?> -
													<strong><?php echo date('d-m-Y H:i:s', strtotime($res['checkout_date'])); ?></strong>
													pana la
													<strong><?php echo date('d-m-Y H:i:s', strtotime($res['checkin_date'])); ?> </strong>
												</h4>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="d-block text-center card-footer">
					<a href="/reservations" class="btn-shadow btn-wide btn-pill btn btn-focus">
            <span class="badge badge-dot badge-dot-lg badge-warning badge-pulse">Badge
            </span>
						Vezi toate rezervarile

					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-lg-6">
			<div class="card-hover-shadow-2x mb-3 card">
				<div class="card-header-tab card-header">
					<div class="card-header-title font-size-lg font-weight-normal">
						<i class="header-icon lnr-sync icon-gradient bg-amy-crisp"></i>
						Urmatoarele abonamente expira in 3 zile sau mai putin:

					</div>
				</div>
				<div class="scroll-area-lg">
					<div class="scrollbar-container">
						<div class="p-4">

							<?php foreach ($memberships as $key => $m) { ?>

								<div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column"
									 m-id="<?php echo $m['ID']; ?>">
									<div class="vertical-timeline-item dot-success vertical-timeline-element">
										<div>
											<span class="vertical-timeline-element-icon bounce-in"></span>
											<div class="vertical-timeline-element-content bounce-in">
												<h4 class="timeline-title">

													<?php echo $m['name']; ?>
													- <?php echo $m['model']; ?> <?php echo $m['make']; ?> <?php echo $m['nr_inmatriculare']; ?>
													- <?php echo $m['phone']; ?> - <?php echo $m['price']; ?>
													RON <?php echo $m['type']; ?>

													<button class="btn-pill btn-shadow btn-wide btn btn-primary btn-sm"
															id="renew_membership" m-id="<?php echo $m['ID']; ?>">
														<span class="mr-1">Prelungeste</span>
													</button>

												</h4>
											</div>
										</div>
									</div>
								</div>

							<?php } ?>


						</div>
					</div>
				</div>
				<div class="d-block text-center card-footer">

					<a href="/add_membership" class="btn-shadow btn-wide btn-pill btn btn-focus">
            <span class="badge badge-dot badge-dot-lg badge-warning badge-pulse">Badge
            </span>
						Vezi toate abonamentele

					</a>
				</div>

			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>
<script type="text/javascript">

	$(document).ready(function () {

		$('#renew_membership').click(function () {

			var m_id = $(this).attr('m-id');

			$.post("/Main_controller/renew_membership/" + m_id, function (data) {


				if (data == '1') {

					toastr.success('Abonament reinnoit cu succes!', 'Succes!');

					$('.vertical-time-simple[m-id="' + m_id + '"]').hide(500);

				} else {

					toastr.error('A avut loc o eroare!', 'Error!');

				}

			});

		});


	});

</script>
