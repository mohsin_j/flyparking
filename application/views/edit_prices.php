<?php include('header.php'); ?>


<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Lista: <strong><?= $price_list->name; ?></strong> <small>( <?= $price_list->start_date; ?> - <?= $price_list->end_date; ?> )</small>

         </div>

      </div>

   </div>

</div>


<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

          
            <div class="col-md-12">

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>Zile</th>

                      	<th>Pret in RON</th>

                     </tr>

                  </thead>

                  <tbody>
					
                  <?php $i = 1; ?>
                  <?php while( $i <= 31 ) { ?>
                      <tr>
                         
                            <td><input type="text" class="days" data-index="<?php echo $i; ?>" value="<?php echo $i; ?>" readonly></td>
                            <td><input type="text" class="price" data-index="<?php echo $i; ?>" value="<?php echo (!empty($prices[$i - 1]->price)) ? $prices[$i - 1]->price : 0 ?>"></td>

                      </tr>
                  <?php $i++; } ?>



                  </tbody>

                  

               </table>

               


            

            </div>

            
         </div>



   </div>

</div>

<div class="main-card mb-3 card">

   <div class="card-body">


              <h3>Copiaza lista preturi</h3>
           
              <?php echo form_open('main_controller/copy_price_list'); ?>
              <div class="row">
                <div class="col-md-6">
                  <label>
                    Denumire lista noua:
                    <input type="text" class="form-control input-medium" name="name" value="<?= $price_list->name; ?>">


                  </label>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <label>
                    Perioada inceput lista noua:
                    <input type="text" class="form-control input-medium" name="start_date" value="<?= $price_list->start_date; ?>">


                  </label>

                </div>
              </div>

               <div class="row">
                <div class="col-md-6">
                  <label>
                    Perioada final lista noua:
                    <input type="text" class="form-control input-medium" name="end_date" value="<?= $price_list->end_date; ?>">


                  </label>

                </div>
              </div>

              <input type="text" class="form-control input-medium" hidden name="copy_price_list_id" value="<?= $price_list->id; ?>">
            <hr />
            <input type="submit" value="Copiaza" class="btn btn-lg btn-info" />
              <?= form_close(); ?>
            


 </div>

</div>
<?php include('footer.php'); ?>

<script type="text/javascript">

  function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}


  $(document).ready(function(){

      $('.price').keyup(delay(function (e) {

        var element = $(this);

        element.parents('tr').css({'background':'white'})

        var data = [];

        $('.price').each(function( index, value ) {
            
            data.push({'day':value.getAttribute("data-index") , 'price' : value.value, 'price_list_id':<?= $price_list->id; ?>})

        });

        var info = {'data':data};

        $.post( "/Main_controller/update_prices",info, function( data ) {
       
            element.parents('tr').css({'background':'#d5ffd5'})

        });


      }, 500));

      $('[name="start_date"]').datetimepicker({

            i18n: {

                ro: {

                    months: [

                        'Ianuare', 'Februarie', 'Martie', 'Aprilie',

                        'Mai', 'Iunie', 'Iulie', 'August',

                        'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie',

                    ],

                    dayOfWeek: [

                        "Du", "Lu", "Ma", "Mi",

                        "Jo", "Vi", "Sa",

                    ]

                }

            },

            timepicker: false,
            step: 15,
            format: 'Y-m-d',

        });


      $('[name="end_date"]').datetimepicker({

            i18n: {

                ro: {

                    months: [

                        'Ianuare', 'Februarie', 'Martie', 'Aprilie',

                        'Mai', 'Iunie', 'Iulie', 'August',

                        'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie',

                    ],

                    dayOfWeek: [

                        "Du", "Lu", "Ma", "Mi",

                        "Jo", "Vi", "Sa",

                    ]

                }

            },

            timepicker: false,
            step: 15,
            format: 'Y-m-d',

        });

  });


</script>