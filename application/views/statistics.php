<?php include('header.php'); ?>


<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Spalatorie

         </div>

      </div>

   </div>

</div>


<div class="main-card mb-3 card">

   <div class="card-body">

    <!-- Begin table row-->
      <div class="row">

        <div class="col-md-12">

          <h1>Nr Rezervari / Luna</h1>         

        </div>

            <div class="col-md-12">

               <table style="width: 100%;" class=" datatabletable mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>Luna - An</th>

                      	<th>Nr Rezervari</th>

                     </tr>

                  </thead>

                  <tbody>

                    <?php if(!empty($count_res_by_month)) {?>
                    <?php foreach($count_res_by_month as $year => $data){ ?>
                      <?php foreach($data as $month => $d){ ?>

        

                          <tr>

                            <td> <?php echo '01.'.$month.'.'.$year; ?> </td>

                            <td> <?php echo $d; ?> </td>

                          </tr>

                    <?php }}} else { echo '<p class="text-center">No data </p>'; }?>

          



                  </tbody>

                  

               </table>

            </div>

            <div class="col-md-12">
            	

            </div>

         </div>
         <!-- End table row-->

          <!-- Begin table row-->
      <div class="row">

        <div class="col-md-12">

          <h1>Incasari agreement / luna</h1>         

        </div>

            <div class="col-md-12">

               <table style="width: 100%;" class=" datatabletable mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>Luna - An</th>

                        <th>Total RON</th>

                        <th>Total EUR ( 4.7 RON )</th>

                     </tr>

                  </thead>

                  <tbody>

                    <?php if(!empty($count_agr_paid_by_month)) {?>
                    <?php foreach($count_agr_paid_by_month as $year => $data){ ?>
                      <?php foreach($data as $month => $d){ ?>

        

                          <tr>

                            <td> <?php echo '01.'.$month.'.'.$year; ?> </td>

                            <td> <?php echo $d; ?> </td>

                            <td> <?php echo ceil($d / 4.7); ?> </td>

                          </tr>

                    <?php }}} else { echo '<p class="text-center">No data </p>'; }?>

          



                  </tbody>

                  

               </table>

            </div>

            <div class="col-md-12">
              

            </div>

         </div>
         <!-- End table row-->


               <!-- Begin table row-->
      <div class="row">

        <div class="col-md-12">

          <h1>Incasari / luna din rezervari </h1>         

        </div>

            <div class="col-md-12">

               <table style="width: 100%;" class=" datatabletable mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                        <th>Luna - An</th>

                        <th>Total RON</th>

                        <th>Total EUR ( 4.7 RON )</th>

                     </tr>

                  </thead>

                  <tbody>

                    <?php if(!empty($count_future_res_payment_by_month)) {?>
                    <?php foreach($count_future_res_payment_by_month as $year => $data){ ?>
                      <?php foreach($data as $month => $d){ ?>


                          <tr>

                            <td> <?php echo '01.'.$month.'.'.$year; ?> </td>

                            <td> <?php echo $d; ?> </td>

                            <td> <?php echo ceil($d / 4.7); ?> </td>

                          </tr>

                    <?php }}} else { echo '<p class="text-center">No data </p>'; }?>

          



                  </tbody>

                  

               </table>

            </div>

            <div class="col-md-12">
              

            </div>

         </div>
         <!-- End table row-->

   </div>

</div>


<?php include('footer.php'); ?>

<script type="text/javascript">


  var table = $('.datatabletable').DataTable({

    columnDefs: [

           { type: 'de_datetime', targets: 1 }

         ],
     
     "aaSorting": [[1,'asc']]

  });
	  


</script>