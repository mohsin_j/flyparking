<?php include('header.php'); ?>

<style type="text/css">

	

	#btn_calc_price{

		display: block;

		margin: 0 auto;

	}



</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Adauga rezervare de tip Abonament

         </div>

      </div>

   </div>

</div>

<div class="tab-pane tabs-animation fade active show" id="tab-content-2" role="tabpanel">

   <div class="main-card mb-3 card">

      <div class="card-body">

         <div id="smartwizard3" class="forms-wizard-vertical sw-main sw-theme-default">

            <ul class="forms-wizard nav nav-tabs step-anchor">

               <li class="nav-item active">

                  <a href="#step-122" class="nav-link">

                  <em>1</em><span>Alege clientul</span>

                  </a>

               </li>


               <li class="nav-item">

                  <a href="#step-222" class="nav-link">

                  <em>2</em><span>Finalizeaza rezervarea</span>

                  </a>

               </li>

            </ul>

            <form class="add_reservation">

               <div class="form-wizard-content sw-container tab-content" style="min-height: 0px;">

                  <div id="step-122" class="tab-pane step-content" style="display: block;">

                     <div class="card-body">

                        <div class="form-row">

                        	<div class="col-md-12">
	                        	<div class="position-relative form-group"><label for="">Tip rezervare*</label>

			                        <select id="res_type" name="tip" class="form-control">
			                          
			                            <option value="Abonament">Abonament</option>

			                        </select>

			                     </div>
                        		
                        	</div>

                          </div>

                          <div class="form-row">
                            <div class="col-md-12">

                              <div class="position-relative form-group"><label >Alege clientul:</label>

                               <select class="form-control" name="customer_id">
                                 

                                <?php foreach ($memberships as $key => $value) {

                                    ?>

                                      <option value="<?php echo $value['customer_id']; ?>"><?php echo $value['customer_name']; ?></option>

                                    <?php

                                }
                                ?>


                               </select>

                           </div>
                           </div>
                           </div>

                          <div class="form-row">
                           <div class="col-md-6">

                              <div class="position-relative form-group"><label >Clientul ajunge in data de:</label>

                              	<input name="checkout_date"  placeholder="date placeholder" type="text" class="form-control calc"></div>

                           </div>

                           

                           <div class="col-md-6">

                              <div class="position-relative form-group"><label >Clientul pleaca in data de:</label>

                              	<input name="checkin_date"  placeholder="date placeholder" type="text" class="form-control calc"></div>

                           </div>

                           </div>

						
                         

            

                        </div>



                           

						

                  </div>

                 

                  <div id="step-222" class="tab-pane step-content">

                     <div class="no-results">

                        <div class="swal2-icon swal2-success swal2-animate-success-icon">

                           <div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>

                           <span class="swal2-success-line-tip"></span>

                           <span class="swal2-success-line-long"></span>

                           <div class="swal2-success-ring"></div>

                           <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>

                           <div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>

                        </div>

                        <div class="results-subtitle mt-4">Ultimul pas este sa salvezi!</div>

                        <div class="results-title">Doresti sa verifici din nou informatiile?</div>

                        <div class="mt-3 mb-3"></div>

                        <div class="text-center">

                           <button class="btn-shadow btn-wide btn btn-success btn-lg" id="save_reservation">Salveaza rezervarea</button>

                        </div>

                     </div>

                  </div>

               </div>

            </form>

         </div>

         <div class="divider"></div>

         <div class="clearfix">

            <button type="button" id="next-btn22" class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-primary">Inainte</button>

            <button type="button" id="prev-btn22" class="btn-shadow float-right btn-wide btn-pill mr-3 btn btn-outline-secondary">Inapoi</button>

         </div>

      </div>

   </div>

</div>

<?php include('footer.php'); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"  />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script type="text/javascript">

	



$( document ).ready(function() {

  $(document).find('[name="customer_id"]').select2();


	$('#res_type').on('change', function() {
	 	if(this.value == 'Abonament'){
	 		window.location.href = '/add_membership';
	 	}
	});



$.datetimepicker.setLocale('ro');

$('[name="checkout_date"]').datetimepicker({

 i18n:{

  ro:{

   months:[

    'Ianuare','Februarie','Martie','Aprilie',

    'Mai','Iunie','Iulie','August',

    'Septembrie','Octombrie','Noiembrie','Decembrie',

   ],

   dayOfWeek:[

    "Du", "Lu", "Ma", "Mi", 

    "Jo", "Vi", "Sa",

   ]

  }

 },

 timepicker:true,

 format:'d/m/Y H:i',

 mask:'39/19/9999 29:59'

});



$('[name="checkin_date"]').datetimepicker({

 i18n:{

  ro:{

   months:[

    'Ianuare','Februarie','Martie','Aprilie',

    'Mai','Iunie','Iulie','August',

    'Septembrie','Octombrie','Noiembrie','Decembrie',

   ],

   dayOfWeek:[

    "Du", "Lu", "Ma", "Mi", 

    "Jo", "Vi", "Sa",

   ]

  }

 },

 timepicker:true,

 format:'d/m/Y H:i',

 mask:'39/19/9999 29:59'

});





$('#save_reservation').click(function(e){

	e.preventDefault();



	var data = $('form').serialize();

	

	  $.post("/Main_controller/save_membership_reservation", data, function(data, status){

       

     	var data = JSON.parse(data);



     	if(data.success == false){

     		toastr.error(data.msg, 'Eroare!');

     	}else if( data.success == true ){

     		 toastr.success(data.msg, 'Success!');

         setTimeout(function(){ window.location.href = "/edit_reservation?id="+data.res_id; }, 500);

     	}else{

     		toastr.error('Cauza neidentificata', 'Eroare!');

     	}



      });

});



});



</script>