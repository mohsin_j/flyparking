<?php include('header.php'); ?>
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">


<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">

			
			<div id="editor" contenteditable="true">

			 	<?php echo $template->text; ?>

			</div>

			</br>
			</br>

			<button type="button" class="btn btn-primary" id="update_template"> Update Template </button>

		</div>
	</div>
</div>


<?php include('footer.php'); ?>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script>

	$('#update_template').click(function(){

		var html = $('#editor').html();
		var template_id = '<?php echo $_GET['id']; ?>';

		$.ajax({

            url: '/Main_controller/update_template',

            dataType: 'json',

			type: 'POST',

            data: {

                id: template_id,

                html: html

            },

			error: function (request, error) {

				console.log(arguments);

			},

            complete: function(res) {

				window.location.reload();

            }

        });


	});

</script>