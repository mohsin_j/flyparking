<!doctype html>

<script>
	let base_url = "<?= base_url() ?>";
</script>
<html lang="ro">

   <!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Jul 2019 22:10:13 GMT -->

   <!-- Added by HTTrack -->

   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

   <!-- /Added by HTTrack -->

   <head>

      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta http-equiv="Content-Language" content="en">

      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

      <title>Fly Parking</title>

      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"

         />

      <meta name="description" content="This is an example dashboard created using build-in elements and components.">

      <!-- Disable tap highlight on IE -->

      <meta name="msapplication-tap-highlight" content="no">

      <link href="<?= base_url('/main.cba69814a806ecc7945a.css') ?>" rel="stylesheet">

       <link rel="stylesheet" href="<?= base_url('/alana-parking.css?v=3') ?>">

      <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" integrity="sha256-DOS9W6NR+NFe1fUhEE0PGKY/fubbUCnOfTje2JMDw3Y=" crossorigin="anonymous" />

      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">



      <style type="text/css">

      	

      .custom_notice {

		    background: red;

		    width: 100%;

		    padding: 20px;

		    text-align: center;

		    color: white;

		    margin-top: 56px;

		}

      thead{

         background:#f2d403 !important;

      }

      </style>



   </head>

   <body>

      <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">

      	

         <div class="app-header header-shadow">

         	

            <div class="app-header__logo">

               <div class="logo-src"></div>

               <div class="header__pane ml-auto">

                  <div>

      

                     <!-- <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">

                     <span class="hamburger-box">

                     <span class="hamburger-inner"></span>

                     </span>

                     </button> -->

                  </div>

               </div>

            </div>

            <div class="app-header__mobile-menu">

               <div>

                  <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">

                  <span class="hamburger-box">

                  <span class="hamburger-inner"></span>

                  </span>

                  </button>

               </div>

            </div>

            <div class="app-header__menu">

               <span>

               <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">

               <span class="btn-icon-wrapper">

               <i class="fa fa-ellipsis-v fa-w-6"></i>

               </span>

               </button>

               </span>

            </div>

            <div class="app-header__content">

            <?php include('menu.php'); ?>

               <div class="app-header-right">

                  <div class="header-dots">





                  </div>

                  <div class="header-btn-lg pr-0">

                     <div class="widget-content p-0">

                        <div class="widget-content-wrapper">

                           <div class="widget-content-left">

                              <div class="btn-group">

                                 <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">

                                 <i class="metismenu-icon pe-7s-user" style="font-size: 42px;"></i>

                                 <i class="fa fa-angle-down ml-2 opacity-8"></i>

                                 </a>

                                 <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">

                                    <div class="dropdown-menu-header">

                                       <div class="dropdown-menu-header-inner bg-info">

                                          <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>

                                          <div class="menu-header-content text-left">

                                             <div class="widget-content p-0">

                                                <div class="widget-content-wrapper">

                                                   <div class="widget-content-right mr-2">

                                                      <a href="/Main_controller/logout"><button class="btn-pill btn-shadow btn-shine btn btn-focus">Logout

                                                      </button></a>

                                                   </div>

                                                </div>

                                             </div>

                                          </div>

                                       </div>

                                    </div>

                                    

                                 </div>

                              </div>

                           </div>

                           <div class="widget-content-left  ml-3 header-user-info">

                              <div class="widget-heading">

                                 <?php echo $_SESSION['auth_username']; ?>

                              </div>

                           </div>

                        </div>

                     </div>

                  </div>

               </div>

            </div>

         </div>



         <div class="custom_notice" style="display: none">

      		Platforma a fost actualizata! Daca intampinati erori, sunati la 0749 879 494.

   		</div>



         <div class="app-main">

            



            <!-- <div class="app-sidebar sidebar-shadow">

               <div class="app-header__logo">

                  <div class="logo-src"></div>

                  <div class="header__pane ml-auto">

                     <div>

                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">

                        <span class="hamburger-box">

                        <span class="hamburger-inner"></span>

                        </span>

                        </button>

                     </div>

                  </div>

               </div>

               <div class="app-header__mobile-menu">

                  <div>

                     <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">

                     <span class="hamburger-box">

                     <span class="hamburger-inner"></span>

                     </span>

                     </button>

                  </div>

               </div>

               <div class="app-header__menu">

                  <span>

                  <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">

                  <span class="btn-icon-wrapper">

                  <i class="fa fa-ellipsis-v fa-w-6"></i>

                  </span>

                  </button>

                  </span>

               </div>

               <?php //include('menu.php'); ?>

            </div> -->

              <div class="app-main__outer">

               <div class="app-main__inner">

