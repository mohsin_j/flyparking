<?php include('header.php'); ?>

<style type="text/css">



	#btn_calc_price{

		display: block;

		margin: 0 auto;

	}



	.washing_total{

		display:none;

		visibility:hidden;

	}



	body .show{

		visibility:visible !important;

	}

	.modal-backdrop.fade.show {
		display: none;
	}

	select#mail_language {
		display: inline;
		transform: translateY(2px);
	}

	h4{
		color: #343a40;
		font-weight: 100;
		font-size: 20px;
	}

    .hide-me-non-admin {
        display: none;
    }
</style>

<div class="app-page-title">

	<div class="page-title-wrapper">

		<div class="page-title-heading">

			<div class="page-title-icon">

				<i class="pe-7s-car icon-gradient bg-mean-fruit">

				</i>

			</div>

			<div>

				Modifica rezervare

			</div>

		</div>

	</div>

</div>

<div class="row">

	<div class="col-md-12">

		<div class="mb-3 card">

			<div class="tabs-lg-alternate card-header">

				<ul class="nav nav-justified">

					<li class="nav-item">

						<a data-toggle="tab" href="#tab-eg9-0" class="nav-link active show">

							<div class="widget-number">Detalii Rezervare</div>
							<h4>Loc parcare <?php echo $res_data['parking_spot']; ?></h4>



						</a></li>
				</ul>

			</div>

			<div class="tab-content">

				<div class="tab-pane active show" id="tab-eg9-0" role="tabpanel">

					<div class="card-body"><p class="mb-0">

						<div class="card-body">

							<?php
							if($res_data['status'] == 1){
								$status = '<button class="mb-2 mr-2 btn btn-success">Confirmat</button>';
							} else if($res_data['status'] == 2){
								$status = '<button class="mb-2 mr-2 btn btn-danger">Anulat</button';
							} else if($res_data['status'] == 3){
								$status = 'No Show';
							} else if($res_data['status'] == 4){
								$status = '<a href="/edit_agreement?id='. get_agreement_by_res_id($_GET['id']).' "><button class="mb-2 mr-2 btn btn-secondary">Agreement</button></a>';
							}
							?>


							<label>Status Rezervare: <?php echo $status; ?> <button type="button" class="btn btn-lg btn-info" data-toggle="modal" data-target="#exampleModal" style="transform: translateY(-5px);">Adauga Plata</button></label>

							<hr />
							<div class="col-md-12">

								<select name="status" class="form-control col-md-3" id="mail_language" style="display:none">

									<option value="0" selected>Fara Mail</option>

									<option value="RO">RO</option>

									<option value="EN">EN</option>

									<option value="IT">IT</option>

									<option value="GE">GE</option>

									<option value="ES">ES</option>

								</select>

								<?php if($res_data['status'] != 2) {?>
									<?php if(check_if_admin()){ ?>
										<button type="button" class="btn btn-danger btn-lg change_status_btn" status="2">Anuleaza</button>
									<?php } ?>
								<?php } ?>

								<?php if($res_data['status'] != 1) {?>
									<button type="button" class="btn btn-success btn-lg change_status_btn" status="1">Confirma</button>
								<?php } ?>

								<?php if($res_data['status'] != 4) {?>
									<button type="button" class="btn btn-primary btn-lg change_status_btn" status="4">Mergi in Agreement</button>
								<?php } ?>


								<button type="button" class="btn btn-warning btn-lg" id="modifica">Modifica Rezervarea</button>



							</div>

							<hr />

							<br>















						</div>



						<form id="reservation_form">



							<div class="form-row">



								<div class="col-md-6">

									<div class="position-relative form-group"><label >Clientul ajunge in data de:</label>



										<input name="checkout_date" placeholder="date placeholder" type="text" class="form-control calc" readonly disabled="disabled"></div>

								</div>



								<div class="col-md-6">

									<div class="position-relative form-group"><label >Clientul pleaca in data de:</label>

										<input name="checkin_date"  placeholder="date placeholder" type="text" class="form-control calc" readonly disabled="disabled"></div>

								</div>



							</div>

							<div class="position-relative form-group"><label for="">Zile in rezervare</label><input readonly disabled="disabled" name="days" value="<?php echo $res_data['days']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control"></div>

							<input type="text" hidden name="reservation_id" value="<?php echo $res_data['ID']; ?>">

							<input type="text" hidden name="customer_id" value="<?php echo $cus_data['ID']; ?>">

							<div class="position-relative form-group"><label for="">Tip rezervare*</label>

								<?php $options = get_res_types(); ?>

								<select name="tip" class="form-control" disabled="disabled" readonly>

									<?php foreach ($options as $key => $value) {
										if($res_data['tip'] == $value){
											echo '<option value="'.$value.'" selected>'.$value.'</option>';
										}else{
											echo '<option value="'.$value.'">'.$value.'</option>';
										}
									}
									?>


								</select>

							</div>


							<div class="position-relative form-group"><label for="">Lista preturi</label>

								<?php $price_list_id = get_res_price_list_id($res_data['ID']); ?>

								<?php $price_list_id = ( $price_list_id == 0 ) ? $website_price_list_id : $price_list_id; ?>

								<select name="price_list" class="form-control" disabled="disabled" readonly>

									<?php foreach ($prices_list as $key => $value) {
										if($price_list_id == $value['id']){
											echo '<option value="'.$value['id'].'" selected>'.$value['name'].'</option>';
										}else{
											echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
										}
									}
									?>


								</select>

							</div>

							<div class="position-relative form-group"><label for="">Nume si prenume</label><input disabled="disabled" name="name" value="<?php echo $cus_data['name']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control"></div>

							<div class="position-relative form-group"><label for="">Email</label><input disabled="disabled" name="email" value="<?php echo $cus_data['email']; ?>" id="" placeholder="with a placeholder" type="email" class="form-control"></div>

							<div class="position-relative form-group"><label for="">Telefon</label><input disabled="disabled" name="phone" value="<?php echo $cus_data['phone']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control"></div>

							<div class="position-relative form-group"><label for="">Total de plata</label><input readonly="readonly" name="total" id="totalToPay" value="<?php echo $res_data['total']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control" readonly></div>

							<div class="position-relative form-group"><label for="">Total initial | fara discount</label><input readonly="readonly" name="initial_total" id="initalTotal" value="<?php echo $res_data['initial_total']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control" readonly></div>

							<div class="position-relative form-group"><label for="">Discount</label><input disabled="disabled" name="discount" value="<?php echo $res_data['discount']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control"></div>

							<div class="position-relative form-group"><label for="">Platit</label><input disabled="disabled" name="total" value="<?php echo $res_data['paid']; ?>" id="" placeholder="with a placeholder" type="text" class="form-control" readonly></div>

							<!-- <div class="position-relative form-check"><label class="form-check-label"><input disabled="disabled" type="checkbox" <?php //echo ( $res_data['washing'] == 1 ) ? 'checked' : '' ; ?> name="washing" class="form-check-input"> Spalatorie</label></div> -->
							<?php 
                                                    foreach($services->result() as $service)
                                                    {
														$check = "";
														if(in_array($service->ID,$service_option)){
															$check = "checked";
														}
echo '<div class="position-relative form-check"><label class="form-check-label"><input disabled="disabled"
type="checkbox" value="'.$service->ID.'" '.$check.' name="services[]" class="form-check-input services"> '.$service->name.' - '.$service->value.' Ron</label></div>';
                                                    }
                                                    ?>
							 <div class="position-relative form-check"><label class="form-check-label"><input disabled="disabled"  type="checkbox" <?php echo ( $res_data['lock_payment'] == 1 ) ? 'checked' : '' ; ?> name="lock_payment" class="form-check-input"> Blocheaza Totalul ( totalul se schimba doar la prelungire )</label></div> 

                            <?php
                                if($res_data['lock_payment'] == 1 && $res_data['locked_price']) { ?>

                                    Pret blocat per zi: <strong><?php echo $res_data['locked_price']; ?> RON </strong>

                            <?php    }
                            ?>

                            <div class="position-relative form-group"><label for="">Calculeaza diferenta folosind lista de preturi:</label>



								<select name="new_price_list" class="form-control" disabled="disabled">

									<option value="0" >Lista actuala</option>
									<option value="<?= $website_price_list_id; ?>" >Preturi Website</option>
									<option value="<?= $website_price_list_prepaid; ?>" >Preturi Website Prepaid</option>
									<option value="<?= $parkavia_price_list_id; ?>" >Preturi Parkavia</option>


								</select>

							</div>

							<div class="row">

								<div class="col-sm-4"></div>

								<div class="col-sm-4">

									<div class="widget-chart widget-chart-hover result">



										<div class="widget-numbers total"><?php echo  $res_data['total']; ?> RON</div>

										<div class="widget-subheading days_total">Pentru <?php echo $res_data['days']; ?> zile</div>

										<div class="widget-subheading washing_total <?php if( $res_data['washing'] == 1 ){ ?> show <?php } ?>">



											Spalatorie: <span class="washing"><?php echo $options_data['value']; ?></span> RON





										</div>

										<div class="widget-subheading">

											Parcare: <span class="subtotal"><?php echo $res_data['washing'] == 1 ?  $res_data['total'] - $options_data['value'] :  $res_data['subtotal']; ?></span> RON

										</div>

									</div>

								</div>





							</div>



							<div class="text-center">

								<button class="btn-shadow btn-wide btn btn-success btn-lg" id="update_reservation">Actualizeaza rezervarea</button>

							</div>



						</form>



						</p></div>

				</div>

			</div>

		</div>

	</div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:60px;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Adauga Plata</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="number" name="paid" class="form-control" id="paid_amount">

				<select  class="form-control" id="payment_type">



					<option value="0">Cash</option>

					<option value="1">Card</option>





				</select>


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="paid">Actualizeaza plata</button>
			</div>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">





	$( document ).ready(function() {



		$('#modifica').click(function(){

			if (confirm('Esti sigur ca vrei sa modifici rezervarea?')) {
				// Save it!
				$('.form-control').attr('disabled', false);
				$('.form-check-input').attr('disabled', false);
			}

		});


		$(".change_status_btn").click(function(){

			var answer = window.confirm("Sunteti sigur?")
			if (answer) {
				//some code
				var status = $(this).attr('status');
				var res_id = '<?php echo $_GET['id']; ?>';
				change_status(status, res_id);
			}
			else {
				return;
			}

		});


		$('#paid').click(function(e){

			e.preventDefault();
			var answer = window.confirm("Sunteti sigur ca vreti sa adaugati aceasta plata?")
			if (answer) {
				//some code
				var paid = $('#paid_amount').val();


				var payment_type = $('#payment_type').val();

				var res_id = $('[name="reservation_id"]').val();


				if(paid == ''){

					alert('Va rugam introduceti suma.');

					return;
				}

				var data = { amount: paid , type: payment_type, res_id: res_id};



				$.post("/Main_controller/add_payment", data, function(data, status){



					var obj = JSON.stringify(data);
					var obj2 = JSON.parse(data);

					if(obj2 === false){
						toastr.error('Suma este mai mare decat suma rezervarii!', 'failed');
					} else {
						toastr.success('Rezervarea a fost modificata!', 'Success!');
						setTimeout(function(){ location.reload(); }, 500);

					}





					//send_mail(res_id, language);


				});


			}
			else {
				return;
			}









		});



		/*$('#change_status').change(function(){



			var status = $(this).val();

			var res_id = $('[name="reservation_id"]').val();



			var data = { status: status, res_id: res_id };

				console.log('yo');

			 $.post("/Main_controller/change_reservation_status", data, function(data, status){



				 toastr.success('Rezervarea a fost modificata!', 'Success!');

				if(status == 3){

					setTimeout(function(){ window.location.href = "/edit_agreement?id="+data.res_id; }, 500);

				}



			  });



		});*/

		function send_mail(res_id){

			var language = $('#mail_language option:selected').val();

			if(language == 0 || language == '0'){
				return;
			}

			var data = { res_id: res_id, language: language };

			$.post("/Main_controller/send_reservation_mail", data, function(data){



				if(data == '1'){

					toastr.success('Rezervarea a fost modificata!', 'Success!');

					//setTimeout(function(){ window.location.href = "/edit_agreement?id="+data.res_id; }, 500);

				}else{

					toastr.danger('Eroare', 'Nu s-a putut trimite E-mail-ul.!');

				}

			});
		}

		function change_status(status, res_id){
			var data = { status: status, res_id: res_id };
			$.post("/Main_controller/change_reservation_status", data, function(data, status1){


				toastr.success('Rezervarea a fost modificata!', 'Success!');

				send_mail(res_id);

				if(parseInt(status) == 4){
					setTimeout(function(){ window.location.href = "/edit_agreement?id="+data; }, 500);
				} else {
					setTimeout(function(){ window.location.reload(); }, 500);

				}

			});
		}



		$.datetimepicker.setLocale('ro');

		$('[name="checkout_date"]').datetimepicker({

			i18n:{

				ro:{

					months:[

						'Ianuare','Februarie','Martie','Aprilie',

						'Mai','Iunie','Iulie','August',

						'Septembrie','Octombrie','Noiembrie','Decembrie',

					],

					dayOfWeek:[

						"Du", "Lu", "Ma", "Mi",

						"Jo", "Vi", "Sa",

					]

				}

			},

			timepicker:true,

			format:'d/m/Y H:i',

			mask:'39/12/9999 29:59',
            onChangeDateTime:function(dp,$input){
             $('[name="checkin_date"]').datetimepicker({minDate: dp})
           }

		}).val('<?php echo date('d/m/Y H:i', strtotime($res_data['checkout_date'])); ?>');



		$('[name="checkin_date"]').datetimepicker({

			i18n:{

				ro:{

					months:[

						'Ianuare','Februarie','Martie','Aprilie',

						'Mai','Iunie','Iulie','August',

						'Septembrie','Octombrie','Noiembrie','Decembrie',

					],

					dayOfWeek:[

						"Du", "Lu", "Ma", "Mi",

						"Jo", "Vi", "Sa",

					]

				}

			},

			timepicker:true,

			format:'d/m/Y H:i',

			mask:'39/12/9999 29:59'

		}).val('<?php echo date('d/m/Y H:i', strtotime($res_data['checkin_date'])); ?>');



		$('[name="checkin_date"]').change(function(e){

			e.preventDefault();



			calc_price();

		});



		$('[name="checkout_date"]').change(function(e){

			e.preventDefault();



			calc_price();

		});


		let unchecked_service = [];
		let checked_service = [];
	

		$('.form-check-input').click(function(){
			// if ($(this).hasClass("form-check-input") && $(this).is(':checked')) {
			
			// 		checked_service.push($(this).val());
			// 		var myIndex = unchecked_service.indexOf($(this).val());
			// 			if (myIndex !== -1) 
			// 			{
			// 				unchecked_service.splice(myIndex, 1);
			// 			}

			// }
			// else {
			// 	unchecked_service.push($(this).val());
			// 	var myIndex = checked_service.indexOf($(this).val());
			// 			if (myIndex !== -1) 
			// 			{
			// 				checked_service.splice(myIndex, 1);
			// 			}

			// }
			// console.log("checked_service => "+ checked_service);
			// console.log("unchecked_service => "+ unchecked_service);
			calc_price();

		});

		$('[name="new_price_list"]').change(function(){





			calc_price();

		});



		function calc_price(){

			var checkout_date = $('[name="checkout_date"]').val();

			var checkin_date = $('[name="checkin_date"]').val();

			// var washing = $('[name="washing"]').prop('checked');
			let washing = []
        $("input[name='services[]']:checked").each(function ()
        {
            washing.push(parseInt($(this).val()));
        });
	
			var res_id = $('[name="reservation_id"]').val();

			var new_price_list = $('[name="new_price_list"]').val();



			var data = { checkout : checkout_date, checkin : checkin_date, washing: washing, res_id: res_id, new_price_list: new_price_list,edit : "edit" };

			$.post("/Main_controller/new_compute_reservation_price", data, function(data, status){



				var data = JSON.parse(data);



				if(washing === true){

					$('.washing_total').show();

					$('.washing_total').addClass('show');

				}else{

					$('.washing_total').hide();

					$('.washing_total').removeClass('show');

				}
				// console.log(data);
				let total = parseInt(data.total);
				let subtotal = parseInt(data.subtotal);
				$('.total').text(total.toFixed(2)+' RON');

				$('.subtotal').text(subtotal.toFixed(2)+'');


				$('#totalToPay').val(total.toFixed(2));

				$('.days_total').text('Pentru '+data.days+' zile');



			});

		}



		$('#update_reservation').click(function(e){

			e.preventDefault();

			var answer = window.confirm("Sunteti sigur?")
			if (answer) {
				//some code
				var data = $('form').serialize();



				$.post("/Main_controller/update_reservation", data, function(data, status){


					if(data == 'false'){

						toastr.error(data.msg, 'Eroare!');

					}else if( data == 'true' ){

						toastr.success('Actualizare cu succes!', 'Success!');

						setTimeout(function(){ location.reload(); }, 500);

					}else{

						toastr.error('Cauza neidentificata', 'Eroare!');

					}



				});
			}
			else {
				return;
			}




		});



	});



</script>
