<?php include('header.php'); ?>


<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Casierie

         </div>

      </div>

   </div>

</div>


<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

            <div class="col-md-12">
                <input class="form-control date_filters col-md-2" placeholder="De la" style="display: inline-block;" value="<?= (isset($_GET['start'])) ? $_GET['start'] : ''; ?>" />

                 <span style="display: inline-block;">-</span> 


                 <input class="form-control date_filters col-md-2" placeholder="Pana la" style="display: inline-block;"  value="<?= (isset($_GET['end'])) ? $_GET['end'] : ''; ?>"/>


                <button type="button" class="btn btn-lg btn-primary" id="apply_interval">Apply Filters</button>
            </div>

            <div class="col-md-12">

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                      	<th>Agreement</th>

                      	<th>Client</th>

                        <th>Suma</th>

                        <th>Data Creare</th>

                        <th>Incasat de</th>

                     </tr>

                  </thead>

                  <tbody>

					<?php $total_amount = 0; ?>

                  	<?php foreach($payments as $key => $p){ if($p->amount !== '0')	{?>

				            

						<tr>

							

							<td><a href="/edit_reservation?id=<?php echo $p->ID; ?>">Vezi Rezervare</a></td>

							     <td><?php echo $p->name; ?></td>
          					<td><?php echo $p->amount; ?></td>

          					<td><?php echo $p->created_at; ?></td>

                    <td><?php echo get_user_by_id($p->created_by); ?></td>


						</tr>

					<?php $total_amount += $p->amount; ?>

                  	<?php }} ?>

					



                  </tbody>

                  

               </table>

            </div>

            <div class="col-md-12">
            	
				<h3 class="text-right">Total: <?php echo $total_amount; ?></h3>

        <button type="button" class="btn btn-secondary" style="float:left;" onclick="window.location.href='/historic_casierie'">Vezi Historic plati Cash</button>


            </div>

         </div>

   </div>

</div>


<?php include('footer.php'); ?>

<script type="text/javascript">

  $("#apply_interval").click(function(){
    var interval = [];

    $('.date_filters').each(function(){
      interval.push($(this).val());
    });

    if(interval[0] !== '' && interval[1] !== ''){
      window.location.href = '/historic_casierie?start='+interval[0]+'&end='+interval[1]+'';
    }



  });
	
	$('.date_filters').datetimepicker({

 i18n:{

  ro:{

   months:[

    'Ianuare','Februarie','Martie','Aprilie',

    'Mai','Iunie','Iulie','August',

    'Septembrie','Octombrie','Noiembrie','Decembrie',

   ],

   dayOfWeek:[

    "Du", "Lu", "Ma", "Mi", 

    "Jo", "Vi", "Sa",

   ]

  }

 },

 timepicker:true,

 format:'Y-m-d H:i:s'

});

</script>