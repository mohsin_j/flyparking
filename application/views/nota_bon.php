<?php include('header.php'); ?>


<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Nota Bon

         </div>

      </div>

   </div>

</div>



<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<div class="position-relative form-group">
	          <label for="">Editeaza Nota:</label>

	          <textarea class="form-control" id="edit_nota_input" rows="5"><?php echo $nota->nota; ?></textarea>
	        </div>

	        <button type="button" class="btn btn-secondary" id="update_nota">Salveaza Nota</button>
		</div>
	</div>
</div>


<?php include('footer.php'); ?>

<script type="text/javascript">
	
$('#update_nota').click(function(){
	var nota = $('#edit_nota_input').val();

	var data = { nota: nota };

    $.post("/Main_controller/update_nota", data, function(data, status){

      var data = JSON.parse(data);

      if(data){
        window.location.reload();
      }

    });
});

</script>