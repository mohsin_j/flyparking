<?php include('header.php'); ?>

<style type="text/css">

	
	.select_car {
	    text-align: center;
	    border: 1px solid;
	    padding: 10px;
	    margin: 5px 0;
	    color: white;
	    background: #16aaff;
	    display: inline-block;
	    cursor: pointer;
	}

	.select_car::after {
	    position: relative;
	    content: "";
	    display: inline-block;
	    width: 0.4em;
	    height: 0.4em;
	    border-right: 0.2em solid white;
	    border-top: 0.2em solid white;
	    transform: rotate(	135deg	) translateY(-1px) translateX(-5px);
	    margin-right: 0.5em;
	}

	.car {
	    padding: 15px;
	    border-bottom: 1px solid;
	}

	.cars_select {
	    background: white;
	    color: #16aaff;
	    margin-top: 10px;
	}

	#btn_calc_price{

		display: block;

		margin: 0 auto;

	}

	

	.washing_total{

		display:none;

		visibility:hidden;

	}

	.hide_if_not_closed{
		<?php if($res_data['status'] != 5 && check_if_admin() == false ){ ?>
			visibility: hidden;
		<?php }else{ ?>
			visibility: visible;
		<?php } ?>
	}

	
	.show_if_not_closed{
		<?php if($res_data['status'] == 5){ ?>
			display: none;
		<?php }else{ ?>
			display: block;
		<?php } ?>
		
	    padding: 20px;
	    text-align: center;
	    color: red;
	    font-size: 20px;

	}

	

	body .show{

		visibility:visible !important;

	}
	.modal-backdrop.fade.show {
	    display: none;
	}
	
	h4{    
		color: #343a40;
	    font-weight: 100;
	    font-size: 20px;
	}


</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Modifica Agreement 

         </div>

      </div>

   </div>

</div>

<div class="row">

<div class="col-md-12">

    <div class="mb-3 card">

        <div class="tabs-lg-alternate card-header">

            <ul class="nav nav-justified">

                <li class="nav-item">

                    <a data-toggle="tab" href="#tab-eg9-0" class="nav-link active show">

                        <div class="widget-number">Detalii Agreement</div>
                        <h4>Loc parcare <?php echo $res_data['parking_spot']; ?></h4>
                     

                        

                    </a></li>

            </ul>

        </div>

        <div class="tab-content">

            <div class="tab-pane active show" id="tab-eg9-0" role="tabpanel">

            	<div class="card-body">
					
					<?php 
						if($res_data['status'] == 1){
							$status = 'Confirmat';
						} else if($res_data['status'] == 2){
							$status = '<button class="mb-2 mr-2 btn btn-danger">Anulat</button>';
						} else if($res_data['status'] == 3){
							$status = 'No Show';
						} else if($res_data['status'] == 4){
							$status = '<button class="mb-2 mr-2 btn btn-success">Deschis</button>';
						}else if($res_data['status'] == 5){
							$status = '<button class="mb-2 mr-2 btn btn-secondary">Inchis</button>';
						}
						
						
					?>
					 <h4>Rezervarea nr <a href="/edit_reservation?id=<?php echo $res_data['res_id']; ?>"> <?php echo $res_data['res_id']; ?></a></h4> 

					<label>Status Agreement: <?php echo $status; ?></label>

					<div class="col-md-12">

						<?php if($res_data['status'] != 5 && $res_data['status'] != 2) {?>
							
							 <button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#closeAgr">Inchide</button>
							<button type="button" class="btn btn-warning btn-lg show_edit">Modifica rezervarea</button>
						<?php } ?>

						<?php if($res_data['status'] != 2) {?>
						<?php if(check_if_admin()){ ?>
							<button type="button" class="btn btn-danger btn-lg cancel_res" res-id="<?php echo $_GET['id']; ?>">Anuleaza</button>
						<?php } ?>
						<?php } ?>

						<?php if($res_data['status'] != 1) {?>
							<!--<button type="button" class="btn btn-success btn-lg change_status_btn" status="1">Confirma</button>-->
						<?php } ?>

						<?php if($res_data['status'] == 5 || $res_data['status'] == 2) {?>
							<?php if(check_if_admin()){ ?>
								<button type="button" class="btn btn-primary btn-lg" onclick="change_status(4, <?php echo $_GET['id']; ?>)">Redeschide Agreement-ul</button>
							<?php } ?>
						<?php } ?>
					</div>

					<!--<label>Schimba status Rezervare</label>

					<select name="status" class="form-control" id="change_status">

						<option value="4" <?php if($res_data['status'] == 4) { echo 'selected'; } ?>>Confirmat</option>

						<option value="2" <?php if($res_data['status'] == 2) { echo 'selected'; } ?>>Anulat</option>

						<option value="3" <?php if($res_data['status'] == 3) { echo 'selected'; } ?>>No Show</option>

						

					</select>-->

					

					<br />
					
					<div class="col-md-12">

					
							<button type="button" class="btn btn-lg btn-info" data-toggle="modal" data-target="#exampleModal11">Adauga Plata</button>
			                   
			                  <?php if(check_if_admin()){ ?>
							 <!-- <button class="btn-shadow btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">Adauga Costuri Extra</button>-->
								<?php } ?>

			                   <a id="hreff" href="/Main_controller/test_pdf/<?php echo $_GET['id']; ?>"><button type="button" id="genereaza_bon" class="btn btn-primary" >Genereaza Contract</button></a>

			                  <button type="button" id="trimite_bon" class="btn btn-primary" >Trimite contract pe email</button>

								
					</div>
					<br />
					
					<div class="col-md-12">
					
						<!-- <button type="button" class="btn btn-lg btn-info" id="recalc_totals">Recalculeaza Totalul si Platile</button> -->
							
						<button type="button" class="btn btn-lg btn-info" id="see_payments">Vezi platile</button>

						
					</div>
					
					<br />
					
					<div class="see_payments_div" style="display:none;">
					
						<table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

			                  <thead>

			                     <tr role="row">

			                        <th>Valoare</th>
									
			                        <th>Tip</th>
									
			                        <th>Status</th>

			                        <th>Adaugata de</th>
									
			                        <th>Adaugat la</th>

			                     </tr>

			                  </thead>

			                  <tbody>



			                  	<?php foreach($payments as $d){	?>

							

									<tr>

										

										<td><?php echo $d->amount; ?></td>

										<td><?php echo format_payment_type($d->type); ?></td>

			              				<td><?php echo ($d->status == 1) ? 'Activa' : '<span style="color:red">Anulata</span>'; ?></td>
										
			              				<td><?php echo get_user_by_id($d->created_by); ?></td>
										
			              				<td><?php echo $d->created_at; ?></td>

									</tr>



			                  	<?php } ?>

								



			                  </tbody>

			                  

			               </table>
					
					</div>

					
               </br>




					

					

						

				

                </div>

                <div class="card-body"><p class="mb-0">



                	<form id="reservation_form">

                		 <div class="position-relative form-group"><label for="">Tip rezervare*</label>

						 	<?php $options = get_res_types(); ?>

	                        <select name="tip" class="form-control">

	                        	<?php foreach ($options as $key => $value) {
	                        		if($res_data['tip'] == $value){
	                        			echo '<option value="'.$value.'" selected>'.$value.'</option>';
	                        		}else{
	                        			echo '<option value="'.$value.'">'.$value.'</option>';
	                        		}
	                        	}
	                        	?>
	                          

	                        </select>

	                     </div>


						<div class="form-row">

							<div class="col-md-4 hide_if_not_closed">
								<label>Total de plata</label>
								<input type="text" name="total" class="form-control" value="<?php echo $res_data['total']; ?>" disabled>
							</div>

							<div class="col-md-4 hide_if_not_closed">
								<label>Total Initial(fara discount)</label>
								<input type="text" name="initial_total" class="form-control" value="<?php echo $res_data['initial_total']; ?>" disabled>
							</div>

							<div class="col-md-4 hide_if_not_closed">
								<label>Platit pana acum</label>
								<input type="text" name="" class="form-control" value="<?php echo $res_data['paid']; ?>" disabled>
							</div>

							<div class="col-md-4 hide_if_not_closed">
								<label>Ramas de Plata</label>
								<input type="text" name="" class="form-control" id="total_left_to_pay" value="<?php echo $res_data['total'] - $res_data['paid']; ?>" disabled>
							</div>

							<div class="col-md-4 hide_if_not_closed">
								<label>Discount</label>
								<input type="text" name="discount" class="form-control" id="discount" value="<?php echo $res_data['discount']; ?>" disabled>
							</div>

							<div class="col-md-12 show_if_not_closed">

								Totalul de plata va fi afisat dupa inchiderea contractului.

							</div>

							<br>

                           <div class="col-md-6">

                              <div class="position-relative form-group"><label >Clientul ajunge in data de:</label>



                              	<input name="checkout_date" readonly placeholder="date placeholder" type="text" class="form-control calc"></div>

                           </div>

                           

                           <div class="col-md-6">

                              <div class="position-relative form-group"><label >Clientul pleaca in data de:</label>

                              	<input name="checkin_date" readonly placeholder="date placeholder" type="text" class="form-control calc"></div>

                           </div>

						

                        </div>

						<input type="text" hidden name="agreement_id" value="<?php echo $res_data['ID']; ?>"> 

						<input type="text" hidden name="reservation_id" value="<?php echo $res_data['res_id']; ?>"> 

						<input type="text" hidden name="customer_id" value="<?php echo $cus_data['ID']; ?>"> 

						<div class="position-relative form-group"><label for="">Lista preturi</label>

								<?php $price_list_id = get_res_price_list_id($res_data['res_id']); ?>

								<?php $price_list_id = ( $price_list_id == 0 ) ? $website_price_list_id : $price_list_id; ?>

								<select name="price_list" class="form-control" disabled="disabled" readonly>

									<?php foreach ($prices_list as $key => $value) {
										if($price_list_id == $value['id']){
											echo '<option value="'.$value['id'].'" selected>'.$value['name'].'</option>';
										}else{
											echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
										}
									}
									?>


								</select>

							</div>
                        <div class="position-relative form-group"><label for="">Nume si prenume</label><input name="name" value="<?php echo $cus_data['name']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Email</label><input name="email" value="<?php echo $cus_data['email']; ?>" id="" placeholder="" type="email" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Telefon</label><input name="phone" value="<?php echo $cus_data['phone']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <?php if(!empty($cars)){ ?>

		                     <div class="select_car">

		                     	<span>Vezi alte masini cu care clientul a mai venit</span>

		                     	<div class="cars_select" style="display: none;">

		                     		<?php foreach ($cars as $key => $car) { ?>
		                     			
		                     			<div class="car">

		                     				<span class="marca"><?= $car['marca']; ?></span>
		                     				<span class="model"><?= $car['model']; ?></span>
		                     				<span class="nr_inmatriculare"><?= $car['nr_inmatriculare']; ?></span>

		                     			</div>

		                     		<?php } ?>
		                     	</div>

		                     </div>

	                     <?php } ?>

	                     <div class="position-relative form-group"><label for="">Marca</label><input name="marca" value="<?php echo $cus_data['marca']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Model</label><input name="model" value="<?php echo $cus_data['model']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Nr. Inmatriculare</label><input name="nr_inmatriculare" value="<?php echo $cus_data['nr_inmatriculare']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Adresa</label><input name="adresa" value="<?php echo $cus_data['adresa']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <div class="position-relative form-group"><label for="">Observatii</label><input name="observatii" value="<?php echo $cus_data['observatii']; ?>" id="" placeholder="" type="text" class="form-control"></div>

	                     <!--<div class="position-relative form-check"><label class="form-check-label"><input type="checkbox" id="pers_jur" class="form-check-input"> Persoana juridica</label></div>
							-->

	                     <div id="pers_jur_details">
                        
	                        <div class="position-relative form-group"><label for="">Firma</label><input name="firma" id="firma" placeholder="" type="text" class="form-control" value="<?php echo $cus_data['firma']; ?>"></div>

	                        <div class="position-relative form-group"><label for="">CUI</label><input name="CUI" id="CUI" placeholder="" type="text" class="form-control" value="<?php echo $cus_data['CUI']; ?>"></div>

	                      </div>

	                     <!-- <div class="position-relative form-check"><label class="form-check-label"><input type="checkbox" <?php //echo ( $res_data['washing'] == 1 ) ? 'checked' : '' ; ?> name="washing" class="form-check-input"> Spalatorie</label></div> -->
<?php 
 foreach($services->result() as $service)
 {
	 $check = "";
	 if(in_array($service->ID,$service_option)){
		 $check = "checked";
	 }
echo '<div class="position-relative form-check"><label class="form-check-label"><input disabled="disabled"
type="checkbox" value="'.$service->ID.'" '.$check.' name="services[]" class="form-check-input services"> '.$service->name.' - '.$service->value.' Ron</label></div>';
 }
 ?>
	                      <?php
                                if($res_data['lock_payment'] == 1 && $res_data['locked_price']) { ?>

                                    Pret blocat per zi: <strong><?php echo $res_data['locked_price']; ?> RON </strong>

                            <?php    }
                            ?>

	                     <div class="position-relative form-check"><label class="form-check-label"><input disabled="disabled"  type="checkbox" <?php echo ( $res_data['lock_payment'] == 1 ) ? 'checked' : '' ; ?> name="lock_payment" class="form-check-input"> Blocheaza Totalul ( totalul se schimba doar la prelungire )</label></div> 

	                     <div class="position-relative form-group"><label for="">Calculeaza diferenta folosind lista de preturi:</label>


								<select name="new_price_list" class="form-control" disabled="disabled">

									<?php $parkavia = ( $res_data['tip'] == 'ParkVia' ) ? 'selected' : ''; ?>

									<?php if($price_list_id_temp != 0){ ?>
										<option value="0" >Lista actuala</option>
									<?php } ?>
									<option value="<?= $website_price_list_id; ?>" >Preturi Website</option>
									<option value="<?= $website_price_list_prepaid; ?>" >Preturi Website Prepaid</option>
									<option value="<?= $parkavia_price_list_id; ?>" <?= $parkavia; ?> >Preturi Parkavia</option>


								</select>

							</div>

						<div class="row">

	                      <div class="col-md-6 offset-md-3">
	                      	
							
							<table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

			                  <thead>

			                     <tr role="row">

			                      	<th>Serviciu Extra</th>

			                        <th>Pret</th>

			                        <th>Adaugat la</th>

			                     </tr>

			                  </thead>

			                  <tbody>



			                  	<?php foreach($extra_costs as $d){	?>

							

									<tr>

										

										<td><?php echo $d->serviciu; ?></td>

										<td><?php echo $d->pret; ?></td>

			              				<td><?php echo $d->created_at; ?></td>

									</tr>



			                  	<?php } ?>

								



			                  </tbody>

			                  

			               </table>


	                      </div>

	                      <div class="col-md-12 hide_if_not_closed">

	                            <div class="widget-chart widget-chart-hover result">

	                               

	                                <div class="widget-numbers total"><?php echo $res_data['total']; ?> RON</div>

	                                <div class="widget-subheading days_total">Pentru <?php echo $res_data['days']; ?> zile</div>

	                                <div class="widget-subheading washing_total <?php if( $res_data['washing'] == 1 ){ ?> show <?php } ?>">

	                                	

	                                		 Spalatorie: <span class="washing"><?php echo $options_data['value']; ?></span> RON

	                                		

                                		

	                                </div>

									<div class="widget-subheading">

										Parcare: <span class="subtotal"><?php echo $res_data['subtotal']; ?></span> RON

	                                </div>

	                            </div>

	                        </div>

	                        

	                  		

                        </div>

                       
						<br>

	                     <div class="text-center">

                           <button class="btn-shadow btn-wide btn btn-success btn-lg" id="update_reservation">Actualizeaza rezervarea</button>


                        </div>



                       </form>



                </p></div>

            </div>

           

        </div>

    </div>

</div>

</div>


<!-- Modal -->
<div id="closeAgr" class="modal fade" role="dialog" style="margin-top:60px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detalii finale</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        

		<label>Clientul Doreste Factura?</label>
		<select id="wants_invoice" class="form-control">
			
			<option value="no"> Nu </option>
			<option value="yes"> Da </option>

		</select>
		<br>

		<label>CUI Firma</label>
		<input type="text" name="" id="cui_modal" class="form-control" value="<?php echo $cus_data['CUI']; ?>">

		</br>
		</br>
		</br>

		<button type="button" class="btn btn-danger btn-lg change_status_btn" status="5">Inchide Contractul</button>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="margin-top:60px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Costs</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        

		<label>Serviciu</label>
		<input type="text" name="" id="extra_cost" class="form-control">

		<br>

		<label>Pret</label>
		<input type="text" name="" id="extra_cost_price" class="form-control">

		</br>
		</br>
		</br>

		<button type="button" id="add_extra_cost" class="btn btn-primary">Adauga Cost Extra</button>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:60px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adauga Plata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<label>Suma platita:</label>
			<input type="number" name="paid" class="form-control" id="paid_amount">
		</div>
		<div class="form-group">
			<select  class="form-control" id="payment_type">

						

				<option value="0">Cash</option>

				<option value="1">Card</option> 

				

				

			</select>
		</div>
		
		<div class="form-group">
		
			<label>S-a emis bon fiscal?</label>

			<select  class="form-control" id="bon_fiscal">
			
				<option value="0">Nu</option>
				<option value="1">Da</option>

			</select>
		
		</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="paid">Actualizeaza plata</button>
      </div>
    </div>
  </div>
</div>




<?php include('footer.php'); ?>

<script type="text/javascript">

	
function change_status(status, res_id){
		var data = { status: status, res_id: res_id };

		 $.post("/Main_controller/change_agreement_status", data, function(data, status){

	     	toastr.success('Rezervarea a fost modificata!', 'Success!');

			if(status == 3){

				setTimeout(function(){ window.location.href = "/edit_agreement?id="+data.res_id; }, 500);

			}

	      });
	}


$( document ).ready(function() {

	$('.select_car').click(function(){

		$('.cars_select').toggle()

	})

	$('.car').click(function(){

		var marca = $(this).find('.marca').text()
		var model = $(this).find('.model').text()
		var nr_inmatriculare = $(this).find('.nr_inmatriculare').text()

		$('[name="marca"]').val(marca)
		$('[name="model"]').val(model)
		$('[name="nr_inmatriculare"]').val(nr_inmatriculare)

	})

	var temp_tip = $('[name="tip"]').val();
	var temp_total = $('[name="total"]').val();

	console.log(temp_tip);
	console.log(temp_total);

	if( temp_tip == 'Standard' && temp_total == "0" ){

		//recalc_totals();

	}

	disable_all_inputs(true);

	$('#update_reservation').hide();

	$('.show_edit').click(function(){

		if(!request_confirm( $(this).text() )){
			return false;
		}

		enable_all_inputs();

		$('#update_reservation').show();

	});

	$(".change_status_btn").click(function(){

		var answer = window.confirm("Sunteti sigur ca vreti sa schimbati statusul?")
		if (answer) {
		    //some code
			var status = $(this).attr('status');
			var res_id = '<?php echo $_GET['id']; ?>';

			if(status !== '5'){
				change_status(status, res_id);
			} else {

				var wants_invoice = 0;

				if( $('#wants_invoice').val() == 'yes' ){

					wants_invoice = 1;
					
					if( $('#cui_modal').val().length < 8 ){

						alert('CUI-ul nu este completat sau nu are minimum 8 cifre!');
						return false;
					}

				}

				var total_left_to_pay = $("#total_left_to_pay").val();
				


					var data = { update_id: <?php echo $cus_data['ID']; ?>, update_table : 'customers', CUI: $('#cui_modal').val(), wants_invoice : wants_invoice};

					$.post("/Main_controller/update_data", data, function(data, status){

				     	if(data == 'false'){

				     		toastr.error('Nu s-a putut actualiza!', 'Eroare!');

				     	}else if( data == 'true' ){

							toastr.success('Actualizat!', 'Success!');

				     	}else{

				     		toastr.error('Cauza neidentificata', 'Eroare!');

				     	}

				    });

					change_status(status, res_id);



					alert('Nu uita sa incasezi clientul si sa adaugi plata.')

					setTimeout(function(){ 

						$('.show_edit').click()

						$('[name="checkin_date"]').val('<?= date('d/m/Y H:i'); ?>')
						$('[name="checkin_date"]').trigger('change')

						$('#update_reservation').click()

					 }, 500);

					
				
			}
		}else {
		    return;
		}


	});

	$("#add_extra_cost").click(function(){
		var serviciu = $("#extra_cost").val();
		var pret = $("#extra_cost_price").val();
		var agreement_id = '<?php echo $res_data['res_id']; ?>';

		var data = { agreement_id: agreement_id, serviciu: serviciu, pret: pret };

	    $.post("/Main_controller/add_extra_costs", data, function(data, status){

	      var data = JSON.parse(data);

	      if(data){
	      	alert('Extra cost adaugat cu succes!');
	      	window.location.reload();
	      }

	    });

	});

	$('.cancel_res').click(function(){

		var id = $(this).attr('res-id');

		var data = { id: id};

	    $.post("/Main_controller/cancel_contract", data, function(data, status){

	     
	      	window.location.reload();
	     

	    });
	});

	$("#trimite_bon").click(function(){

		var answer = window.confirm("Sunteti sigur ca vreti sa trimiteti bonul pe mail?")
		if (answer) {
		    //some code
			var agreement_id = '<?php echo $_GET['id']; ?>';

			var data = { agreement_id: agreement_id };

		    $.post("/Main_controller/trimite_bon", data, function(data, status){

		      //var data = JSON.parse(data);

		      alert('Mail trimis cu succes!');

		    });
		}
		else {
		    return;
		}

	});

	$('#pers_jur').click(function(){
	    $("#pers_jur_details").toggle();
	  });

	$("#genereaza_bon").click(function(e){

		
		var res_id = '<?php echo $_GET['id']; ?>';

		var data = { res_id: res_id };

		$.post("/Main_controller/genereaza_bon", data, function(data, status){

       		

	  	});
		
	

	});
	
	$("#recalc_totals").click(function(){
		var answer = window.confirm("Sunteti sigur ca vreti sa recalculati totalul?")
		if (answer) {

				recalc_totals();

		}
		else {
		    return;
		}

	});

	function recalc_totals(){
		
		    //some code
			var res_id = '<?php echo $res_data['res_id']; ?>';

			$.post("/Main_controller/recalculate_total/"+res_id+"/true", function(data, status){

	       		if(data == '1'){
					
					toastr.success('Totalul a fost recalculat!', 'Success!');

					setTimeout(function(){ location.reload(); }, 500);

						
				}else{
					
					toastr.success('A avut loc o eroare!', 'Danger!');
					
				}

			});
		
	}

	$('#see_payments').click(function(){
		
		$('.see_payments_div').toggle();
		
	});
	

	$('#paid').click(function(e){

		

		e.preventDefault();

		var answer = window.confirm("Sunteti sigur ca vreti sa adaugati aceasta plata?")
		if (answer) {
		    //some code
			var paid = $('#paid_amount').val();


			var payment_type = $('#payment_type').val();
			var bon_fiscal_emis = $('#bon_fiscal option:selected').val();

			var res_id = '<?php echo $res_data['res_id']; ?>';

			
			if(paid == ''){
				//change_status( $('#change_status option:selected').val(), res_id );

				var language = $('#mail_language option:selected').val();
				
				//send_mail(res_id, language);
				return;
			}

			var data = { amount: paid , type: payment_type, res_id: res_id, agreement: 'yes', bon_fiscal_emis: bon_fiscal_emis};

			

			 $.post("/Main_controller/add_payment", data, function(data, status){

	       

			 	var obj = JSON.stringify(data);
			 	var obj2 = JSON.parse(data);

			 	if(obj2 === false){
			 		toastr.error('Suma este mai mare decat suma rezervarii!', 'failed');
			 	} else {
					toastr.success('Rezervarea a fost modificata!', 'Success!');

			 	}

				setTimeout(function(){ window.location.reload(); }, 500);
				//change_status( $('#change_status option:selected').val(), res_id );

				var language = $('#mail_language option:selected').val();
				
				//send_mail(res_id, language);


			  });
		}
		else {
		    return;
		}

		
	});

	function send_mail(res_id, language){

		if(language == 0 || language == '0'){
			return;
		}

		var data = { res_id: res_id, language: language };

		 $.post("/Main_controller/send_reservation_mail", data, function(data, status){

	       

	     	toastr.success('Rezervarea a fost modificata!', 'Success!');

			if(status == 3){

				setTimeout(function(){ window.location.href = "/edit_agreement?id="+data.res_id; }, 500);

			}



	      });
	}

	



$.datetimepicker.setLocale('ro');

$('[name="checkout_date"]').datetimepicker({

			i18n:{

				ro:{

					months:[

						'Ianuare','Februarie','Martie','Aprilie',

						'Mai','Iunie','Iulie','August',

						'Septembrie','Octombrie','Noiembrie','Decembrie',

					],

					dayOfWeek:[

						"Du", "Lu", "Ma", "Mi",

						"Jo", "Vi", "Sa",

					]

				}

			},

			timepicker:true,

			format:'d/m/Y H:i',

			mask:'39/12/9999 29:59',
            onChangeDateTime:function(dp,$input){
             $('[name="checkin_date"]').datetimepicker({minDate: dp})
           }

		}).val('<?php echo date('d/m/Y H:i', strtotime($res_data['checkout_date'])); ?>');



$('[name="checkin_date"]').datetimepicker({

			i18n:{

				ro:{

					months:[

						'Ianuare','Februarie','Martie','Aprilie',

						'Mai','Iunie','Iulie','August',

						'Septembrie','Octombrie','Noiembrie','Decembrie',

					],

					dayOfWeek:[

						"Du", "Lu", "Ma", "Mi",

						"Jo", "Vi", "Sa",

					]

				}

			},

			timepicker:true,

			format:'d/m/Y H:i',

			mask:'39/12/9999 29:59'

		}).val('<?php echo date('d/m/Y H:i', strtotime($res_data['checkin_date'])); ?>');


$('[name="checkin_date"]').change(function(e){

			e.preventDefault();



			calc_price();

		});



		$('[name="checkout_date"]').change(function(e){

			e.preventDefault();



			calc_price();

		});



		// $('[name="washing"]').click(function(){





		// 	calc_price();

		// });
		$('.form-check-input').click(function(){
	
			calc_price();

		});
		$('[name="new_price_list"]').change(function(){





			calc_price();

		});



function calc_price(){

	var checkout_date = $('[name="checkout_date"]').val();

	var checkin_date = $('[name="checkin_date"]').val();

	//var washing = $('[name="washing"]').prop('checked');
	let washing = []
        $("input[name='services[]']:checked").each(function ()
        {
            washing.push(parseInt($(this).val()));
        });

	var res_id = $('[name="reservation_id"]').val();

			var new_price_list = $('[name="new_price_list"]').val();



			var data = { checkout : checkout_date, checkin : checkin_date, washing: washing, res_id: res_id, new_price_list: new_price_list,edit : "edit" };

     $.post("/Main_controller/new_compute_reservation_price", data, function(data, status){

       

     	var data = JSON.parse(data);



     	if(washing === true){

     		$('.washing_total').show();

     		$('.washing_total').addClass('show');

     	}else{

     		$('.washing_total').hide();

     		$('.washing_total').removeClass('show');

     	}



     	$('.total').text(data.total.toFixed(2)+' RON');

     	$('.subtotal').text(data.subtotal.toFixed(2)+'');



     	$('.days_total').text('Pentru '+data.days+' zile');



      });

}



$('#update_reservation').click(function(e){

	e.preventDefault();

	var answer = window.confirm("Actualizati contractul?")
	if (answer) {
	    //some code

	    if( $('[name="nr_inmatriculare"]').val().length < 7 ){
	    	alert('Numar de Inmatriculare incomplet! Te rugam sa completezi marca, model si nr Inmatriculare!')
	    	toastr.error('Numar de Inmatriculare incomplet!', 'Eroare!');
	    	return false
	    }

		var data = $('#reservation_form').serializeArray();
		data.push({name: 'is_agreement', value: 'true'});

		  $.post("/Main_controller/update_reservation", data, function(data, status){

	     	if(data == 'false'){

	     		toastr.error('Nu s-a putut actualiza!', 'Eroare!');

	     	}else if( data == 'true' ){

				toastr.success('Actualizat!', 'Success!');

	           setTimeout(function(){ window.location.reload(); }, 500);

	     	}else{

	     		toastr.error('Cauza neidentificata', 'Eroare!');

	     	}



	      });
	}
	else {
	    return;
	}




});





});



</script>