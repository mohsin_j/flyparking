<?php include('header.php'); ?>



<style type="text/css">

   #btn_calc_price{

   display: block;

   margin: 0 auto;

   }

</style>

<div class="app-page-title">

   <div class="page-title-wrapper">

      <div class="page-title-heading">

         <div class="page-title-icon">

            <i class="pe-7s-car icon-gradient bg-mean-fruit">

            </i>

         </div>

         <div>

            Agreement-uri

         </div>

      </div>

   </div>

</div>

<div class="main-card mb-3 card">

   <div class="card-body">

      <div class="row">

            <div class="col-md-12">

              <div class="row">
                  <div class="col-md-3">
                    <div class="form-group"> 
                      <label> Status </label>
                      <select class="form-control input-medium" onchange="filter(this.id)" id="status" table_col="7">
                        <option value="">All</option>
                        <option>Deschis</option>
                        <option>Inchis</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-3">
                    <button type="button" class="btn btn-lg btn-info" onclick="window.location.href='/add_reservation';" style="margin-top:29px;">Adauga Rezervare</button>
                  </div>
              </div>

               <table style="width: 100%;" id="datatabletable" class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                  <thead>

                     <tr role="row">

                      	<th>ID</th>

                      	<th>Nume</th>

                      	<th>Data sosire</th>

                      	<th class="activate_sorting">Data plecare</th>

                      	<th>Telefon</th>

                      	<th>Auto</th>

                      	<th>Spalatorie</th>

                       
                        <th>Status</th>

                         <?php if( check_if_admin() ){ ?>
                          <th>Total</th>
                        <?php } ?>

                        <th>Creat De</th>

                        <th>Creat La</th>

                        <th>Tip</th>


                     </tr>

                  </thead>

                  <tbody>



                  	<?php foreach($data as $d){	?>

				

						<tr>

							

							<td><a href="/edit_agreement?id=<?php echo $d['ID']; ?>"><?php echo $d['ID']; ?></a></td>

							<td><?php echo $d['name']; ?></td>

							<td><?php echo date('d.m.Y H:i', strtotime($d['checkout_date'])); ?></td>

							<td><?php echo date('d.m.Y H:i', strtotime($d['checkin_date'])); ?></td>

							<td><?php echo $d['phone']; ?></td>

							<td><?php echo $d['marca']; ?> <?php echo $d['model']; ?> <?php echo $d['nr_inmatriculare']; ?></td>

							<td><?php if($d['washing'] == 0){ echo'No'; }  else echo 'Yes';?></td>
             
              <td><?php if($d['status'] == 4) { echo 'Deschis'; } else if($d['status'] == 5){ echo 'Inchis'; }    ?></td>

               <?php if( check_if_admin() ){ ?>
                <td><?php echo $d['total']; ?></td>
              <?php } ?>

              <td><?php echo $d['username']; ?></td>

              <td><?php echo date('d.m.Y H:i', strtotime($d['created_at']) ); ?></td>

              <td><?php echo $d['tip']; ?></td>

						</tr>



                  	<?php } ?>

					



                  </tbody>

                  

               </table>

            </div>

         </div>

   </div>

</div>

<?php include('footer.php'); ?>

<script type="text/javascript">

   var table = $('#datatabletable').DataTable({

       <?php if($access_level == 1) {?>
          dom: 'Blfrtip',
          buttons: [
            'excelHtml5'
          ],

        aLengthMenu: [
          [25, 50, 100, 200, -1],
          [25, 50, 100, 200, "All"]
        ],

        <?php }?>

         columnDefs: [

           { type: 'de_datetime', targets: 2 },

           { type: 'de_datetime', targets: 3 }

         ],
		 
		 

      });

      function filter(id){
        table
          .columns($('#'+id).attr('table_col'))
          .search($('#'+id).val())
          .draw();
          
          
        //table.ajax.reload();
      }

$(window).on("load", function () {
   $("#status option").each(function(){
    console.log($(this).text());
      if($(this).text() == 'Deschis'){
        $(this).prop('selected', true);
      }
   });

   filter('status');
   
   $('.activate_sorting').click();
});  

</script>