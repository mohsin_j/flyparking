<?php include('header.php'); ?>


<div class="app-page-title">

    <div class="page-title-wrapper">

        <div class="page-title-heading">

            <div class="page-title-icon">

                <i class="pe-7s-car icon-gradient bg-mean-fruit">

                </i>

            </div>

            <div>

                Casierie

            </div>

        </div>

    </div>

</div>


<div class="main-card mb-3 card">

    <div class="card-body">

        <div class="row">

            <div class="col-md-12">

                <table style="width: 100%;" id="datatabletable"
                       class="mb-0 table table-hover table-striped table-bordered dataTable dtr-inline">

                    <thead>

                    <tr role="row">

                        <th>
                            <button type="button" onclick="toggleCheck()" class="btn btn-primary btn-xs">Select</button>
                        </th>

                        <th>Agreement</th>

                        <th>Client</th>

                        <th>Data Creare</th>

                        <th>Suma</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php $total_amount = 0; ?>

                    <?php foreach ($payments as $key => $p) { ?>


                        <tr>

                            <td>
                                <div class="checkbox"><input type="checkbox" value="" class="select_payment"
                                                             payment-id="<?php echo $p->payment_id; ?>"
                                                             amount="<?php echo $p->amount; ?>"></div>
                            </td>

                            <td><a href="/edit_agreement?id=<?php echo $p->ID; ?>">Vezi Agreement</a></td>

                            <td><?php echo $p->name; ?></td>

                            <td><?php echo $p->created_at; ?></td>

                            <td><?php echo $p->amount; ?></td>

                        </tr>

                        <?php $total_amount += $p->amount; ?>

                    <?php } ?>


                    </tbody>


                </table>

            </div>

            <div class="col-md-12">

                <h3 class="text-right">Total: <span id="total_amountt">0</span></h3>

                <button type="button" class="btn btn-secondary" style="float:left;"
                        onclick="window.location.href='/historic_casierie'">Vezi Historic plati Cash
                </button>

                <button type="button" class="btn btn-secondary" style="float:right;" id="muta_in_banca">Muta in Banca
                </button>

            </div>

        </div>

    </div>

</div>


<?php include('footer.php'); ?>

<script type="text/javascript">

    function toggleCheck() {
        var isChecked = $('.select_payment').first().prop('checked');

        $('.select_payment').each(function(){
           $(this).prop('checked', !isChecked).trigger('change');
        });

    }


    $(document).on('change', '.select_payment', function () {

        var total = 0;

        $('.select_payment').each(function () {

            if ($(this).is(':checked')) {
                var current_total = parseFloat($(this).attr('amount'));

                total += current_total;


                $('#total_amountt').text(total);


            } else {
                $('#total_amountt').text('0');
            }

        });

    });

    $("#muta_in_banca").click(function () {

        var selected = [];

        $('.select_payment').each(function () {

            if ($(this).is(':checked')) {
                selected.push($(this).attr('payment-id'));
            }

        });

        var data = {selected: selected};

        $.post("/Main_controller/move_to_bank", data, function (data, status) {


            window.location.reload();


        });
    });


</script>