<nav class="navbar navbar-expand-lg">



<!-- Links -->

<ul class="navbar-nav">

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/dashboard') ?>">Pagina principala</a>

  </li>

  <!-- <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/planning') ?>"> Planning</a>

  </li> -->

  <li class="nav-item">

    <a class="nav-link" href=" <?= base_url('/reservations') ?>">Rezervari</a>

  </li>



  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/new_agreements') ?>">Contracte</a>

  </li>

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/spalatorie') ?>">Spalatorie</a>

  </li>

  <?php if(check_if_admin()){ ?>

    <li class="nav-item dropdown">

      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">

      Abonamente

      </a>

      <div class="dropdown-menu">

        <a class="dropdown-item" href="<?= base_url('/add_membership') ?>">Abonamente</a>

        <a class="dropdown-item" href="<?= base_url('/customers') ?>">Clienti</a>

        <a class="dropdown-item" href="<?= base_url('/Main_controller/customers_cars') ?>">Masini Clienti</a>

      </div>

    </li>

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/users') ?>">Utilizatori</a>

  </li>

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/unpaid_contracts') ?>">Contracte neplatite</a>

  </li>

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/casierie') ?>">Casierie</a>

  </li>

  <li class="nav-item dropdown">

      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">

      Setari Bon

      </a>

      <div class="dropdown-menu">

        <a class="dropdown-item" href="<?= base_url('/conditii_bon') ?>">Conditii</a>

        <a class="dropdown-item" href="<?= base_url('/nota_bon') ?>">Nota</a>

      </div>

    </li>

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/mail_templates') ?>">Template Emailuri</a>

  </li>

  <!-- <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/create_statistics') ?>">Statistici</a>

  </li> -->

  <li class="nav-item">

    <a class="nav-link" href="<?= base_url('/main_controller/price_lists') ?>">Preturi</a>

  </li>

  <?php } ?>

</ul>



</nav>
