<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main_controller extends CI_Controller {

	private $auth;





	public function __construct() {

		parent::__construct();

		//load authclass

		$this->load->database();

		$db = new \PDO('mysql:dbname=' . $this->db->database . ';host=localhost;charset=utf8mb4', $this->db->username, $this->db->password);

		$this->auth = new \Delight\Auth\Auth($db);

		$this->load->helper('url');



		$data = $this->input->post('key');

		if ($data !== 'parking' || empty($data)) {

			//check if user is logged in

			if (!$this->auth->isLoggedIn()) {

				$segs = $this->uri->segment_array();

				if (!isset($segs[1])) {

					redirect('login', 'refresh');

					exit();

				}

				if ($segs[1] !== 'login') {

					redirect('login', 'refresh');

				}

			}

		}

	}

	public function login() {

		$this->load->view('login');

	}

	public function logout() {

		$this->auth->logOut();

		redirect('/login', 'refresh');

	}

	public function index() {

		//$this->dashboard();

		redirect('/reservations', 'refresh');

	}

	public function planning() {

		$data['total_parking_spots'] = $this->db->where('name', 'total_parking_spots')->get('options')->row()->value;

		$this->load->view('planning', $data);

	}

	public function dashboard() {

		$this->load->model('Reservation_model');



		$data['next_days_reservations'] = $this->Reservation_model->getNextDaysReservations(7);

		$data['next_days_exits'] = $this->Reservation_model->getNextDaysExits(7);



		$this->db->select('*, r.ID as ID');

		$this->db->join('customers c', 'c.ID = r.customer_id', 'left');

		$data['latest_reservations'] = $this->db->order_by('r.ID', 'desc')->limit(25)->get('reservations r')->result_array();

		$data['memberships'] = $this->db->select('*, m.ID as ID')->join('customers c', 'c.ID = m.customer_id', 'left')->where('end <=', date('Y-m-d', strtotime('+3 days')).'23:59'  )->where('renewed',0)->get('memberships m')->result_array();

		$this->load->view('dashboard', $data);

	}

	public function add_new_condition() {

		$conditie = $this->input->post('conditie');

		$data = array('text' => $conditie);

		$this->db->insert('conditii_bon', $data);

		if (is_int($this->db->insert_id())) {

			echo json_encode(true);

		}

	}

	public function delete_condition() {

		$condition_id = $this->input->post('condition_id');

		$this->db->where('ID', $condition_id);

		$this->db->delete('conditii_bon');

		if ($this->db->affected_rows() > 0) {

			echo json_encode(true);

		}

	}

	public function update_condition() {

		$condition_id = $this->input->post('condition_id');

		$conditie = $this->input->post('conditie');

		$data = array('text' => $conditie);

		$this->db->where('ID', $condition_id);

		$this->db->update('conditii_bon', $data);

		if ($this->db->affected_rows() > 0) {

			echo json_encode(true);

		}

	}

	public function update_nota() {

		$nota = $this->input->post('nota');

		$data = array('nota' => $nota);

		$this->db->where('ID', 1);

		$this->db->update('nota_bon', $data);

		if ($this->db->affected_rows() > 0) {

			echo json_encode(true);

		}

	}

	public function conditii_bon() {

		$data['conditii'] = $this->db->get('conditii_bon')->result();

		$this->load->view('conditii_bon', $data);

	}

	public function nota_bon() {

		$data['nota'] = $this->db->where('ID', 1)->get('nota_bon')->row();

		$this->load->view('nota_bon', $data);

	}

	public function trimite_bon() {

		$agr_id = $this->input->post('agreement_id');

		$data['res_data'] = $this->db->where('ID', $agr_id)->get('agreements')->result_array() [0];

		$data['cus_data'] = $this->db->where('ID', $data['res_data']['customer_id'])->get('customers')->result_array() [0];

		$data['price_data'] = $this->db->where('reservation_id', $data['res_data']['res_id'])->get('prices_log')->result_array() [0];

		$data['options_data'] = $this->db->where('reservation_id', $data['res_data']['res_id'])->get('options_log')->result_array() [0];

		$data['prices'] = $this->db->get('prices')->result_array();

		$data['conditii'] = $this->db->get('conditii_bon')->result_array();

		$data['nota'] = $this->db->where('ID', 1)->get('nota_bon')->row()->nota;

		$html = $this->load->view('bon', $data, TRUE);

		$dompdf = new Dompdf\Dompdf();

		$dompdf->load_html($html);

		$dompdf->set_paper("letter", "portrait");

		$dompdf->render();

		$output = $dompdf->output();

		$name = 'Bon' . time() . '.pdf';

		file_put_contents('uploads/' . $name, $output);

		$settings = $this->db->where('email_status',1)->get('settings');

					if($settings->num_rows() > 0)

					{

		$this->load->library('email');

		$this->email->to($data['cus_data']['email']);

		$this->email->from('reservations@flyparking.ro');

		$this->email->subject('Bonul Dvs Flying Parking');

		$this->email->message('Bonul dumneavoastra este atasat.');

		$this->email->attach('uploads/' . $name);

		@$this->email->send();

		}

		unlink('uploads/' . $name);

	}

	public function test_pdf($agr_id) {

		$dompdf = new Dompdf\Dompdf();

		$data = array();

		$data['res_data'] = $this->db->where('ID', $agr_id)->get('agreements')->result_array() [0];

		$data['cus_data'] = $this->db->where('ID', $data['res_data']['customer_id'])->get('customers')->result_array() [0];

		$data['price_data'] = $this->db->where('reservation_id', $data['res_data']['res_id'])->get('prices_log')->result_array() [0];

		$data['options_data'] = $this->db->where('reservation_id', $data['res_data']['res_id'])->get('options_log')->result_array() [0];

		$data['prices'] = $this->db->get('prices')->result_array();

		$data['conditii'] = $this->db->get('conditii_bon')->result_array();

		$data['nota'] = $this->db->where('ID', 1)->get('nota_bon')->row()->nota;

		$html = $this->load->view('bon', $data, TRUE);

		$dompdf->loadHtml($html);

		$dompdf->render();

		$dompdf->stream("bon.pdf", array("Attachment" => 0));

	}

	public function add_reservation() {

		$this->load->model('prices_model');

		$data['prices_list'] = $this->prices_model->get_price_lists(true);

		$data['services'] = $this->db->where('status',1)->get('options');

		$this->load->view('add_reservation',$data);

	}



	public function find_client(){



		$this->load->model('clients_model');



		$data = $this->clients_model->find_client( $this->input->post('q') );



		echo json_encode($data);



	}



	public function customers() {



		$data['page_title'] = 'Management Clienti';

		$this->load->helper('xcrud_helper');

		$xcrud = xcrud_get_instance();

		$xcrud->table('customers');

		$xcrud->fields('invoice_data, created_at', true);

		$xcrud->unset_remove();

		$data['table'] = $xcrud->render();

		$this->load->view('xcrud', $data);



	}



	public function customers_cars() {



		$data['page_title'] = 'Management Masini Clienti';

		$this->load->helper('xcrud_helper');

		$xcrud = xcrud_get_instance();

		$xcrud->table('customers_cars');

		$xcrud->join('customer_email','customers','email');

		$xcrud->relation('customer_email','customers','email','email');

		$xcrud->join('created_by','users','id');

		$xcrud->columns('customers.name, customer_email, marca, model, nr_inmatriculare, users.email, created_at', false);

		$xcrud->fields('customer_email, customers_cars.marca, customers_cars.model, customers_cars.nr_inmatriculare', false);

		$xcrud->column_name('users.email', 'Created by');

		$xcrud->unset_remove();

		$xcrud->unset_edit();

		$xcrud->unset_add();

		$data['table'] = $xcrud->render();

		$this->load->view('xcrud', $data);



	}



	public function renew_membership( $m_id ){



		$this->db->where('ID', $m_id);

		$m_data = $this->db->get('memberships')->row_array();



		if($m_data['renewed'] == 0){



			switch ($m_data['type']) {

				case 'monthly':

					$days = 30;

					break;

				case 'weekly':

					$days = 7;

					break;

				case 'yearly':

					$days = 365;

					break;

			}

			$m_data['start'] = $m_data['end'];

			$m_data['end'] = date('Y-m-d H:i:s', strtotime( $m_data['end'].' +'.$days.' days' ));

			$m_data['renewed'] == 0;



			$m_data['created_by'] = $_SESSION['auth_user_id'];



			unset($m_data['ID']);



			$this->db->where('ID', $m_id)->update('memberships', array('renewed' => 1));

		}



		$this->db->insert('memberships', $m_data);



		echo ($this->db->affected_rows() != 1) ? false : true;



	}



	public function add_membership() {

		//$data['customers'] = $this->db->select('*')->get('customers')->result();

		$data['page_title'] = 'Management Abonamente';

		$this->load->helper('xcrud_helper');

		$xcrud = xcrud_get_instance();

		$xcrud->table('memberships');

		$xcrud->relation('customer_id', 'customers', 'ID', array('name','email','phone'), null, null, null, ' - ');

		$xcrud->relation('created_by', 'users', 'ID', 'email');

		$xcrud->fields('created_by, created_at', true);

		$xcrud->change_type('type', 'select', '', 'monthly,weekly,yearly');

		$xcrud->change_type('active', 'select', '', array(1 => 'Activ', 0 => 'Inactiv'));

		//$customerdetails = $xcrud->nested_table('customers', 'customer_id', 'customers', 'ID');

		// $customerdetails->columns('invoice_data', true);

		$xcrud->pass_var('created_by', $_SESSION['auth_user_id'], 'create');

		$xcrud->unset_remove();

		$xcrud->duplicate_button();

		$data['table'] = $xcrud->render();

		$this->load->view('xcrud', $data);

	}



	public function create_statistics(){



		$data = $this->db->select('ID,checkout_date,checkin_date,total')->where('checkin_date >=', '2019-06-30')->get('reservations')->result_array();



		$count_res_by_month = array();

		$count_future_res_payment_by_month = array();



		foreach($data as $key => $d){



			$year = date('Y', strtotime($d['checkout_date']));

			$month = date('m', strtotime($d['checkout_date']));



			if( empty($count_res_by_month[ $year ][ $month ]) ){

				$count_res_by_month[ $year ][ $month ] = 0;

				$count_future_res_payment_by_month[ $year ][ $month ] = 0;

			}



			$count_res_by_month[ $year ][ $month ] += 1;

			$count_future_res_payment_by_month[ $year ][ $month ] += $d['total'];



		}



		$data = $this->db->select('ID,checkout_date,checkin_date,total,paid')->where('checkin_date >=', '2019-06-30')->get('agreements')->result_array();



		$count_agr_by_month = array();

		$count_agr_paid_by_month = array();



		foreach($data as $key => $d){



			$year = date('Y', strtotime($d['checkin_date']));

			$month = date('m', strtotime($d['checkin_date']));



			if( empty($count_agr_by_month[ $year ][ $month ]) ){

				$count_agr_by_month[ $year ][ $month ] = 0;

				$count_agr_paid_by_month[ $year ][ $month ] = 0;

			}



			$count_agr_by_month[ $year ][ $month ] += $d['total'];

			$count_agr_paid_by_month[ $year ][ $month ] += $d['paid'];



		}



		//print_r($count_agr_by_month);

		//print_r($count_agr_paid_by_month);

		//print_r($count_future_res_payment_by_month);



		$data['count_res_by_month'] = $count_res_by_month;

		$data['count_agr_by_month'] = $count_agr_by_month;

		$data['count_agr_paid_by_month'] = $count_agr_paid_by_month;

		$data['count_future_res_payment_by_month'] = $count_future_res_payment_by_month;



		$this->load->view('statistics', $data);



	}



	public function add_membership_reservation() {

		$today = date('Y-m-d H:i:s');

		$data['memberships'] = $this->db->select('*, c.ID as customer_id, c.name as customer_name')->where('active', 1)->where('start <=', $today)->where('end >=', $today)->join('customers c', 'c.ID = m.customer_id')->get('memberships m')->result_array();

		$this->load->view('add_membership_reservation', $data);

	}

	public function recalculate_total($res_id, $is_agreement) {

		//Check if reservation or agreement

		$table = 'reservations';

		$col = 'ID';

		if ($is_agreement == true || $is_agreement == 'true') {

			$table = 'agreements';

			$col = 'res_id';

		}

		//get res or agreement data

		$res_data = $this->db->where($col, $res_id)->get($table)->result_array();

		if (!empty($res_data)) {

			$res_data = $res_data[0];

		} else {

			echo false;

			exit();

		}

		//prepare washing for compute reservation price

		$washing = ($res_data['washing'] == 1) ? "true" : "false";

		$total = $this->compute_reservation_price($res_data['checkout_date'], $res_data['checkin_date'], $washing, $res_id);

		$subtotal = $total['subtotal'];

		$total = $total['total'];

		$extras = $this->db->select('*')->where('res_id', $res_id)->where('status', 1)->get('costuri_extra')->result_array();

		//add extras to total

		foreach ($extras as $extra) {

			$total+= $extra['pret'];

		}

		//recalculate payments

		$payments = $this->db->select('*')->where('res_id', $res_id)->where('status', 1)->get('payments')->result_array();

		//initialize paid

		$paid = 0;

		foreach ($payments as $payment) {

			$paid+= $payment['amount'];

		}

		//format update data

		$update_data = array('total' => $total, 'initial_total' => $total, 'discount' => 0,  'subtotal' => $subtotal, 'paid' => $paid,);

		//Update data

		$this->db->trans_start();

		$this->db->where($col, $res_id)->update($table, $update_data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {

			//if update error

			echo false;

			exit();

		}

		//update succesful

		echo true;

	}





	public function save_price_options(){



		$data = $this->input->post();



		$this->db->truncate('prices_options');



		foreach ($data as $name => $value) {

			

			$this->db->insert('prices_options', array( 'name' => $name, 'value' => $value ));



		}



		redirect('main_controller/price_lists');



	}



	public function calc_online_price(){

		$this->load->model('prices_model');



		if (!empty($this->input->post('checkout'))) {

			$checkout = $this->input->post('checkout');						

		}else{

			api_response(array('success' => false, 'message' => 'Please select checkout date.'));

		}



		if (!empty($this->input->post('checkin'))) {

			$checkin = $this->input->post('checkin');						

		}else{

			api_response(array('success' => false, 'message' => 'Please select checkin date.'));

		}



		

		$website_price_list_id = $this->prices_model->get_option('website_price_list');

		$website_price_list_prepaid_id = $this->prices_model->get_option('website_price_list_prepaid');



		$website_price_list_data = $this->calc_price_per_pricelist($website_price_list_id, $checkin, $checkout);

		$website_price_list_prepaid_data = $this->calc_price_per_pricelist($website_price_list_prepaid_id, $checkin, $checkout);

		// //getting option from db

		$options = $this->db->where('status',1)->get('options');

		//assign value to array

		$option_arr = [];

		if($options->num_rows() > 0){

			foreach($options->result() as $o)

			{

				$option_arr[] = [

					"ID" => $o->ID,

					"name" => $o->name,

					"price" => $o->value

				];

			}

		}

		

		

		api_response(array( 'prices' => [ 0 => $website_price_list_data, 1 => $website_price_list_prepaid_data ], "options" => $option_arr));	



	}



	private function calc_price_per_pricelist($pricelist_id, $checkin, $checkout, $start_day = null, $end_day = null, $res_id = null){



		$date1 = $this->dmy_to_ymd($checkout);

		$date1 = new DateTime(date('Y-m-d H:i:s', strtotime($date1)));

		$date2 = $this->dmy_to_ymd($checkin);

		$date2 = new DateTime(date('Y-m-d H:i:s', strtotime($date2)));

		$date2 = $date2->modify("-1 second");

		

		$diff = $date1->diff($date2)->days + 1;



		$pricelist_data = $this->db->where('id', $pricelist_id)->get('prices_list')->row_array();



		$pricelist_data = $this->db->where('name',$pricelist_data['name'])->where('active',1)->where('start_date <=', $date1->format('Y-m-d H:i:s'))->where('end_date >=', $date1->format('Y-m-d H:i:s'))->order_by('start_date','desc')->get('prices_list')->row_array();





		if(isset($pricelist_data)){

			$pricelist_id = $pricelist_data['id'];

		}

		





		$prices = $this->db->order_by('days','asc')->where('list_id',$pricelist_id)->get('prices')->result_array();



		if(!empty($res_id)){



			$prices = $this->db->where('reservation_id', $res_id)->where('list_id',$pricelist_id)->order_by('days','asc')->get('prices_log')->result_array();



		}

		

		if(empty($prices)){

		    

		       $prices = $this->db->order_by('days','asc')->where('list_id',$pricelist_id)->get('prices')->result_array();

		    

		}

		

		if(!empty($start_day)){

		    

		       $diff = $end_day - $start_day;

		       

		       $diff++;

		    

		}

		



		$total = 0;

		$days_left_to_add = $diff;

		$continous_price = false;

		foreach ($prices as $key => $p) {

		    

		    

			if(strpos($p['price'], '+') !== false){

			    

				$continous_price = (int) str_replace('+','',$p['price']);

				break;

			}



			if(($p['days'] < $start_day) && !empty($start_day)){

               

				continue;

			}	



			if(($p['days'] > $end_day) && !empty($end_day)){

		       

				continue;

			}			

			



			$total += $p['price'];



			$days_left_to_add--;

			

			//We have finished adding all the days. Lets take a break

			if($days_left_to_add == 0){

				break;

			}



		}

		

		



		if($continous_price !== false){



			for ($i=0; $i < $days_left_to_add; $i++) { 

				$total += $continous_price;

			}



		}



        

        



		return array('days' => $diff, 'total' => $total);



	}





	public function compute_reservation_price($checkout = null, $checkin = null, $washing = null, $res_id = null, $price_list = null, $edit = null) {

		

		$this->load->model('prices_model');

		$this->load->model('reservation_model');



		



		if (!empty($this->input->post('checkout'))) {

			$checkout = $this->input->post('checkout');

		}

		if (!empty($this->input->post('checkin'))) {

			$checkin = $this->input->post('checkin');

		}

		if (!empty($this->input->post('washing')) && empty($washing)) {

			$washing = $this->input->post('washing');

		}

		if (!empty($this->input->post('res_id')) && empty($res_id)) {

			$res_id = $this->input->post('res_id');

			$prices = $this->db->where('reservation_id', $res_id)->get('prices_log')->result_array();

			$price_list_id = $prices[0]['list_id'];

		}

	

		if($res_id != null){

			$edit = "edit";

			$reservation_data = $this->reservation_model->get_agreement_or_reservation($res_id);

		}

		



if(!empty($reservation_data)){

		if(empty($reservation_data['initial_days'])){



	

			if($reservation_data['total'] == $reservation_data['initial_total']){

				//is agreement

				if( isset($reservation_data['res_id']) ){



					$this->db->where('ID', $reservation_data['ID'])->update('agreements', ['initial_days' => $reservation_data['days']]);



				}else{



					$this->db->where('ID', $reservation_data['ID'])->update('reservations', ['initial_days' => $reservation_data['days']]);



				}



				$reservation_data['initial_days'] = $reservation_data['days'];



			}



		}

}





		if (!empty($this->input->post('new_price_list'))) {

			$new_price_list = $this->input->post('new_price_list');

		}





		if (!empty($this->input->post('price_list'))) {

			$price_list_id = $this->input->post('price_list');

		}



		$website_price_list_id = $this->prices_model->get_option('website_price_list');

		$parkvia_price_list_id = $this->prices_model->get_option('parkvia_reschedule_pricing');



		if(!isset($price_list_id)){



			$price_list_id = 0;



		}



		//This is in case there is no existing pricelistid for old reservations before pricelists existed

		if($price_list_id == 0){



			//$price_list_id = $website_price_list_id;

			if($res_id != null){

				if($reservation_data['tip'] == 'ParkVia'){

					$price_list_id = $parkvia_price_list_id;

				}

			}



		}



		if(!isset($new_price_list) || $new_price_list == 0){

			$new_price_list = $price_list_id;

		}



		if(isset($reservation_data['tip']) && $reservation_data['tip'] == 'ParkVia'){

				$new_price_list = $parkvia_price_list_id;

		}



		$scenario = 0;

		

		$pre = [];



		$new_checkout = dmy_to_ymd($checkout);

		$new_checkin = dmy_to_ymd($checkin);





        if(!empty($res_id)){

            $res_checkout_date = $reservation_data['checkout_date'];

	    	$res_checkin_date = $reservation_data['checkin_date'];

	    	

    		$days_in_old_reservation = daysInDateRange($res_checkout_date, $res_checkin_date);

			$days_in_new_reservation = daysInDateRange($res_checkout_date, $new_checkin);

			$days_in_diff_reservation = daysInDateRange($res_checkin_date, $new_checkin);

	    	

        }



		



	



		 //pre($days_in_old_reservation);

		// pre($days_in_new_reservation);

		//pre($days_in_diff_reservation);

		

		if(!empty($reservation_data['discount'])){

		    

		       if(strpos($reservation_data['discount'],'%') !== false){

		           

		           $discount = str_replace('%','',$reservation_data['discount']);

		           

		           $discount = 100 - $discount;

		           

		           $discount = $discount / 100;

		           

		           $reservation_data['total'] = $reservation_data['total'] / $discount;

		           

		       }

		    

		}

		

		if(!empty($res_id)){

		    

		        if( $reservation_data['created_at'] < '2022-01-18' && $reservation_data['tip'] != 'ParkVia'){



			$price_data = $this->calc_price_per_pricelist($price_list_id, $checkin, $checkout);



		}else if(isset($reservation_data) && $reservation_data['lock_payment'] != 1 ){



				$scenario = 1;



				$price_data = $this->calc_price_per_pricelist(7, $new_checkin, $new_checkout);				



		}else{



			if($days_in_new_reservation > $reservation_data['days']){



				//add to current price



				$scenario = 2;





                if( $reservation_data['initial_total'] == 0 ){

                    $price_data = $this->calc_price_per_pricelist($new_price_list, $new_checkin, $res_checkout_date, null,null, $res_id);

                }else{

                    $price_data = $this->calc_price_per_pricelist($new_price_list, $new_checkin, $res_checkout_date, $reservation_data['days'] + 1, $days_in_new_reservation, $res_id);

                }





				



				$pre['price_data'] = $price_data;

				$pre['startday'] = $reservation_data['days'] + 1;

				$pre['endday'] = $days_in_new_reservation;

				$pre['new_price_list'] = $new_price_list;

				$pre['total_old'] = $reservation_data['total'];



				$price_data['total'] = $reservation_data['total'] + $price_data['total'];

				

				$price_data['days'] = $reservation_data['days'] + $price_data['days'];



			}else{





				if(($days_in_new_reservation <= $reservation_data['initial_days']) && ($reservation_data['initial_days'] != 0)){



					//limit price reduction



					$scenario = 3;



					$price_data['total'] = (int) $reservation_data['initial_total'];

					$price_data['days'] = (int) $reservation_data['initial_days'];

					

					if($reservation_data['initial_total'] == 0){

					    $price_data = $this->calc_price_per_pricelist($new_price_list, $new_checkin, $res_checkout_date, null,null, $res_id);

					}



				}else{



					$scenario = 4;



					$price_data = $this->calc_price_per_pricelist($new_price_list, $new_checkin, $res_checkout_date, $reservation_data['initial_days'] + 1, $days_in_new_reservation, $res_id);

					$pre['price_data'] = $price_data;

					

					$pre['start'] = $reservation_data['initial_days'] + 1;

					

					$pre['end'] = $days_in_new_reservation;



					$price_data['total'] = $reservation_data['initial_total'] + $price_data['total'];



				}



			}



		}



	

		    

		}else{

		    

		       $price_data = $this->calc_price_per_pricelist($new_price_list, $checkin, $checkout);

		    

		}



		





			$res['total'] = $price_data['total'];

			$res['subtotal'] = (!isset($price_data['subtotal'])) ? $price_data['total'] : $price_data['subtotal'];

			$res['days'] = $price_data['days'];



			if ($washing == 'true') {

				$washing = $this->db->where('name', 'washing')->get('options')->row();

				$washing = $washing->value;

				if (!empty($res_id)) {

					$washing = $this->db->where('name', 'washing')->where('reservation_id', $res_id)->get('options_log')->row();

					$washing = $washing->value;

				}

				$res['total'] = $res['subtotal'] + $washing;

			}



 			// if(!empty($this->input->post('edit')) || $edit != null)

			 // {

				// $currentservice = $this->db->select_sum('price')->from('reservation_options')->where('res_id',$res_id)->get()->row();

				// $currentservice = $currentservice->price;

				// $res['total'] = $res['total'] - $currentservice;

				

			 // }



			$service_total = 0;

			if (isset($washing) && count($washing) > 0) {

				$washing = $this->db->where_in('ID', $washing)->get('options');

				foreach($washing->result() as $w){

					if(!empty($this->input->post('edit')) || $edit != null)

					{

						$value = $this->getoptionlogprice($w->name,$res_id);

						if($value != null)

						{

							$service_total += $value;

						}else

						{

							$service_total += $w->value;

						}

					}else

					{

						$service_total += $w->value;

					}

					

				}

		

			 	$res['total'] = $res['total'] + $service_total;

			

				

			}



			if (!empty($this->input->post('checkin'))) {



				$res['scenario'] = $scenario;

				$res['pre'] = $pre;



				echo json_encode($res);

			} else {

				return $res;

			}

		



	}

	public function new_compute_reservation_price($checkout = null, $checkin = null, $washing = null, $res_id = null, $price_list = null,$edit = null) {

		

		$this->load->model('prices_model');

		$this->load->model('reservation_model');



		



		if (!empty($this->input->post('checkout'))) {

			$checkout = $this->input->post('checkout');

		}

		if (!empty($this->input->post('checkin'))) {

			$checkin = $this->input->post('checkin');

		}

		// if (!empty($this->input->post('washing')) && empty($washing)) {

		// 	$washing = $this->input->post('washing');

		// }

		if (!empty($this->input->post('washing')) && empty($washing)) {

			$washing = $this->input->post('washing');

		}

		if (!empty($this->input->post('services')) && empty($washing)) {

			$washing = $this->input->post('services');

		}

		if (!empty($this->input->post('res_id')) && empty($res_id)) {

			

			$res_id = $this->input->post('res_id');

			$prices = $this->db->where('reservation_id', $res_id)->get('prices_log')->result_array();

			$price_list_id = $prices[0]['list_id'];

		}

	

		if($res_id != null){

			$edit = "edit";

			$reservation_data = $this->reservation_model->get_agreement_or_reservation($res_id);

		}

		



		if (!empty($this->input->post('new_price_list'))) {

			$new_price_list = $this->input->post('new_price_list');

		}





		if (!empty($this->input->post('price_list'))) {

			$price_list_id = $this->input->post('price_list');

		}



		$website_price_list_id = $this->prices_model->get_option('website_price_list');

		$parkvia_price_list_id = $this->prices_model->get_option('parkvia_price_list');



		if(!isset($price_list_id)){



			$price_list_id = 0;



		}



		//This is in case there is no existing pricelistid for old reservations before pricelists existed

		if($price_list_id == 0){



			//$price_list_id = $website_price_list_id;

			if($res_id != null){

				if($reservation_data['tip'] == 'ParkVia'){

					$price_list_id = $parkvia_price_list_id;

				}

			}



		}



		if(!isset($new_price_list) || $new_price_list == 0){

			$new_price_list = $price_list_id;

		}



	



		if(isset($new_price_list) && ($new_price_list != $price_list_id)){

		



			if($new_price_list == 0){

				$new_price_list = $website_price_list_id;

			}

			//rest_checkout_date is not decalre or call

			// $res_checkout_date  == null ? "" : $reservation_data['checkout_date'];

			// $res_checkin_date  == null ? "" : $reservation_data['checkin_date'];

		$res_checkout_date  = $reservation_data['checkout_date'];

			$res_checkin_date  = $reservation_data['checkin_date'];

			$new_checkout = dmy_to_ymd($checkout);

			$new_checkin = dmy_to_ymd($checkin);



			$days_in_initial_price_list = daysInDateRange($res_checkout_date, $res_checkin_date);



			$days_in_second_price_list = daysInDateRange($res_checkin_date, $new_checkin);



			$days_in_new_reservation = daysInDateRange($res_checkout_date, $new_checkin);







			if($days_in_new_reservation > $days_in_initial_price_list){

				$price_data_second_price_list = $this->calc_price_per_pricelist($new_price_list, $new_checkin, $res_checkout_date, $days_in_initial_price_list + 1, $days_in_initial_price_list + $days_in_second_price_list);

			

				//pre($price_data_initial_price_list);

				$price_data['total'] = $price_data_second_price_list['total'] + $reservation_data['total'];

				$price_data['days'] = (int)$days_in_second_price_list + (int)$reservation_data['days'];

			}else if(($days_in_new_reservation < $days_in_initial_price_list) && $reservation_data['lock_payment'] != 1){



				$price_data = $this->calc_price_per_pricelist($price_list_id, $checkin, $checkout, null, null, $res_id);



			}else{



				$price_data['total'] = $reservation_data['total'];

				$price_data['days'] = (int)$reservation_data['days'];



			}



			



		}else if(!isset($reservation_data)){



			$price_data = $this->calc_price_per_pricelist($price_list_id, $checkin, $checkout);



		}else if(isset($reservation_data) && $reservation_data['lock_payment'] != 1 ){



			$new_checkout = dmy_to_ymd($checkout);

			$new_checkin = dmy_to_ymd($checkin);



			$res_checkout_date = $reservation_data['checkout_date'];

			$res_checkin_date = $reservation_data['checkin_date'];



			$days_in_initial_price_list = daysInDateRange($res_checkout_date, $res_checkin_date);

			$days_in_new_price_list = daysInDateRange($new_checkout, $new_checkin);



			//only calc new price if dates actually change

			if($days_in_initial_price_list != $days_in_new_price_list){

				$price_data = $this->calc_price_per_pricelist($price_list_id, $checkin, $checkout, null, null, $res_id);



				if($reservation_data['tip'] == 'ParkVia'){

						



					if( $days_in_initial_price_list > $days_in_new_price_list ){



						// $price_data['total'] = $reservation_data['initial_total'];

						// $price_data['subtotal'] = $reservation_data['initial_total'];



						$price_data['total'] = $reservation_data['total'];

						$price_data['subtotal'] = $reservation_data['subtotal'];



					}



				}	

			}else{



				$price_data['total'] = $reservation_data['total'];

				$price_data['subtotal'] = $reservation_data['subtotal'];

				$price_data['days'] = $reservation_data['days'];



			}



					







		}else{

			$price_data['total'] = $reservation_data['total'];

			$price_data['subtotal'] = $reservation_data['subtotal'];

			$price_data['days'] = $reservation_data['days'];

		}





			$res['total'] = $price_data['total'];

			$res['subtotal'] = $price_data['total'];

			$res['days'] = $price_data['days'];

		

			 if(!empty($this->input->post('edit')) || $edit != null)

			 {

				$currentservice = $this->db->select_sum('price')->from('reservation_options')->where('res_id',$res_id)->get()->row();

				$currentservice = $currentservice->price;

				$res['total'] = $res['total'] - $currentservice;

				

			 }

			$service_total = 0;

			if (isset($washing) && count($washing) > 0) {

				$washing = $this->db->where_in('ID', $washing)->get('options');

				foreach($washing->result() as $w){

					if(!empty($this->input->post('edit')) || $edit != null)

					{

						$value = $this->getoptionlogprice($w->name,$res_id);

						if($value != null)

						{

							$service_total += $value;

						}else

						{

							$service_total += $w->value;

						}

					}else

					{

						$service_total += $w->value;

					}

					

				}

		

			 	$res['total'] = $res['total'] + $service_total;

			

				

			}

		

				



			if (!empty($this->input->post('checkin'))) {

				echo json_encode($res);

			} else {

				return $res;

			}

		



	}

//getting price from option log by name of option

function getoptionlogprice($option_name,$res_id)

{

	$data = $this->db->where('name',$option_name)->where('reservation_id',$res_id)->get('options_log');

	if($data->num_rows() > 0){

		$data = $data->row();

		return $data->value;

	}

	return null;

	

}	



	public function update_data() {

		$data = $this->input->post();

		$ID = $data['update_id'];

		$update_table = $data['update_table'];

		unset($data['update_id']);

		unset($data['update_table']);

		$this->db->where('ID', $ID)->update($update_table, $data);

		echo "true";

	}

	public function get_company_data_anaf($cui) {

		//$data = $this->input->post();

		$this->load->library('registru_tva');

		$data = $this->registru_tva->get_response($cui);

		return $data;

	}

	public function update_reservation() {



		$data = $this->input->post();



		$washing = null;

		if(isset($data["services"])){

			$washing = $data["services"];//(!empty($data['washing'])) ? "true" : "false";

		}



		$lock_payment = (!empty($data['lock_payment'])) ? "true" : "false";



		//$subtotal_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date'], null, $data['reservation_id'], $data['price_list']);

		$total_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date'], $washing, $data['reservation_id'], $data['price_list']);





		$data['days'] = $total_calculation['days'];

//		if ($lock_payment != 'on') {

			$data['total'] = $total_calculation['total'];

			$data['subtotal'] = $total_calculation['subtotal'];

//		}

		$data['checkout_date'] = $this->dmy_to_ymd($data['checkout_date']);

		$data['checkin_date'] = $this->dmy_to_ymd($data['checkin_date']);

		$customer_data['name'] = $data['name'];

		$customer_data['email'] = $data['email'];

		$customer_data['phone'] = $data['phone'];

		$customer_data['marca'] = !(empty($data['marca'])) ? $data['marca'] : '';

		$customer_data['model'] = !(empty($data['model'])) ? $data['model'] : '';

		$customer_data['nr_inmatriculare'] = !(empty($data['nr_inmatriculare'])) ? $data['nr_inmatriculare'] : '';

		$customer_data['adresa'] = !(empty($data['adresa'])) ? $data['adresa'] : '';

		$customer_data['observatii'] = !(empty($data['observatii'])) ? $data['observatii'] : '';

		$customer_data['firma'] = !(empty($data['firma'])) ? $data['firma'] : '';

		$customer_data['CUI'] = !(empty($data['CUI'])) ? $data['CUI'] : '';

		$this->db->where('ID', $data['customer_id'])->update('customers', $customer_data);



		$this->load->model('cars_model');



		$this->cars_model->save_client_car($customer_data['email'], $customer_data['marca'], $customer_data['model'], $customer_data['nr_inmatriculare']);



		$res_data['checkout_date'] = $data['checkout_date'];

		$res_data['checkin_date'] = $data['checkin_date'];

		$res_data['days'] = $data['days'];



		$res_data['lock_payment'] = $lock_payment == 'true' ? 1 : 0;



		if (!empty($data['subtotal'])) {



			if(isset($data['discount']) && $data['discount'] !== '' && $data['discount'] != 0){

				if (strpos($data['discount'], '%') !== false) {

					$discountPercentage = str_replace('%', '', $data['discount']);

					$discountPercentage = str_replace('-', '', $discountPercentage);



					$res_data['subtotal'] = $data['initial_total'] - (  ($discountPercentage * $data['subtotal']) / 100);

					$res_data['total'] = $data['initial_total'] - (  ($discountPercentage * $data['total']) / 100);



				} else {

					$res_data['subtotal'] = $data['initial_total'] -   $data['discount'] ;

					$res_data['total'] = $data['initial_total'] - $data['discount'] ;

				}



				$res_data['discount'] = $data['discount'];



			} else{

				$res_data['subtotal'] = $data['subtotal'];

				$res_data['total'] = $data['total'];

				$res_data['discount'] = 0;



			}



		}



		// $res_data['washing'] = (isset($data['washing'])) ? 1 : 0;

		$this->db->where('res_id', $data['reservation_id']);

			$this->db->delete('reservation_options');

		if(isset($data["services"])){

			

			//insert reservation options

				//getting price of option from db

				$washing = $this->db->where_in('ID', $washing)->get('options');

				$service_arr = [];

				$washing_option =[];

				foreach($washing->result() as $w){

					$service_arr[] = [

						"res_id" => $data['reservation_id'],

						"option_id" => $w->ID,

						"price" => $w->value

	

					];

					//check option log is alreay exist

					$optioncheck = $this->db->where('reservation_id',$data['reservation_id'])->where('name',$w->name)->get('options_log');

					if($optioncheck->num_rows() < 1)

					{

						$washing_option[] = [

							"name" => $w->name,

							"reservation_id" => $data['reservation_id'],

							"value" => $w->value

		

						];

					}

				

				}

				$this->db->insert_batch('reservation_options', $service_arr);

				if(count($washing_option) > 0){

					$this->db->insert_batch('options_log', $washing_option);

				}

				

			}

			//

		

		if (isset($data['is_agreement']) && $data['is_agreement'] == 'true') {

			$this->db->where('res_id', $data['reservation_id'])->update('agreements', $res_data);

		} else {

			$this->db->where('ID', $data['reservation_id'])->update('reservations', $res_data);

		}

		echo json_encode(true);

	}



	public function update_res_mobilpay_status(){



		$res_no = $this->input->post('res_no');



		$res_data = $this->db->select('total')->where('ID', $res_no)->get('reservations')->row_array();



		$this->db->where('ID', $res_no)->update('reservations', array( 'paid' => $res_data['total'], 'mobilpay_status' => 'Confirmed' ) );



		$this->send_reservation_mail($res_no);



		$payment_data['res_id'] = $res_no;

		$payment_data['amount'] = $res_data['total'];

		$payment_data['type'] = 'mobilpay';

		$payment_data['transfered'] = 1;

		$payment_data['bon_fiscal_emis'] = 0;

		$payment_data['status'] = 1;

		$payment_data['created_by'] = 'alana-parking.ro';



		$this->db->insert('payments', $payment_data);











	}



	public function save_reservation_post() {



		$this->load->model('prices_model');



		$data = $this->input->post('data');

		// print_r($data);exit();

		// $data = json_decode($data, true);

		//  print_r($this->input->post("services"));exit();

		$services = $this->input->post("services");



		$res_data['checkout_date'] = date("Y-m-d H:i:s", strtotime($this->input->post("checkout")));

		$res_data['checkin_date'] = date("Y-m-d H:i:s", strtotime($this->input->post("checkin")));



		$pricelist_id = 0;



		if( $data['pay_online'] == 'true' ){

			

			$pricelist_id_temp = $this->prices_model->get_option('website_price_list_prepaid');



			$pricelist_data = $this->db->where('id', $pricelist_id_temp)->get('prices_list')->row_array();



			$pricelist_data = $this->db->where('name',$pricelist_data['name'])->where('active',1)->where('start_date <=', date('Y-m-d',strtotime($res_data['checkout_date'])))->where('end_date >=', date('Y-m-d', strtotime($res_data['checkin_date'])))->order_by('start_date','desc')->get('prices_list')->row_array();



			$pricelist_id = $pricelist_data['id'];



			$price_data = $this->calc_price_per_pricelist($pricelist_id, $res_data['checkin_date'], $res_data['checkout_date']);



			$res_data['total'] = $price_data['total'];



			$res_data['mobilpay_status'] = 'Unpaid';



		}else{



			$pricelist_id_temp = $this->prices_model->get_option('website_price_list');





			$pricelist_data = $this->db->where('id', $pricelist_id_temp)->get('prices_list')->row_array();



			$pricelist_data = $this->db->where('name',$pricelist_data['name'])->where('active',1)->where('start_date <=', date('Y-m-d',strtotime($res_data['checkout_date'])))->where('end_date >=', date('Y-m-d', strtotime($res_data['checkin_date'])))->order_by('start_date','desc')->get('prices_list')->row_array();





			$pricelist_id = $pricelist_data['id'];





			$price_data = $this->calc_price_per_pricelist($pricelist_id, $res_data['checkin_date'], $res_data['checkout_date']);



			$res_data['total'] = $price_data['total'];

		}



		$res_data['days'] = $price_data['days'];

		$res_data['subtotal'] = $price_data['total'];

		$res_data['total'] = $price_data['total'];

		$res_data['initial_total'] = $price_data['total'];

		$res_data['discount'] = 0;



		$lock_payment = $this->input->post('lock_payment');

		

		// $washing = (!empty($data['washing'])) ? $data['washing'] : 'false';

		

		// $subtotal_calculation = $this->compute_reservation_price($data['data_plecare'], $data['data_retur']);

		// $total_calculation = $this->compute_reservation_price($data['data_plecare'], $data['data_retur'], $washing);

		// $data['total'] = $total_calculation['total'];

		// $data['subtotal'] = $subtotal_calculation['total'];

		// $data['days'] = $total_calculation['days'];

		// $data['initial_total'] = $total_calculation['total'];

		

		$customer_data['name'] = $data['nume'];

		$customer_data['email'] = $data['email'];

		$customer_data['phone'] = $data['telefon'];

		$customer_data['CUI'] = (!empty($data['cui'])) ? $data['cui'] : 0;

		//print_r($customer_data);

		$this->db->insert('customers', $customer_data);

		$success = ($this->db->affected_rows() != 1) ? false : true;

		if (!$success) {

			echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut insera clientul'));

			exit();

		}

		$res_data['customer_id'] = $this->db->insert_id();

		$parking_spot = $this->find_parking_spot($res_data['checkout_date'], $res_data['checkin_date']);

		$res_data['parking_spot'] = $parking_spot;

		



		







		$res_data['washing'] = 0;//($data['washing'] == 'true') ? 1 : 0;

		$res_data['status'] = 1;

		$res_data['created_by'] = 3;

		$res_data['tip'] = 'Standard';

		if(isset($data["request_invoice"])){

			$res_data['request_invoice'] = ($data['request_invoice'] == 'true') ? 1 : 0;

		}else{

			$res_data['request_invoice'] = 0 ;

		}

		



		//website locked price logic



		// $washingDb = $this->db->where('name', 'washing')->get('options')->row();

		 $washingPrice = 0;



		// if($res_data['washing'] === 1) {

		// 	$washingPrice = $washingDb->value;

		// }



		

		//getting price

		$washing = $this->db->where_in('ID', $services)->get('options');

		foreach($washing->result() as $w)

		{

			$washingPrice+=$w->value;

		}

		 $res_data['total'] += $washingPrice;

			//

		// $res_data['locked_price'] = number_format(($data['total'] - $washingPrice) / $data['days'], 2);

		// $res_data['lock_payment'] = 1;

		//end website locked price logic

		

		$this->db->insert('reservations', $res_data);

		$success = ($this->db->affected_rows() != 1) ? false : true;

		$res_id = $this->db->insert_id();

		if(isset($services)){

			//insert reservation options

				//getting price of option from db

				

				$service_arr = [];

				

				foreach($washing->result() as $w){

					$service_arr[] = [

						"res_id" => $res_id,

						"option_id" => $w->ID,

						"price" => $w->value

	

					];

				

				}

			//

			

			$this->db->insert_batch('reservation_options', $service_arr);

			

			}

			//addd option log with current option services

			$option_logs = $this->db->where('status',1)->get('options');

			if($option_logs->num_rows() > 0)

			{

				$washing_option =[];

				foreach($option_logs->result() as $o)

				{

					$washing_option[] = [

						"name" => $o->name,

						"reservation_id" => $res_id,

						"value" => $o->value

	

					];

				}

				$this->db->insert_batch('options_log', $washing_option);

			}

			//





		$prices = $this->db->where('list_id', $pricelist_id)->get('prices')->result_array();

		foreach ($prices as $price) {

			unset($price['ID']);

			$price['reservation_id'] = $res_id;

			$this->db->insert('prices_log', $price);

		}

		// $washing_option = $this->db->where('name', 'washing')->get('options')->result_array() [0];

		// $washing_option['reservation_id'] = $res_id;

		// unset($washing_option['ID']);

		// $this->db->insert('options_log', $washing_option);

		if ($success) {





			echo json_encode(array('success' => true, 'msg' => 'Rezervare salvata cu succes!', 'res_id' => $res_id, 'price_list_id' => $pricelist_id));

			exit();

		} else {

			echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut salva rezervarea!'));

			exit();

		}

	}



	public function save_membership_reservation() {

		$data = $this->input->post();

		$res_data['customer_id'] = $data['customer_id'];

		$subtotal_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date']);

		$total_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date']);

		$data['total'] = 0;

		$data['subtotal'] = 0;

		$data['days'] = $total_calculation['days'];

		$data['checkout_date'] = $this->dmy_to_ymd($data['checkout_date']);

		$data['checkin_date'] = $this->dmy_to_ymd($data['checkin_date']);

		$parking_spot = $this->find_parking_spot($data['checkout_date'], $data['checkin_date']);

		$res_data['parking_spot'] = $parking_spot;

		$res_data['checkout_date'] = $data['checkout_date'];

		$res_data['checkin_date'] = $data['checkin_date'];

		$res_data['tip'] = $data['tip'];

		$res_data['days'] = $data['days'];

		$res_data['subtotal'] = $data['subtotal'];

		$res_data['total'] = $data['total'];

		$res_data['washing'] = (isset($data['washing'])) ? 1 : 0;

		$res_data['status'] = 1;

		$res_data['created_by'] = $_SESSION['auth_user_id'];

		$this->db->insert('reservations', $res_data);

		$success = ($this->db->affected_rows() != 1) ? false : true;

		$res_id = $this->db->insert_id();

		if ($success) {

			echo json_encode(array('success' => true, 'msg' => 'Rezervare salvata cu succes!', 'res_id' => $res_id));

			exit();

		} else {

			echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut salva rezervarea!'));

			exit();

		}

	}

	public function save_reservation() {



		$lockedPriceFor  = array('Standard');



		$data = $this->input->post();

		$washing = null;

		if(isset($data["services"])){

			$washing = $data["services"];//(!empty($data['washing'])) ? "true" : "false";

		}

	

		$send_mail = (!empty($data['send_mail'])) ? "true" : "false";

		$subtotal_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date'], null, null, $data['price_list'],null);

		$total_calculation = $this->compute_reservation_price($data['checkout_date'], $data['checkin_date'], $washing, null, $data['price_list'],null);

		$data['total'] = $data['discounted_price'];

		$data['initial_total'] = $data['full_price'];

		$data['subtotal'] = $subtotal_calculation['total'];

		$data['days'] = $total_calculation['days'];

		$data['checkout_date'] = $this->dmy_to_ymd($data['checkout_date']);

		$data['checkin_date'] = $this->dmy_to_ymd($data['checkin_date']);

		

		if(empty($data['clientid'])){

			$customer_data['name'] = $data['name'];

			$customer_data['email'] = $data['email'];

			$customer_data['phone'] = $data['phone'];

			$customer_data['firma'] = (!isset($data['firma'])) ? '' : $data['firma'];

			$customer_data['CUI'] = (isset($data['CUI'])) ? $data['CUI'] : '';

			$this->db->insert('customers', $customer_data);

			$success = ($this->db->affected_rows() != 1) ? false : true;

			if (!$success) {

				echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut insera clientul'));

				exit();

			}

			$res_data['customer_id'] = $this->db->insert_id();

		}else{

			$res_data['customer_id'] = $data['clientid'];

		}



		

		$parking_spot = $this->find_parking_spot($data['checkout_date'], $data['checkin_date']);

		$res_data['parking_spot'] = $parking_spot;

		$res_data['checkout_date'] = $data['checkout_date'];

		$res_data['checkin_date'] = $data['checkin_date'];

		$res_data['tip'] = $data['tip'];

		if($data['tip'] == 'ParkVia'){

			$res_data['paid'] = $data['total'];

		}



		if (in_array($data['tip'], $lockedPriceFor)) {



			$washingDb = $this->db->where('name', 'washing')->get('options')->row();

			$washingPrice = 0;



			if($washing == 'true') {

				$washingPrice = $washingDb->value;

			}



			$res_data['locked_price'] = number_format(($data['total'] - $washingPrice) / $data['days'], 2);

			$res_data['lock_payment'] = 0;

		}



		$res_data['days'] = $data['days'];

		$res_data['subtotal'] = $data['total'];

		$res_data['discount'] = $data['discount'];

		$res_data['initial_total'] = $data['initial_total'];

		$res_data['total'] = $data['total'];

		$res_data['washing'] = 0;//(!empty($data['washing'])) ? 1 : 0;

		$res_data['status'] = 1;

		$res_data['created_by'] = $_SESSION['auth_user_id'];

		$this->db->insert('reservations', $res_data);

		$success = ($this->db->affected_rows() != 1) ? false : true;

		$res_id = $this->db->insert_id();

		if(isset($data["services"])){

		//insert reservation options

			//getting price of option from db

			$washing = $this->db->where_in('ID', $washing)->get('options');

			$service_arr = [];

			

			foreach($washing->result() as $w){

				$service_arr[] = [

					"res_id" => $res_id,

					"option_id" => $w->ID,

					"price" => $w->value



				];

			

			}

		//

		

		$this->db->insert_batch('reservation_options', $service_arr);

		

		}

		//addd option log with current option services

		$option_logs = $this->db->where('status',1)->get('options');

		if($option_logs->num_rows() > 0)

		{

			$washing_option =[];

			foreach($option_logs->result() as $o)

			{

				$washing_option[] = [

					"name" => $o->name,

					"reservation_id" => $res_id,

					"value" => $o->value



				];

			}

			$this->db->insert_batch('options_log', $washing_option);

		}

		//

		

		if($data['tip'] == 'ParkVia'){



			$payment_data['res_id'] = $res_id;

			$payment_data['amount'] = $data['total'];

			$payment_data['type'] = 'ParkVia';

			$payment_data['transfered'] = 1;

			$payment_data['bon_fiscal_emis'] = 0;

			$payment_data['status'] = 1;

			$payment_data['created_by'] = $_SESSION['auth_user_id'];



			$this->db->insert('payments', $payment_data);



		}



		$pricelist_data = $this->db->where('id',  $data['price_list'])->get('prices_list')->row_array();



		$pricelist_data = $this->db->where('name',$pricelist_data['name'])->where('active',1)->where('start_date <=', date('Y-m-d',strtotime($data['checkout_date'])))->where('end_date >=', date('Y-m-d', strtotime($data['checkin_date'])))->order_by('start_date','desc')->get('prices_list')->row_array();



		$pricelist_id = $pricelist_data['id'];



		$prices = $this->db->where('list_id',$pricelist_id)->get('prices')->result_array();





		foreach ($prices as $price) {

			unset($price['ID']);

			$price['reservation_id'] = $res_id;

			$this->db->insert('prices_log', $price);

		}

		// $washing_option = $this->db->where('name', 'washing')->get('options')->result_array() [0];

		// $washing_option['reservation_id'] = $res_id;

		// unset($washing_option['ID']);

		// $this->db->insert('options_log', $washing_option);

		if ($success) {

			if($send_mail == 'true'){

				$this->send_reservation_mail($res_id);

			}

			echo json_encode(array('success' => true, 'msg' => 'Rezervare salvata cu succes!', 'res_id' => $res_id));

			exit();

		} else {

			echo json_encode(array('success' => false, 'msg' => 'Nu s-a putut salva rezervarea!'));

			exit();

		}

	}

	public function recalc_parking_spots() {

		$res = $this->db->where_in('status', array(1, 4))->get('reservations')->result_array();

		$this->db->update('reservations', array('parking_spot' => 0));

		foreach ($res as $r) {

			$spot = $this->find_parking_spot($r['checkout_date'], $r['checkin_date']);

			$this->db->where('ID', $r['ID'])->update('reservations', array('parking_spot' => $spot));

		}

	}



	public function cancel_contract(){

		$id = $this->input->post('id');



		$this->db->set('status', 2);

		$this->db->where('ID', $id);

		$this->db->update('agreements');

	}



	public function find_parking_spot($checkout, $checkin) {

		$total_parking_spots = $this->db->where('name', 'total_parking_spots')->get('options')->row()->value;

		$statuses = array(1, 4);

		$results = $this->db->group_start()->or_group_start()->where('checkin_date >=', $checkin)->where('checkout_date <=', $checkin)->group_end()->or_group_start()->where('checkin_date >=', $checkout)->where('checkout_date <=', $checkout)->group_end()->or_group_start()->where('checkin_date <=', $checkin)->where('checkout_date >=', $checkout)->group_end()->group_end()->where_in('status', $statuses)->order_by('parking_spot', 'ASC')->get('reservations')->result_array();

		/*$this->db->where('checkin_date >=', $checkout);

		$this->db->where('checkout_date <=', $checkin);

		$this->db->where_in('status', $statuses);

		$this->db->order_by('parking_spot', 'ASC');

		$results = $this->db->get('reservations')->result_array();*/

		if (empty($results)) {

			return 1;

		}

		$parking_spots = array();

		foreach ($results as $res) {

			array_push($parking_spots, $res['parking_spot']);

		}

		$i = 1;

		while ($i <= $total_parking_spots) {

			if (!in_array($i, $parking_spots)) {

				return $i;

				exit();

			}

			$i++;

		}

	}

	public function reservations() {



		$userid = $_SESSION['auth_user_id'];



		$data['access_level'] = $this->db->select('access_level')->where('id', $userid)->get('users')->row()->access_level;



		$this->db->select('*, r.ID as ID');

		$this->db->join('customers c', 'c.ID = r.customer_id', 'left');

		$data['data'] = $this->db->get('reservations r')->result_array();

		$data['data'] = '';

		$this->load->view('reservations', $data);

	}



	public function dt_reservations(){



		$this->load->model('Dt_reservations_model');



		$data = $row = array();



		$memData = $this->Dt_reservations_model->getRows($_POST);



		$i = $_POST['start'];



		foreach($memData as $member){

			$i++;



			$checkout_date = date('d.m.Y H:i', strtotime($member->checkout_date));

			$checkin_date = date('d.m.Y H:i', strtotime($member->checkin_date));



			$statuses = array(



				1 => 'Confirmat',

				2 => 'Anulat',

				3 => 'No Show',

				4 => 'Contract',



			);



			if( $member->mobilpay_status == '' ) {

				$mobilpay_status = 'Irelevant';

			}

			if( $member->mobilpay_status == 'Unpaid' ) {

				$mobilpay_status = '<p style="background: orange; color: #fff; padding: 5px 10px; border-radius: 5px; display: inline-block; margin: 0;">' . $member->mobilpay_status . '</p>';

			}

			if( $member->mobilpay_status == 'Confirmed' ) {

				$mobilpay_status = '<p style="background: green; color: #fff; padding: 5px 10px; border-radius: 5px; display: inline-block; margin: 0;">' . $member->mobilpay_status . '</p>';

			}



			if($member->tip == 'ParkVia') {

				$mobilpay_status = '<p class="parkvia-prepaid"><img class="pv-prepaid-logo" src="assets/images/PV_Logo.png"></p>';

			}



			$data[] = array('<a href="/edit_reservation?id=' . $member->ID . '">'.$member->ID.'</a>', $member->name, $checkout_date ,$checkin_date,$member->tip,$member->phone,$statuses[$member->status],$member->total,$member->username, date('d-m-Y H:i:s', strtotime($member->created_at)), $mobilpay_status);

		}



		$recordsTotal = $this->Dt_reservations_model->countAll();



		$recordsFiltered = $this->Dt_reservations_model->countFiltered($_POST);



		//$recordsFiltered = $i;

		$this->benchmark->mark('code_end');



		$output = array(

			"draw" => $_POST['draw'],

			"recordsTotal" => $recordsTotal,

			"recordsFiltered" => $recordsFiltered,

			"data" => $data,

		);



		echo json_encode($output);



	}



	public function agreements() {

		$this->db->select('*, r.ID as ID, r.status as status');

		$this->db->join('customers c', 'c.ID = r.customer_id', 'left');

		$this->db->join('users u', 'u.ID = r.created_by', 'left');

		$data['data'] = $this->db->get('agreements r')->result_array();



		$userid = $_SESSION['auth_user_id'];

//

//        dd($data);





		$data['access_level'] = $this->db->select('access_level')->where('id', $userid)->get('users')->row()->access_level;





		$this->load->view('agreements', $data);

	}



	public function new_agreements() {

		$this->db->select('*, r.ID as ID, r.status as status');

		$this->db->join('customers c', 'c.ID = r.customer_id', 'left');

		$this->db->join('users u', 'u.ID = r.created_by', 'left');

		$data['data'] = $this->db->get('agreements r')->result_array();



		$userid = $_SESSION['auth_user_id'];

//

//        dd($data);





		$data['access_level'] = $this->db->select('access_level')->where('id', $userid)->get('users')->row()->access_level;





		$this->load->view('new_agreements', $data);

	}



	public function new_agreements_ajax() {



		$column = [

			'agreements.ID',

			'customers.name',

			'agreements.checkout_date',

			'agreements.checkin_date',

			'customers.phone',

			'customer_auto',

			'agreements.washing',

			'agreements.mobilpay_status',

			'agreements.status',

			'agreements.total',

			'users.username',

			'agreements.tip',

		];





		$draw   = $this->input->get( 'draw' );

		$start  = $this->input->get( 'start' );

		$length = $this->input->get( 'length' );

		$status = $this->input->get('status');

		$search = $this->input->get('search')['value'];







		$select = "SELECT SQL_CALC_FOUND_ROWS agreements.ID as agreement_id,

            agreements.checkin_date,

            agreements.checkout_date,

            agreements.created_at,     

            agreements.washing,

            agreements.mobilpay_status,

            agreements.status,

            agreements.total,

            agreements.tip,    

            customers.name as customer_name,

            customers.phone as customer_phone,

            users.username,

            CONCAT(customers.marca,' ',customers.model,' ',customers.nr_inmatriculare)  as customer_auto        

            FROM  agreements

            LEFT JOIN customers ON customers.ID = agreements.customer_id

            LEFT JOIN users ON agreements.created_by = users.ID

        ";



		if($status != -1) {

			$select .=  " where agreements.status = $status";

		}else {

			$select .= " where 1 = 1";

		}



		if ($search) {

			$select .= " AND (customers.name LIKE '%" . $search . "%' OR customers.phone LIKE '%" . $search . "%' OR CONCAT(customers.marca,' ',customers.model,' ',customers.nr_inmatriculare) LIKE '%" . $search . "%' )  ";

		}





		if ($this->input->get('order')[0]['column'] || $this->input->get('order')[0]['column'] == 0) {

			if (isset($column[$this->input->get('order')[0]['column']])) {

				$select .= " order by " . $column[$this->input->get('order')[0]['column']] . " " . strtoupper($this->input->get('order')[0]['dir']);

			}

		} 







		$select .= " LIMIT ".$length." OFFSET ".$start;



		$query = $this->db->query($select);

		$count = $this->db->query('SELECT FOUND_ROWS() as totalFound');

		$countResult  = $count->result();

		$data = $query->result();

//        dd($query->result());



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $countResult[0]->totalFound,

			"recordsFiltered" =>  $countResult[0]->totalFound,

			"data" => $data,

		);



		$this->output

			->set_status_header(200)

			->set_content_type('application/json', 'utf-8')

			->set_output(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))

			->_display();

		exit;

	}



	public function change_reservation_status() {

		$data = $this->input->post();

		$this->db->where('ID', $data['res_id']);

		$this->db->update('reservations', array('status' => $data['status']));

		if ($data['status'] == 4) {

			$agr_id = $this->create_agreement($data['res_id']);

			echo json_encode($agr_id);

		}

		//$this->load->view('reservations', $data);



	}

	public function change_agreement_status() {

		$data = $this->input->post();

		$this->db->where('ID', $data['res_id']);

		$this->db->update('agreements', array('status' => $data['status']));



		//Only happens if Agreement is Closing

		if ($data['status'] == 5) {

			$customer_id = $this->db->where('ID', $data['res_id'])->get('agreements')->row()->customer_id;

			$cus_data = $this->db->where('ID', $customer_id)->get('customers')->row();

			if ($cus_data->wants_invoice == 1) {

				$company_data = $this->get_company_data_anaf($cus_data->CUI);

				//print_r($company_data);

				if (!empty($company_data)) {

					$cus_data = (array)$cus_data;

					//print_r($cus_data);

					$settings = $this->db->where('email_status',1)->get('settings');

					if($settings->num_rows() > 0)

					{

						$this->load->library('email');

						$subject = 'Agreement-ul ' . $data['res_id'] . ' doreste factura pentru parcare';

						/** Get full html: */

						$body = 'Detaliile sunt urmatoarele: <br />';

						$company_data = json_decode($company_data);

						$body.= 'Date firma: <br />';

						foreach ($company_data->success[0] as $key => $value) {

							$body.= $key . ': ' . $value . '<br />';

						}

						$body.= 'Date client: <br />';

						foreach ($cus_data as $key => $value) {

							$body.= $key . ': ' . $value . '<br />';

						}

						//print_r($body);

						@$result = $this->email->from('reservations@flyparking.ro')->reply_to('reservations@flyparking.ro')

							/** Optional, an account where a human being reads. */->to('reservations@flyparking.ro')->cc('emtoader@gmail.com')->subject($subject)->message($body)->send();

						echo 1;

					}

				

					exit;

				}

			}

		}

	}

	public function create_agreement($res_id) {

		$data = $this->db->select('*')->where('ID', $res_id)->get('reservations')->result_array();

		$data = $data[0];

		$data['res_id'] = $data['ID'];

		unset($data['ID']);

		unset($data['created_at']);

		$data['created_by'] = $_SESSION['auth_user_id'];

		$this->db->insert('agreements', $data);

		return $this->db->insert_id();

	}

	public function casierie() {

		$data['payments'] = $this->db->select('*, payments.ID as payment_id')->join('payments', 'agreements.res_id = payments.res_id', 'left')->join('customers', 'customers.ID = agreements.customer_id', 'left')->where('payments.type', 0)->where('payments.transfered', 0)->get('agreements')->result();

		//echo '<pre>'; var_dump($data['payments']); exit();

		$this->load->view('casierie', $data);

	}



	public function price_lists() {

		$data['page_title'] = 'Liste preturi';

		$this->load->helper('xcrud_helper');

		$xcrud = xcrud_get_instance();

		$xcrud->table('prices_list');

		$xcrud->order_by('name, start_date');

		$xcrud->unset_view();

		$edit_price_url = base_url('/main_controller/edit_prices/{id}');
		$xcrud->button($edit_price_url,'Actualizeaza preturi','icon-calculate','xcrud-green');

		$data['table'] = $xcrud->render();



		$data['price_lists'] = $this->db->select('*')->order_by('name','asc')->group_by('name')->get('prices_list')->result_array();



		$data['price_options'] = meta_key_value_transform( $this->db->get('prices_options')->result_array() );



		$this->load->view('price_lists', $data);

	}



	public function copy_price_list(){



		$data = $this->input->post();



		$copy_price_list_id = $data['copy_price_list_id'];

		unset($data['copy_price_list_id']);



		$prices = $this->db->select('*')->where('list_id', $copy_price_list_id)->order_by('days','asc')->get('prices')->result_array();



		$this->db->insert('prices_list', $data);



		$new_price_list = $this->db->insert_id();



		foreach ($prices as $key => $p) {

			

			$prices[$key]['list_id'] = $new_price_list;

			unset($prices[$key]['ID']);



		}





		$this->db->insert_batch('prices', $prices);



		//set_alert('success', 'Lista copiata');



		redirect('/main_controller/edit_prices/'.$new_price_list);



	}





	public function edit_prices($listid) {

		$data['prices'] = $this->db->select('*')->where('list_id', $listid)->order_by('days','asc')->get('prices')->result();



		$data['price_list'] = $this->db->where('id', $listid)->get('prices_list')->row();



		$this->load->view('edit_prices', $data);

	}







	public function update_prices() {



		$data = $this->input->post('data');





		foreach($data as $index => $prices){



			if($prices['price'] == 0 || $prices['price'] == '0'){

				continue;

			}



			$price_data = $this->db->where('days', $prices['day'])->where('list_id', $prices['price_list_id'])->get('prices');



			if($price_data->num_rows() > 0){

				$this->db->where('days', $prices['day'])->where('list_id', $prices['price_list_id'])->update('prices',array('price' => $prices['price']));

			}else{

				$this->db->insert('prices', array('days'=>$prices['day'], 'price'=>$prices['price'], 'list_id' => $prices['price_list_id']));

			}



		}



	}







	public function spalatorie(){



		$data['upcoming_washes'] = $this->db->select('*, agreements.res_id as reservationn_id, agreements.ID as ID')->join('customers', 'customers.ID = agreements.customer_id', 'left')->where('washing', 1)->where('checkin_date >', date('Y-m-d H:m:s'))->get('agreements')->result();



		//$data['washing_price'] = $this->db->select('*')->where('name','washing')->get('options')->row()->value;

		$data['washing_price'] = $this->db->select('*')->get('options');

		// echo $this->db->last_query() ;

		// die();

//echo '<pre>'; var_dump($data['upcoming_washes']); exit();



		$this->load->view('spalatorie', $data);

	}



	public function unpaid_contracts(){



		$data['unpaid_contracts'] = $this->db->select('*, agreements.res_id as reservationn_id, agreements.ID as ID')->join('customers', 'customers.ID = agreements.customer_id', 'left')->where('agreements.status',5)->where('checkin_date <', date('Y-m-d H:m:s'))->where('( total - paid ) >', 0.1)->get('agreements')->result();





//echo '<pre>'; var_dump($data['upcoming_washes']); exit();



		$this->load->view('unpaid_contracts', $data);

	}

	//create service

	public function create_service(){

		$name = $this->input->post('name');

		$price = $this->input->post('price');

		$data = ["name" => $name,"value" => $price];

	

	$this->db->insert('options', $data);

		

		redirect(base_url('spalatorie'));

	}

	//update services 

	public function update_services(){

		$id = $this->input->post('id');

		$name = $this->input->post('name');

		$price = $this->input->post('price');

		$data = ["name" => $name,"value" => $price];

		$this->db->where('ID', $id);

		$this->db->update('options',$data);

		

		redirect(base_url('spalatorie'));

	}

	public function change_washing_price(){

		$new_val = $this->input->post('new_price');



		$this->db->set('value', $new_val);

		$this->db->where('ID', 1);

		$this->db->update('options');



	}



	public function add_extra_costs() {

		$agreement_id = $this->input->post('agreement_id');

		$serviciu = $this->input->post('serviciu');

		$pret = $this->input->post('pret');

		$data = array('res_id' => $agreement_id, 'serviciu' => $serviciu, 'pret' => $pret, 'created_by' => $_SESSION['auth_user_id']);

		$this->db->insert('costuri_extra', $data);

		if (is_int($this->db->insert_id())) {

			echo json_encode(true);

		}

	}

	public function historic_casierie() {



		if (isset($_GET['start']) && isset($_GET['end'])) {

			$data['payments'] = $this->db->select('*, reservations.ID as ID, payments.created_at as created_at')->join('payments', 'reservations.ID = payments.res_id', 'left')->join('customers', 'customers.ID = reservations.customer_id', 'left')->where('payments.created_at >=', $_GET['start'])->where('payments.created_at <=', $_GET['end'])->get('reservations')->result();

		} else {

			// $data['payments'] = $this->db->select('*')->join('payments', 'agreements.ID = payments.res_id', 'left')->join('customers', 'customers.ID = agreements.customer_id', 'left')->where('payments.type', 0)->get('agreements')->result();



			$data['payments'] = array();



		}

		$this->load->view('historic_casierie', $data);

	}

	public function move_to_bank() {

		$payments = $this->input->post('selected');

		foreach ($payments as $id) {

			$this->db->set('transfered', 1);

			$this->db->where('type', 0);

			$this->db->where('ID', $id);

			$this->db->update('payments');

		}

	}

	public function add_payment() {

		$data = $this->input->post();

		$data['created_by'] = $_SESSION['auth_user_id'];

		$table = (!empty($data['agreement'])) ? 'agreements' : 'reservations';

		if ($table == 'agreements') {

			$response = $this->db->select('res_id, total, paid')->where('res_id', $data['res_id'])->get('agreements')->result_array() [0];

			$data['res_id'] = $response['res_id'];

		} else {

			$response = $this->db->select('total, paid')->where('ID', $data['res_id'])->get('reservations')->result_array() [0];

		}

		$paid = $response['paid'];

		$total = $response['total'];

		//if()

		$paid = $paid + $data['amount'];

		//echo json_encode($paid.'--'.$total); exit();

		if ($paid > $total) {

			echo json_encode(false);

			exit();

		}

		unset($data['agreement']);

		$data['status'] = 1;

		$this->db->insert('payments', $data);

		if ($table == 'agreements') {

			$this->db->where('res_id', $data['res_id'])->update('agreements', array('paid' => $paid));

		} else {

			$this->db->where('ID', $data['res_id'])->update('reservations', array('paid' => $paid));

		}

		echo json_encode(true);

	}

	public function dmy_to_ymd($datetime) {

		$date = date_create_from_format('d/m/Y H:i', $datetime);

		if ($date != false) {

			$datetime = date_format($date, 'Y-m-d H:i:s');

		}

		return $datetime;

	}

	public function ymd_to_dmy($datetime) {

		$date = date_create_from_format('Y-m-d H:i:s', $datetime);

		if ($date != false) {

			$datetime = date_format($date, 'd/m/Y H:i');

		}

		return $datetime;

	}

	public function update_reservation_actions() {

		$data = $this->input->post();

		$data['created_by'] = $_SESSION['auth_user_id'];

		$this->db->insert('payments', $data);

		$table = (!empty($data['agreement'])) ? 'agreements' : 'reservations';

		if ($table == 'agreements') {

			$response = $this->db->select('paid')->where('res_id', $data['res_id'])->get('agreements')->result_array() [0];

		} else {

			$response = $this->db->select('paid')->where('ID', $data['res_id'])->get('reservations')->result_array() [0];

		}

		$paid = $response['paid'];

		$paid = $paid + $data['amount'];

		if ($table == 'agreements') {

			$this->db->where('res_id', $data['res_id'])->update('agreements', array('paid' => $paid));

		} else {

			$this->db->where('ID', $data['res_id'])->update('reservations', array('paid' => $paid));

		}

	}

	public function send_reservation_mail($res_id) {

		$data = $this->input->post();

	

		$settings = $this->db->where('email_status',1)->get('settings');

					if($settings->num_rows() > 0)

					{

					    

		$this->load->library('email');

		$res_data = $this->db->select('*, reservations.created_at as reservation_created_at')->join('customers', 'customers.ID = reservations.customer_id', 'left')->where('reservations.id', $res_id)->get('reservations')->row();

		//echo json_encode($res_data); exit();

		$subject = 'test';

		$searchReplaceArray = array('{firstname}' => $res_data->name, '{phone_no}' => $res_data->phone, '{email}' => $res_data->email, '{checkout_date}' => $res_data->checkout_date, '{checkin_date}' => $res_data->checkin_date, '{created_date}' => $res_data->created_at, '{reservation_number}' => 123, '{total}' => $res_data->total);

		$message = $this->db->select('text')->where('language', 'RO')->get('email_remplates')->row()->text;

		$message = str_replace(array_keys($searchReplaceArray), array_values($searchReplaceArray), $message);

		/** Get full html: */

		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml">

		<head>

			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />

			<title>' . $subject . '</title>

			<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

			<link href="http://alana-rent.m-web-design.ro/metronic_assets/pages/css/invoice-2.min.css" rel="stylesheet" type="text/css">

			<script src="https://code.jquery.com/jquery-3.2.1.min.js"

				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="

				  crossorigin="anonymous"></script>

			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

		</head>

		<body>

		' . $message . '

		</body>

		</html>';

		/** Also, for getting full html you may use the following internal method: */

		/** $body = $this->email->full_html($subject, $message); */

		@$result = $this->email->from('reservations@flyparking.ro')->reply_to('reservations@flyparking.ro')

			/** Optional, an account where a human being reads. */->to($res_data->email)->cc('reservations@flyparking.ro')->subject('Rezervarea dumneavoastra')->message($body)->send();

	}



	}

	public function mail_templates() {

		$data['templates'] = $this->db->get('email_remplates')->result();

		$this->load->view('mail_templates', $data);

	}

	public function edit_template() {

		$id = $_GET['id'];

		$data['template'] = $this->db->select('*')->where('ID', $id)->get('email_remplates')->row();

		$this->load->view('edit_template', $data);

	}

	public function edit_user() {

		$id = $_GET['id'];

		$data['user'] = $this->db->select('*')->where('ID', $id)->get('users')->row();

		$this->load->view('edit_user', $data);

	}

	public function update_user() {

		$email = $this->input->post('email');

		try {

			$this->auth->forgotPassword($_POST['email'], function ($selector, $token) {

				$password = $this->input->post('password');

				$this->auth->resetPassword($selector, $token, $password);

				echo 'Password has been reset';

			});

		}

		catch(\Delight\Auth\InvalidSelectorTokenPairException $e) {

			die('Invalid token');

		}

		catch(\Delight\Auth\TokenExpiredException $e) {

			die('Token expired');

		}

		catch(\Delight\Auth\ResetDisabledException $e) {

			die('Password reset is disabled');

		}

		catch(\Delight\Auth\InvalidPasswordException $e) {

			die('Invalid password');

		}

		catch(\Delight\Auth\TooManyRequestsException $e) {

			die('Too many requests');

		}

	}

	public function update_template() {

		$template_id = $this->input->post('id');

		$html = $this->input->post('html');

		$this->db->set('text', $html);

		$this->db->where('ID', $template_id);

		$this->db->update('email_remplates');

	}

	public function delete_user() {

		$user_id = $this->input->post('user_id');

		try {



			$this->db->where('id', $user_id);



			$success = $this->db->update('users', array('status' => 5));

			echo json_encode(['status' =>  $success]);



//			$this->db->update('users','SET status = 5', 'id = '.$user_id, 1 );

//			$this->auth->admin()

		}

		catch(\Delight\Auth\UnknownIdException $e) {

			die('Unknown ID');

		}

	}

	public function create_user() {

		$email = $this->input->post('email');

		$password = $this->input->post('password');

		$username = $this->input->post('username');

		$search_for_email = $this->db->select('*')->where('email', $email)->get('users');

		if ($search_for_email->num_rows() > 0) {

			echo json_encode(false);

		} else {

			try {

				$userId = $this->auth->register($email, $password, $username, function ($selector, $token) {

					$this->confirm_user_registration($selector, $token);

				});

				echo json_encode(true);

			}

			catch(\Delight\Auth\InvalidEmailException $e) {

				die('Invalid email address');

			}

			catch(\Delight\Auth\InvalidPasswordException $e) {

				die('Invalid password');

			}

			catch(\Delight\Auth\UserAlreadyExistsException $e) {

				die('User already exists');

			}

			catch(\Delight\Auth\TooManyRequestsException $e) {

				die('Too many requests');

			}

		}

	}

	public function confirm_user_registration($selector, $token) {

		try {

			$this->auth->confirmEmail($selector, $token);

			echo 'Email address has been verified';

		}

		catch(\Delight\Auth\InvalidSelectorTokenPairException $e) {

			die('Invalid token');

		}

		catch(\Delight\Auth\TokenExpiredException $e) {

			die('Token expired');

		}

		catch(\Delight\Auth\UserAlreadyExistsException $e) {

			die('Email address already exists');

		}

		catch(\Delight\Auth\TooManyRequestsException $e) {

			die('Too many requests');

		}

	}

	public function users() {

		$data['users'] = $this->db->get('users')->result();

		$this->load->view('users', $data);

	}

	public function add_user() {

		$this->load->view('add_user');

	}

	public function edit_reservation() {



		$this->load->model('prices_model');



		$res_id = $this->input->get('id');

		$data['res_data'] = $this->db->where('ID', $res_id)->get('reservations')->result_array() [0];

		$data['cus_data'] = $this->db->where('ID', $data['res_data']['customer_id'])->get('customers')->result_array() [0];

		$data['price_data'] = @$this->db->where('reservation_id', $res_id)->get('prices_log')->result_array() [0];

		$data['options_data'] = @$this->db->where('reservation_id', $res_id)->get('options_log')->result_array() [0];

		$data['prices_list'] = $this->prices_model->get_price_lists();



		$data['website_price_list_id'] = $this->prices_model->get_option('website_price_list');

		$data['parkavia_price_list_id'] = $this->prices_model->get_option('parkvia_reschedule_pricing');

		$data['website_price_list_prepaid'] = $this->prices_model->get_option('website_price_list_prepaid');

		$data['services'] = $this->db->get('options');

		//check option log if service exist

		$this->addnewoptioninlog($res_id);

		//getting reservation option ids

		$service_op = $this->db->select('option_id')->where('res_id',$res_id)->get('reservation_options')->result_array();

		// $service_op = $this->db->select('options_log.*,options.ID as option_id')->from('options_log')->join('options','options_log.name=options.name')->where('reservation_id',$res_id)->get()->result_array();

		$service_option=array_map (function($value){

			return $value['option_id'];

		} , $service_op);

		$data['service_option'] = $service_option;

		$this->load->view('edit_reservation', $data);

	}

	public function edit_agreement() {



		$this->load->model('prices_model');



		$res_id = $this->input->get('id');

		$data['res_data'] = $this->db->where('ID', $res_id)->get('agreements')->result_array() [0];

		$res_id = $data['res_data']['res_id'];

		$data['extra_costs'] = $this->db->select('*')->where('res_id', $res_id)->get('costuri_extra')->result();

		$data['payments'] = $this->db->select('*')->where('res_id', $res_id)->get('payments')->result();

		$data['cus_data'] = $this->db->where('ID', $data['res_data']['customer_id'])->get('customers')->result_array() [0];

		if( $data['res_data']['tip'] != "Abonament" ){

			$data['price_data'] = $this->db->where('reservation_id', $res_id)->get('prices_log')->result_array() [0];

			$data['options_data'] = $this->db->where('reservation_id', $res_id)->get('options_log')->result_array() [0];

		}



		$data['cars'] = $this->find_customer_cars($data['cus_data']['email']);



		$data['prices_list'] = $this->prices_model->get_price_lists();



		$data['website_price_list_id'] = $this->prices_model->get_option('website_price_list');

		$data['parkavia_price_list_id'] = $this->prices_model->get_option('parkvia_reschedule_pricing');

		$data['website_price_list_prepaid'] = $this->prices_model->get_option('website_price_list_prepaid');

		$data['services'] = $this->db->get('options');

		//check option log if service exist

		$this->addnewoptioninlog($res_id);

		//getting reservation option ids

		$service_op = $this->db->select('option_id')->where('res_id',$res_id)->get('reservation_options')->result_array();

		$service_option=array_map (function($value){

			return $value['option_id'];

		} , $service_op);

		$data['service_option'] = $service_option;



		$this->load->view('edit_agreement', $data);

	}

	//function for update option log with new srvice if not exist

	function addnewoptioninlog($res_id)

	{

		$option = $this->db->get('options');

		if($option->num_rows() > 0)

		{

			$optionlog_arr = [];

			foreach($option->result() as $o)

			{

				$option_log = $this->db->where('reservation_id',$res_id)->where('name',$o->name)->get('options_log');

				if($option_log->num_rows() < 1)

				{

						$optionlog_arr[]=[

							"name" => $o->name,

							"value" => $o->value,

							"reservation_id" => $res_id

						];

						

				}

			}

			if(count($optionlog_arr) > 0)

			{

				$this->db->insert_batch('options_log', $optionlog_arr);

			}

			

			

		}

	}





	private function find_customer_cars($email){



		$data = $this->db->where('email',$email)->get('customers')->result_array();



		$cars = [];



		foreach ($data as $key => $car) {

			

			$cars[$car['nr_inmatriculare']]['marca'] = $car['marca'];

			$cars[$car['nr_inmatriculare']]['model'] = $car['model'];

			$cars[$car['nr_inmatriculare']]['nr_inmatriculare'] = $car['nr_inmatriculare'];



		}



		$data = $this->db->where('customer_email',$email)->get('customers_cars')->result_array();



		foreach ($data as $key => $car) {

			

			$cars[$car['nr_inmatriculare']]['marca'] = $car['marca'];

			$cars[$car['nr_inmatriculare']]['model'] = $car['model'];

			$cars[$car['nr_inmatriculare']]['nr_inmatriculare'] = $car['nr_inmatriculare'];



		}



		return $cars;



	}





	public function get_reservations() {

		$start_date = $this->input->post('start');

		$end_date = $this->input->post('end');

		$results = $this->db->select('*, r.ID as ID')->join(' customers c', 'c.ID = r.customer_id')->where('r.status', '0')->or_where('r.status', '1')->get('reservations r')->result_array();

		//echo json_encode($results); exit();

		$agreements = $this->db->select('*, r.ID as ID')->join(' customers c', 'c.ID = r.customer_id')->where('r.status', '4')->get('agreements r')->result_array();

		foreach ($agreements as $a) {

			array_push($results, $a);

		}

		echo json_encode($results);

	}

	//function for inserting previous option data in reservation option

	public function insertprev_option()

	{

		$washing = $this->db->where('name','washing')->get('options')->row();

		$washing_id = $washing->ID;

		//getting all reservation ID with option from reservation table

		$res = $this->db->select('ID')->where('washing',1)->get('reservations');

		$res_arr = [];

		foreach($res->result() as $r)

		{

			$washing_price = $this->db->select('value')->where('reservation_id',$r->ID)->where('name','washing')->get('options_log')->row();



			$res_arr[]=[

				"res_id" => $r->ID,

				"option_id" => $washing_id,

				"price" => $washing_price->value

			];

		}

		$this->db->insert_batch('reservation_options',$res_arr);

		echo "<pre>";print_r($res_arr);exit();

	}

	public function send_mail_test(){

		$settings = $this->db->where('email_status',1)->get('settings');

		if($settings->num_rows() > 0)

		{

			$this->load->library('email');

			$this->email->to('emtoader@gmail.com');

			$this->email->from('reservations@flyparking.ro');

			$this->email->subject('Bonul Dvs Flying Parking');

			$this->email->message('Bonul dumneavoastra este atasat.');

			@$this->email->send();

		}
	}

	//end of class



}
