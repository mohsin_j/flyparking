<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Welcome extends CI_Controller {



	private $auth;



// 	private $email;

	private $new_password;



	public function __construct(){

        parent::__construct();

		$language = $this->session->userdata('lang');

		$language = 'romania';
		
		$this->lang->load('default',$language);



        //load authclass

     	 $this->load->database();

		 $db = new \PDO('mysql:dbname='.$this->db->database.';host=localhost;charset=utf8mb4', $this->db->username, $this->db->password);

		 $this->auth = new \Delight\Auth\Auth($db);

		 $this->load->helper('url');

		

      }





	public function index()

	{

		$data['services'] = $this->db->where('status',1)->get('options');

		$this->load->view('site/index',$data);

	}

	public function contact()

	{

		$this->load->view('site/contact');


	}
	public function sendmail(){
	    $message = $this->input->post('message');
	    $subject = $this->input->post('subject');
	    $from = $this->input->post('from');
	    $name = $this->input->post('name');
	    if($message != "" && $subject != "" && $from != "" && $name !=""){
	        $this->send_contact_mail($from,$subject,$message,$name);
	        $this->session->set_flashdata('success', 'Trimiterea cu succes a e-mailului');
	        redirect(base_url('Welcome/contact'));
	    }
	}
	
	public function send_contact_mail($from,$subject,$message,$name) {

	

		$settings = $this->db->where('email_status',1)->get('settings');

					if($settings->num_rows() > 0)

					{

					    

		$this->load->library('email');

	

// 		$subject = $this->input->post('subject');

		

		
		
        $message .= $this->input->post('message');
		

		/** Get full html: */

		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml">

		<head>

			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />

			<title>' . $subject . '</title>

			<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

			<link href="http://alana-rent.m-web-design.ro/metronic_assets/pages/css/invoice-2.min.css" rel="stylesheet" type="text/css">

			<script src="https://code.jquery.com/jquery-3.2.1.min.js"

				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="

				  crossorigin="anonymous"></script>

			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

		</head>

		<body>
		<div class="container">
		    <div class="card">
		        <div class="card-header text-center">
		            <h4>Contact-Form Email</h4>
		        </div>
		        <div class="card-body">
		           
		            <div style="max-width:600px;margin:0 auto;display:block;padding:10px;border:1px solid grey;">
		            <table border="1" cellpadding="1" cellspacing="1" dir="ltr" style="width:100%">
		            <tbody>
		            <tr>
		            <td colspan="6"><img src="https://flyparking.ro/logoflyparkingtrim.png" style="width:220px;"></td>
		         
		            </tr>
		            <tr><td>Nume</td><td>'.$name.'</td></tr>
		            <tr><td>Subiect</td><td>'.$subject.'</td></tr>
		            <tr><td>Mesaj</td><td>'.$message.'</td></tr>
		            </tbody></table>
		        </div>
		        </div>
		    </div>
		</div>

		' . $message . '

		</body>

		</html>';

		/** Also, for getting full html you may use the following internal method: */

		/** $body = $this->email->full_html($subject, $message); */

		@$result = $this->email->from($from)->reply_to('reservations@flyparking.ro')

			/** Optional, an account where a human being reads. */->to('reservations@flyparking.ro')->cc('reservations@flyparking.ro')->subject($subject)->message($body)->send();

	}



	}

	public function parking()

	{

		$this->load->view('site/parking');

	}

	public function howwork()

	{

		$this->load->view('site/how_it_work');

	}

	public function meetandgreet()

	{

		$this->load->view('site/meetandgreet');

	}

	public function testomonial()

	{

		$this->load->view('site/testomonial');

	}

	public function blog()

	{

		$this->load->view('site/blog');

	}

	public function prices()
	{
		$this->load->model('Prices_model');
		$available_price_list = $this->Prices_model->get_available_price_list();
//		echo '<pre>';
//		print_r($available_price_list);
//		exit();
		$this->load->view('site/prices', ['prices_list' => $available_price_list]);
	}

	public function faq()

	{

		$this->load->view('site/faq');

	}

	public function redirect()

	{

		$this->session->set_flashdata('success', 'Rezervarea dumneavoastră a avut succes!');

		redirect(base_url());

	}

	public function changelang()

	{

		$lang = $this->input->post('lang');

		if(isset($lang) && !empty($lang))

		{

			$this->session->set_userdata('lang',$lang);

			echo json_encode(['status' => true]);

			exit;

		}

		echo json_encode(['status' => false]);

		

	}



	function make_base(){



    $this->load->library('VpxMigration');



    // All Tables:



    $this->vpxmigration->generate();



	}



	public function login(){





		$rememberDuration = null;

		if (@$_POST['check'] == 'on') {

		    // keep logged in for one year

		    $rememberDuration = (int) (60 * 60 * 24 * 365.25);

		}

		 try {

		    

		    $user = $this->db->where('email', $_POST['email'])->get('users')->row();



            $isUserValid = $user ? $user->status : 5;



             if ($isUserValid != 0) {

                 die('User invalid');

             }



             $this->auth->login($_POST['email'], $_POST['password'], $rememberDuration);



		    echo 'User is logged in';

		}

		catch (\Delight\Auth\InvalidEmailException $e) {

		    die('Wrong email address');

		}

		catch (\Delight\Auth\InvalidPasswordException $e) {

		    die('Wrong password');

		}

		catch (\Delight\Auth\EmailNotVerifiedException $e) {

		    die('Email not verified');

		}

		catch (\Delight\Auth\TooManyRequestsException $e) {

		    die('Too many requests');

		}

	}



	public function register(){

		try {

		    $userId = $this->auth->register('emil@exclusive.ro', 'test', 'emil', function ($selector, $token) {

		        $this->confirm_user_registration($selector, $token);

		    });



		    echo 'We have signed up a new user with the ID ' . $userId;

		}

		catch (\Delight\Auth\InvalidEmailException $e) {

		    die('Invalid email address');

		}

		catch (\Delight\Auth\InvalidPasswordException $e) {

		    die('Invalid password');

		}

		catch (\Delight\Auth\UserAlreadyExistsException $e) {

		    die('User already exists');

		}

		catch (\Delight\Auth\TooManyRequestsException $e) {

		    die('Too many requests');

		}



	}



	public function confirm_user_registration($selector, $token){

		try {

		    $this->auth->confirmEmail($selector, $token);



		    echo 'Email address has been verified';

		}

		catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {

		    die('Invalid token');

		}

		catch (\Delight\Auth\TokenExpiredException $e) {

		    die('Token expired');

		}

		catch (\Delight\Auth\UserAlreadyExistsException $e) {

		    die('Email address already exists');

		}

		catch (\Delight\Auth\TooManyRequestsException $e) {

		    die('Too many requests');

		}

	}



	public function change_password($email, $new_password){



		$email = urldecode($email);

		$this->email = urldecode($email);

		$this->new_password = $new_password;





		    $this->auth->forgotPassword($email, function ($selector, $token) {

		 

		    	if ($this->auth->canResetPassword($selector, $token)) {



		

   					var_dump($this->auth->resetPassword($selector, $token, $this->new_password));



   					// $rememberDuration = (int) (60 * 60 * 24 * 365.25);

   					// $this->auth->login($this->email, $this->new_password, $rememberDuration);



				}		    	



		    });



		    echo 'Request has been generated';

		



	}



}

