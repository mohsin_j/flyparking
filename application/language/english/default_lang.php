<?php

$lang['name'] = "Name";
$lang['name_surname'] = "Name and Surname";
$lang['email'] = "Email";
$lang['phone'] = "Telephone";
$lang['departure_date'] = "Departure Date";
$lang['arrival_date'] = "Arrival Date";
$lang['arrival_departure_date'] = "Arrival Departure Date";
$lang['arrival_hour'] = "Arrival Hour";
$lang['departure_hour'] = "Departure Hour";
$lang['request_invoice_msg'] = "I want the invoice (legal person)";
$lang['cui_company'] = "CUI company";
$lang['days'] = "Days";
$lang['at_location'] = "At Location";
$lang['online_payment'] = "Online Payment";
$lang['agreement_msg'] = "I agree with the terms and conditions";
$lang['booking_btn'] = 'Confirm Booking';
$lang['onlinereservation_form_err_msg'] = 'Please fill in all fields: name, e-mail, phone, departure date, return date.
<br>Thank you!';
?>
