<?php

$lang['name'] = "Nume";
$lang['name_surname'] = "NUME SI PRENUME";
$lang['email'] = "E-MAIL";
$lang['phone'] = "TELEFON";
$lang['departure_date'] = "DATA PLECARE";
$lang['arrival_date'] = "DATA RETUR";
$lang['arrival_departure_date'] = "RETUR PLECARE DATA";
$lang['arrival_hour'] = "RETUR ORA";
$lang['departure_hour'] = "PLECARE ORA";
$lang['request_invoice_msg'] = "Doresc factura ( persoana juridica )";
$lang['cui_company'] = "CUI FIRMA";
$lang['days'] = "Zila";
$lang['at_location'] = "Plateste la locatie";
$lang['online_payment'] = "Plateste online";
$lang['agreement_msg'] = "Sunt de acord cu termenii si conditiile";
$lang['booking_btn'] = 'Confirmați rezervarea';
$lang['onlinereservation_form_err_msg'] = 'Te rugam sa completez toate campurile: nume, email, telefon, data plecare, data retur si sa fii de acord cu Termenii si Conditiile de mai sus.
<br>Multumim!';
?>
